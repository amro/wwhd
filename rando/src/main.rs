#![allow(dead_code)]
#![allow(clippy::upper_case_acronyms)]
#![feature(str_from_utf16_endian)]
mod asm;
mod changes;
mod formats;
mod logic;

use std::env;
use std::error::Error;
use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::path::Path;

use docopt::Docopt;
use formats::*;
use serde::Deserialize;

use crate::asm::map::Symbols;
use crate::logic::locations::PatchTarget;
use crate::logic::locations::LOCATIONS;
use crate::logic::Item;

const VERSION: &str = env!("CARGO_PKG_VERSION");

const USAGE: &str = "
Wind Waker HD Randomizer.

Usage:
  wwhd patch <input> <output>
  wwhd stage <input> <output>
  wwhd yaz (compress|decompress) <input> <output>
  wwhd sarc <input>
  wwhd info <input>
  wwhd extract <input> <file> <output>
  wwhd (-h | --help)
  wwhd --version

Options:
  -h --help     Show this screen.
  --version     Show version.
";

#[derive(Debug, Deserialize)]
struct Args {
  arg_name: Option<String>,
  arg_file: Option<String>,
  arg_input: Option<String>,
  arg_output: Option<String>,
  flag_version: bool,
  cmd_patch: bool,
  cmd_stage: bool,
  cmd_sarc: bool,
  cmd_extract: bool,
  cmd_yaz: bool,
  cmd_info: bool,
  cmd_compress: bool,
  #[allow(dead_code)]
  cmd_decompress: bool,
}

fn patch<P: AsRef<Path>>(input: P, output: P) -> Result<(), FormatError> {
  log::info!("patching from {:#?} to {:#?}", input.as_ref(), output.as_ref());
  let mut manager = FileManager::new(input.as_ref(), output.as_ref());

  let symbols = Symbols::read_from(&mut File::open("../custom/target/map.txt").unwrap())?;
  {
    let elf = manager.get_rpx_mut();
    let mut data = Vec::new();
    File::open("../custom/target/custom.bin")?.read_to_end(&mut data)?;
    elf.append_text(data);

    asm::patches::patch_starting_items(
      elf,
      &symbols,
      &[
        Item::Hookshot,
        Item::HerosSword,
        Item::Bombs,
        Item::DekuLeaf,
        Item::PiratesCharm,
        Item::SwiftSail,
        Item::BalladOfGales,
        Item::WindsRequiem,
        Item::SongOfPassing,
      ],
    );
    asm::patches::apply_all_patches(elf, &symbols);
  };

  log::info!("patching stages");
  changes::add_dungeon_items(&mut manager, &symbols)?;
  changes::add_chests_underground(&mut manager)?;
  changes::drc_add_magic_jars(&mut manager)?;
  changes::drc_add_boulder_destroyers(&mut manager)?;
  changes::drc_remove_invisible_wall(&mut manager)?;
  changes::drc_add_medli_chest(&mut manager)?;
  changes::fix_deku_leaf_model(&mut manager)?;
  changes::misc(&mut manager)?;

  for location in LOCATIONS.iter() {
    for target in location.targets {
      match *target {
        PatchTarget::Rpx(addr) => {
          let elf = manager.get_rpx_mut();
          asm::patches::put_item(elf, addr, Item::Hookshot);
        }
        PatchTarget::Chest(path, layerid) => {
          let (layer, id) = layerid.to_layer_and_index();
          let dzx = manager.get_dzx(path.to_filename())?;
          if let Some(tres) = dzx.get_tres_mut(layer) {
            if let Some(chest) = tres.get_mut(id as usize) {
              chest.chest_item = Item::Hookshot.id();
              if id == 0x0 {
                chest.chest_item = Item::BigKey.id();
              }
              if id == 0x5 {
                chest.chest_item = Item::DragonRoostCavernCompass.id();
              }
            } else {
              panic!("No chest found at {:?}/{:#X}/{:#X}", path, layer as u8, id);
            }
          } else {
            panic!("No tres found at {:?}/{:#X}", path, layer as u8);
          }
        }
        PatchTarget::Event(path, event, actor, action) => {
          let event_list = manager.get_event_list(path.to_filename())?;

          if event_list
            .update_event_action(event, actor, action, Item::Hookshot.id() as u32)
            .is_none()
          {
            log::warn!("Failed to patch event!");
          }
        }
        PatchTarget::Actor(path, layerid) => {
          let (layer, id) = layerid.to_layer_and_index();
          let dzx = manager.get_dzx(path.to_filename())?;
          if let Some(actors) = dzx.get_actors_mut(layer) {
            // for (i, ref mut act) in actors.iter_mut().enumerate() {
            //   if dzx::DaTsubo::matches(act.name) {
            //     log::info!("Old params pot: {:#x}", act.z_rot);
            //     act.set_z_param(dzx::DaTsubo::ITEM_ID, Item::Hookshot.id());
            //     log::info!("New params pot: {:#x}", act.z_rot);
            //   }
            //   log::info!("{:#x}: {}", i, std::str::from_utf8(&act.name).unwrap());
            // }
            if let Some(ref mut actor) = actors.get_mut(id as usize) {
              if dzx::DaItem::matches(actor.name) {
                actor.set_param(dzx::DaItem::ITEM_ID, Item::Hookshot.id());
              } else if dzx::DaBossItem::matches(actor.name) {
                actor.set_param(dzx::DaBossItem::ITEM_ID, Item::Hookshot.id());
              } else if dzx::DaTsubo::matches(actor.name) {
                actor.set_z_param(dzx::DaTsubo::ITEM_ID, Item::Hookshot.id());
              } else {
                panic!(
                  "Actor {:?}/{:?}/{} ({}) is not an item",
                  path,
                  layer,
                  id,
                  std::str::from_utf8(&actor.name).unwrap(),
                );
              }
            } else {
              dzx.describe_actors();
              panic!("Actor {:?}/{:?}/{} not found", path, layer, id);
            }
          } else {
            dzx.describe_actors();
            panic!("Actors {:?}/{:?} not found {:?}", path, layer, layerid);
          }
        }
      }
    }
  }
  manager.save_rpx()?;
  manager.save()?;

  Ok(())
}

fn stage<P: AsRef<Path>>(input: P, output: P) -> Result<(), FormatError> {
  // let split: Vec<&str> = name.split('/').collect();
  // let sarc_name = format!("E:\\Stage\\{}", split[0]);
  let mut manager = FileManager::new(input.as_ref(), output.as_ref());

  // let dzx = manager.get_dzx("sea_Stage.szs")?;
  // dzx.write(&mut File::create(output)?)?;

  let el = manager.get_event_list("sea_Stage.szs")?;
  el.write(&mut File::create(output)?)?;

  // let mut output = File::create("dumps/event_list.dat").unwrap();
  // output.write_all(fres.embedded_file("event_list.dat"))?;

  // let mut output = File::create("dumps/stage.dzr").unwrap();
  // output.write_all(fres.embedded_file("stage.dzr"))?;
  // dzs.write(&mut output)?;

  Ok(())
}

fn yaz(compress: bool, input: &str, output: &str) {
  let mut buf = Vec::new();
  File::open(input).unwrap().read_to_end(&mut buf).unwrap();

  let res = if compress {
    formats::compress(&buf)
  } else {
    formats::decompress(&buf).unwrap()
  };

  File::create(output).unwrap().write_all(&res).unwrap();
}

fn main() -> Result<(), Box<dyn Error>> {
  pretty_env_logger::init();
  let args: Args = Docopt::new(USAGE).and_then(|d| d.deserialize()).unwrap_or_else(|e| e.exit());

  if args.flag_version {
    println!("Wind Waker HD Randomizer {}", VERSION);
  } else if args.cmd_patch {
    patch(&args.arg_input.unwrap(), &args.arg_output.unwrap())?;
  } else if args.cmd_stage {
    stage(&args.arg_input.unwrap(), &args.arg_output.unwrap())?;
  } else if args.cmd_sarc {
    let path = args.arg_input.unwrap();
    let sarc = formats::Sarc::from_path(path)?;
    sarc.dbg_print();
  } else if args.cmd_info {
  } else if args.cmd_extract {
    let path = args.arg_input.unwrap();
    let sarc = formats::Sarc::from_path(path)?;
    let name = args.arg_file.unwrap();
    let outname = args.arg_output.unwrap();
    if name.contains("bfres:") {
      if let Some(offset) = name.rfind(':') {
        let (bfres_name, inner) = name.split_at(offset);
        let inner = &inner[1..];
        let fres = formats::FRES::from_data(sarc.get_file(bfres_name).unwrap().into()).unwrap();
        println!("Extracting {}", inner);
        let embd = fres.embedded_file(inner);
        File::create(&outname).unwrap().write_all(embd).unwrap();
      } else {
        println!("Extracting {}", name);
        File::create(&outname)?.write_all(sarc.get_file(&name).unwrap())?;
      }
    } else {
      println!("Extracting {}", name);
      File::create(&outname)?.write_all(sarc.get_file(&name).unwrap())?;
    }
  } else if args.cmd_yaz {
    yaz(args.cmd_compress, &args.arg_input.unwrap(), &args.arg_output.unwrap());
  }

  Ok(())
}

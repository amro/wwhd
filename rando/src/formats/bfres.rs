use crate::formats::bytes::*;

use std::collections::HashMap;
use std::io::Cursor;
use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Write;

const FRES_MAGIC: u32 = 0x46524553;

#[derive(Debug)]
#[allow(dead_code)]
struct Header {
  version: u32,
  bom: u16,
  file_length: u32,
  file_alignment: u32,
  file_name_offset: i32,
  string_table_length: i32,
  string_table_offset: i32,
  file_offsets: [i32; 12],
  file_counts: [u16; 12],
}

fn parse_header<T: Read>(reader: &mut T) -> Result<Header, FormatError> {
  let magic = reader.read_u32_be()?;
  if magic != FRES_MAGIC {
    return Err(FormatError::BadMagic);
  }
  let version = reader.read_u32_be()?;
  let bom = reader.read_u16_be()?;
  let header_size = reader.read_u16_be()?;
  if header_size != 0x10 {
    return Err(FormatError::Corrupt("Bad header size".into()));
  }

  let file_length = reader.read_u32_be()?;
  let file_alignment = reader.read_u32_be()?;
  let file_name_offset = reader.read_i32_be()?;
  let string_table_length = reader.read_i32_be()?;
  let string_table_offset = reader.read_i32_be()?;

  let mut file_offsets = [0i32; 12];

  for i in 0..12 {
    file_offsets[i] = reader.read_i32_be()?;
  }

  let mut file_counts = [0u16; 12];

  for i in 0..12 {
    file_counts[i] = reader.read_u16_be()?;
  }

  let _ = reader.read_u32_be()?;
  Ok(Header {
    version,
    bom,
    file_length,
    file_alignment,
    file_name_offset,
    string_table_length,
    string_table_offset,
    file_offsets,
    file_counts,
  })
}

#[derive(Debug, Clone)]
#[allow(dead_code)]
struct Node {
  search: u32,
  left: u16,
  right: u16,
  name: i32,
  data: i32,
  offset: usize,
  idx: usize,
}

#[derive(Debug, Clone)]
#[allow(dead_code)]
struct IndexGroup {
  length: u32,
  entries: Vec<Node>,
}

fn parse_index_group<T: Read>(reader: &mut T, offset: usize) -> Result<IndexGroup, FormatError> {
  let length = reader.read_u32_be()?;
  let num_entries = reader.read_i32_be()?;

  let mut addl_offset = 12;

  let mut entries = Vec::with_capacity(num_entries as usize);
  for i in 0..num_entries + 1 {
    let search = reader.read_u32_be()?;
    let left = reader.read_u16_be()?;
    let right = reader.read_u16_be()?;
    let name = reader.read_i32_be()?;
    let data = reader.read_i32_be()?;
    entries.push(Node {
      search,
      left,
      right,
      name,
      data,
      offset: offset + addl_offset,
      idx: (i - 1) as usize,
    });
    addl_offset += 16;
  }
  Ok(IndexGroup { length, entries })
}

fn _parse_strings<T: Read>(reader: &mut T, length: i32) -> Result<Vec<String>, FormatError> {
  let mut read = 0;
  let mut strings = Vec::new();
  while read < length {
    let length = reader.read_u32_be()?;
    let mut buf = vec![0; length as usize];
    reader.read_exact(&mut buf).unwrap();
    reader.read_u8().unwrap(); //terminating null
    read += length as i32 + 5;
    strings.push(String::from_utf8(buf).unwrap());
    let padding = read % 4;
    if padding != 0 {
      for _ in 0..(4 - padding) {
        reader.read_u8().unwrap(); //padding for 4-alignment
        read += 1;
      }
    }
  }
  Ok(strings)
}

#[derive(Debug)]
#[allow(dead_code)]
pub struct FRES {
  header: Header,
  index_groups: HashMap<usize, IndexGroup>,
  embedded_files: HashMap<String, Node>,
  updates: HashMap<String, Vec<u8>>,
  data: Vec<u8>,
}

impl FRES {
  pub fn print(&self) {
    for name in self.embedded_files.keys() {
      // log::info!("File: {}", f);

      let (offset, length) = self.embedded_file_offset(name);
      println!("File: {} at {:#X}, {:#X}", name, offset, length);
    }
  }
  pub fn from_data(data: Vec<u8>) -> Result<Self, FormatError> {
    let mut reader = Cursor::new(&data);
    let header = parse_header(&mut reader)?;
    let mut index_groups = HashMap::new();
    for i in 0..12 {
      if header.file_counts[i] > 0 {
        let offset = header.file_offsets[i] as usize + 0x20 + (i * 4);
        let mut reader = Cursor::new(&data[offset..]);
        let ig = parse_index_group(&mut reader, offset)?;
        index_groups.insert(i, ig);
      }
    }

    let mut embedded_files = HashMap::new();
    let embedded = index_groups.get(&11).unwrap();
    for node in embedded.entries.iter() {
      if node.name > 0 {
        // let name = fres.node_name_string(e);
        let mut reader = Cursor::new(&data[node.name as usize + node.offset..]);
        let length = reader.read_u32_be().unwrap();
        let mut buf = vec![0; length as usize];
        reader.read_exact(&mut buf).unwrap();
        let name = String::from_utf8(buf).unwrap();
        embedded_files.insert(name, node.clone());
      }
    }
    let fres = FRES {
      header,
      index_groups,
      embedded_files,
      updates: HashMap::new(),
      data,
    };

    Ok(fres)
  }

  pub fn prep_update(&mut self, name: &str, data: Vec<u8>) {
    self.updates.insert(name.into(), data);
  }

  pub fn write_with_updates<T: Write + Seek>(mut self, writer: &mut T) -> Result<(), FormatError> {
    // first we write all the current data
    writer.write_all(&self.data)?;

    let mut to_write = Vec::new();

    for (name, node) in self.embedded_files.iter() {
      let (offset, _) = self.embedded_file_offset(name);
      if let Some(data) = self.updates.remove(name) {
        to_write.push((offset, name, node, data));
      } else {
        to_write.push((offset, name, node, Vec::from(self.embedded_file(name))));
      }
    }

    to_write.sort_by(|a, b| a.0.cmp(&b.0));

    let mut new_offset = to_write[0].0 as i32;

    for (offset, _name, node, data) in to_write {
      let header_off = new_offset - 0x6c - (0x8 * node.idx as i32);
      // log::info!("Writing {} from {:#010x} to {:#010x}", name, offset, new_offset,);
      assert!(offset == new_offset as usize);
      let header_pos = node.offset as i32 + node.data + 8;
      writer.seek(SeekFrom::Start(header_pos as u64))?;
      writer.write_i32_be(header_off)?;
      writer.write_u32_be(data.len() as u32)?;

      writer.seek(SeekFrom::Start(new_offset as u64))?;
      new_offset += data.len() as i32;
      new_offset = new_offset.align(0x100);

      writer.write_all(&data)?;
      writer.pad(0x100)?;
    }

    Ok(())
  }

  fn embedded_file_offset(&self, name: &str) -> (usize, usize) {
    let node = self.embedded_files.get(name).unwrap();
    let header_pos = node.offset as i32 + node.data + 8;
    let mut reader = Cursor::new(&self.data[header_pos as usize..]);
    let real_offset = reader.read_i32_be().unwrap();
    let length = reader.read_u32_be().unwrap() as usize;
    let offset = 0x6c + real_offset + (0x8 * node.idx) as i32;
    let offset = offset as usize;
    (offset, length)
  }

  pub fn embedded_file(&self, name: &str) -> &[u8] {
    let (offset, length) = self.embedded_file_offset(name);
    &self.data[offset..offset + length]
  }
}

use std::collections::HashMap;
use std::fs::File;
use std::io;
use std::io::Cursor;
use std::io::Read;
use std::io::Seek;
use std::io::Write;
use std::num::Wrapping;
use std::path::Path;

use crate::formats::bytes::*;
use crate::formats::yaz;

const SARC_MAGIC: u32 = 0x53415243;
const SFAT_MAGIC: u32 = 0x53464154;
const SFNT_MAGIC: u32 = 0x53464e54;

#[derive(Debug)]
struct Header {
  bom: u16,
  size: u32,
  data_start: u32,
  version: u16,
}

#[derive(Debug)]
pub struct SFATHeader {
  pub node_count: u16,
  hash_key: u32,
}

#[derive(Debug, Clone)]
struct Node {
  hash: u32,
  attributes: u32,
  data_start: u32,
  data_end: u32,
}

pub struct Sarc {
  header: Header,
  sfat_header: SFATHeader,
  nodes: Vec<Node>,
  names: Vec<String>,
  node_map: HashMap<u32, Node>,
  name_map: HashMap<u16, String>,
  updates: HashMap<String, Vec<u8>>,
  pub data: Vec<u8>,
}

fn parse_header<T: Read>(reader: &mut T) -> Result<Header, FormatError> {
  let magic = reader.read_u32_be()?;
  if magic != SARC_MAGIC {
    return Err(FormatError::Corrupt(format!("Wrong header {}", magic)));
  }
  let header_size = reader.read_u16_be()?;
  if header_size != 0x14 {
    return Err(FormatError::Corrupt("Wrong header size found, expected 0x14".into()));
  }
  let bom = reader.read_u16_be()?;

  let size = reader.read_u32_be()?;
  let data_start = reader.read_u32_be()?;
  let version = reader.read_u16_be()?;
  let _ = reader.read_u16_be()?;
  Ok(Header {
    bom,
    size,
    data_start,
    version,
  })
}

fn write_header<T: Write>(writer: &mut T, header: &Header) -> Result<(), io::Error> {
  writer.write_u32_be(SARC_MAGIC)?;
  writer.write_u16_be(0x14)?;
  writer.write_u16_be(header.bom)?;
  writer.write_u32_be(header.size)?;
  writer.write_u32_be(header.data_start)?;
  writer.write_u16_be(header.version)?;
  writer.write_u16_be(0x00)?;
  Ok(())
}

fn parse_sfat_header<T: Read>(reader: &mut T) -> Result<SFATHeader, FormatError> {
  let magic = reader.read_u32_be()?;
  if magic != SFAT_MAGIC {
    return Err(FormatError::Corrupt("Bad SFAT header".into()));
  }
  let header_size = reader.read_u16_be()?;
  if header_size != 0xc {
    return Err(FormatError::Corrupt("Bad SFAT header".into()));
  }
  let node_count = reader.read_u16_be()?;

  let hash_key = reader.read_u32_be()?;
  Ok(SFATHeader { node_count, hash_key })
}

fn write_sfat_header<T: Write>(writer: &mut T, header: &SFATHeader) -> Result<(), io::Error> {
  writer.write_u32_be(SFAT_MAGIC)?;
  writer.write_u16_be(0xc)?;
  writer.write_u16_be(header.node_count)?;
  writer.write_u32_be(header.hash_key)?;
  Ok(())
}

fn parse_sfat_node<T: Read>(reader: &mut T) -> Result<Node, io::Error> {
  let hash = reader.read_u32_be()?;
  let attributes = reader.read_u32_be()?;
  let data_start = reader.read_u32_be()?;
  let data_end = reader.read_u32_be()?;
  Ok(Node {
    hash,
    attributes,
    data_start,
    data_end,
  })
}

fn write_sfat_node<T: Write>(writer: &mut T, node: &Node) -> Result<(), io::Error> {
  writer.write_u32_be(node.hash)?;
  writer.write_u32_be(node.attributes)?;
  writer.write_u32_be(node.data_start)?;
  writer.write_u32_be(node.data_end)?;
  Ok(())
}

fn parse_sfnt<T: Read>(reader: &mut T, expected_length: usize) -> Result<Vec<(u16, String)>, FormatError> {
  let magic = reader.read_u32_be()?;
  if magic != SFNT_MAGIC {
    return Err(FormatError::Corrupt("Bad SFNT header".into()));
  }
  let header_size = reader.read_u16_be()?;
  if header_size != 0x8 {
    return Err(FormatError::Corrupt("Bad SFNT header".into()));
  }
  let _ = reader.read_u16_be()?;

  let mut strings = Vec::new();
  let mut cur_string = Vec::new();
  let mut cur_string_start = 0xffff;
  let mut string_pos = 0;

  while let Ok(b) = reader.read_u8() {
    if b != 0 {
      if cur_string_start == 0xffff {
        cur_string_start = string_pos;
      }
      cur_string.push(b);
    } else if !cur_string.is_empty() {
      strings.push((cur_string_start / 4, String::from_utf8(cur_string).unwrap()));
      if strings.len() == expected_length {
        break;
      }
      cur_string = Vec::new();
      cur_string_start = 0xffff;
    }
    string_pos += 1;
  }

  Ok(strings)
}

fn write_sfnt<T: Write + Pad>(writer: &mut T, strings: &Vec<String>) -> Result<(), io::Error> {
  writer.write_u32_be(SFNT_MAGIC)?;
  writer.write_u16_be(0x8)?;
  writer.write_u16_be(0x00)?;

  for s in strings {
    // let len = s.len();
    // let pack = (len + 1) % 4;
    writer.write_all(s.as_bytes())?;
    writer.write_u8(0x00)?;

    // align to 4-byte boundaries if needed
    writer.pad(0x4)?;
    // for _ in pack..4 {
    //   writer.write_u8(0x00)?;
    // }
  }

  Ok(())
}

impl Sarc {
  pub fn from_path<P: AsRef<Path>>(path: P) -> Result<Self, FormatError> {
    let mut input = File::open(path)?;
    let mut buf = Vec::new();
    input.read_to_end(&mut buf)?;

    Sarc::from_buffer(buf)
  }

  pub fn from_buffer(mut data: Vec<u8>) -> Result<Self, FormatError> {
    if yaz::is_yaz(&data) {
      data = yaz::decompress(&data)?;
    }
    let mut reader = Cursor::new(&data);
    let header = parse_header(&mut reader)?;
    let sfat_header = parse_sfat_header(&mut reader)?;

    let mut node_map = HashMap::new();
    let mut nodes = Vec::new();
    for _ in 0..sfat_header.node_count {
      let node = parse_sfat_node(&mut reader)?;
      nodes.push(node.clone());
      node_map.insert(node.hash, node);
    }

    let sfnt = parse_sfnt(&mut reader, sfat_header.node_count as usize)?;

    let mut name_map = HashMap::new();
    let mut names = Vec::new();
    for (s, name) in sfnt {
      name_map.insert(s, name.clone());
      names.push(name);
    }

    Ok(Sarc {
      header,
      sfat_header,
      nodes,
      names,
      node_map,
      name_map,
      updates: HashMap::new(),
      data,
    })
  }

  pub fn prep_removal(&mut self, name: &str) {
    log::info!("prep removal: {}", name);
    self.updates.insert(name.into(), Vec::new());
  }

  pub fn prep_update(&mut self, name: &str, data: Vec<u8>) {
    log::info!("prep update: {}", name);
    self.updates.insert(name.into(), data);
  }

  pub fn write_with_updates<T: Write + Seek>(mut self, writer: &mut T, compress: bool) -> Result<(), FormatError> {
    let mut buf = Vec::new();
    let out_writer = writer;
    let writer = &mut Cursor::new(&mut buf);
    write_header(writer, &self.header)?;
    write_sfat_header(writer, &self.sfat_header)?;

    let mut to_write = Vec::new();

    // for n in self.updates.keys() {
    //   log::info!("update node {}", n);
    // }
    for n in self.names.iter_mut() {
      // log::info!("saving node {}", n);
      if let Some(data) = self.updates.get(n) {
        if data.is_empty() {
          // log::info!("removing node: {} {}", n, data.len());
          *n = format!("k{}", n);
        }
      }
    }

    for (_, node) in self.node_map.iter() {
      let name = if node.attributes & 0x01000000 == 0x01000000 {
        self
          .name_map
          .get(&((node.attributes & 0x0000ffff) as u16))
          .map(String::as_ref)
          .unwrap_or("unknown")
      } else {
        "unknown"
      };
      if let Some(data) = self.updates.remove(name) {
        to_write.push((node.clone(), node.clone(), data));
      } else {
        to_write.push((node.clone(), node.clone(), Vec::from(self.get_from_node(node))));
      }
    }

    to_write.sort_by(|a, b| a.0.data_start.cmp(&b.0.data_start));

    // let mut nodes_in_order: Vec<(Node, Node)> = self.nodes.iter().map(|n| (n.clone(), n.clone())).collect();
    // nodes_in_order.sort_by(|a, b| a.0.data_start.cmp(&b.0.data_start));

    let mut offtally = 0;
    for (new_node, old_node, data) in to_write.iter_mut() {
      let alignment = get_alignment(self.get_from_node(old_node));

      let size = data.len() as u32;
      offtally = offtally.align(alignment);
      new_node.data_start = offtally;
      new_node.data_end = offtally + size;

      write_sfat_node(writer, new_node)?;

      offtally += size;
    }

    write_sfnt(writer, &self.names)?;

    writer.pad(self.header.data_start)?;
    for (new_node, old_node, data) in to_write {
      let alignment = get_alignment(self.get_from_node(&old_node));
      writer.pad(alignment)?;
      assert_eq!(self.header.data_start + new_node.data_start, writer.stream_position()? as u32);
      writer.write_all(&data)?;
    }

    if compress {
      out_writer.write_all(&yaz::compress(&buf))?;
    } else {
      out_writer.write_all(&buf)?;
    }

    Ok(())
  }

  pub fn write_without_file<T: Write + Seek>(&mut self, writer: &mut T, name: &str, compress: bool) -> Result<(), FormatError> {
    let mut buf = Vec::new();
    let out_writer = writer;
    let writer = &mut Cursor::new(&mut buf);
    write_header(writer, &self.header)?;
    write_sfat_header(writer, &self.sfat_header)?;

    for n in self.names.iter_mut() {
      if n == name {
        *n = format!("k{}", n);
      }
    }

    let node_to_update = self
      .get_node(name)
      .unwrap_or_else(|| panic!("Attempt to update but file doesn't exist: {}", name));

    let mut nodes_in_order: Vec<(Node, Node)> = self.nodes.iter().map(|n| (n.clone(), n.clone())).collect();
    nodes_in_order.sort_by(|a, b| a.0.data_start.cmp(&b.0.data_start));

    let mut offtally = 0;
    for (new_node, old_node) in nodes_in_order.iter_mut() {
      let alignment = get_alignment(self.get_from_node(old_node));
      let size = if old_node.data_start == node_to_update.data_start {
        0
      } else {
        old_node.data_end - old_node.data_start
      };
      offtally = offtally.align(alignment);
      new_node.data_start = offtally;
      new_node.data_end = offtally + size;

      write_sfat_node(writer, new_node)?;

      offtally += size;
    }

    write_sfnt(writer, &self.names)?;

    writer.pad(self.header.data_start)?;
    for (new_node, old_node) in nodes_in_order {
      let alignment = get_alignment(self.get_from_node(&old_node));
      writer.pad(alignment)?;
      assert_eq!(self.header.data_start + new_node.data_start, writer.stream_position()? as u32);

      if old_node.data_start == node_to_update.data_start {
        // no data
      } else {
        writer.write_all(self.get_from_node(&old_node))?;
      }
    }

    if compress {
      out_writer.write_all(&yaz::compress(&buf))?;
    } else {
      out_writer.write_all(&buf)?;
    }

    Ok(())
  }

  pub fn write_with_update<T: Write + Seek>(&self, writer: &mut T, name: &str, data: &[u8], compress: bool) -> Result<(), FormatError> {
    let mut buf = Vec::new();
    let out_writer = writer;
    let writer = &mut Cursor::new(&mut buf);
    write_header(writer, &self.header)?;
    write_sfat_header(writer, &self.sfat_header)?;

    let node_to_update = self
      .get_node(name)
      .unwrap_or_else(|| panic!("Attempt to update but file doesn't exist: {}", name));

    let mut nodes_in_order: Vec<(Node, Node)> = self.nodes.iter().map(|n| (n.clone(), n.clone())).collect();
    nodes_in_order.sort_by(|a, b| a.0.data_start.cmp(&b.0.data_start));

    let mut offtally = 0;
    for (new_node, old_node) in nodes_in_order.iter_mut() {
      let alignment = get_alignment(self.get_from_node(old_node));
      let size = if old_node.data_start == node_to_update.data_start {
        data.len() as u32
      } else {
        old_node.data_end - old_node.data_start
      };
      offtally = offtally.align(alignment);
      new_node.data_start = offtally;
      new_node.data_end = offtally + size;

      write_sfat_node(writer, new_node)?;

      offtally += size;
    }

    write_sfnt(writer, &self.names)?;

    writer.pad(self.header.data_start)?;
    for (new_node, old_node) in nodes_in_order {
      let alignment = get_alignment(self.get_from_node(&old_node));
      writer.pad(alignment)?;
      assert_eq!(self.header.data_start + new_node.data_start, writer.stream_position()? as u32);

      if old_node.data_start == node_to_update.data_start {
        writer.write_all(data)?;
      } else {
        writer.write_all(self.get_from_node(&old_node))?;
      }
    }

    if compress {
      out_writer.write_all(&yaz::compress(&buf))?;
    } else {
      out_writer.write_all(&buf)?;
    }

    Ok(())
  }

  pub fn dbg_print(&self) {
    println!("File data start: {:#010X}", self.header.data_start);
    for node in self.node_map.values() {
      let name = if node.attributes & 0x01000000 == 0x01000000 {
        self
          .name_map
          .get(&((node.attributes & 0x0000ffff) as u16))
          .map(String::as_ref)
          .unwrap_or("unknown")
      } else {
        "unknown"
      };
      println!("{:#010X} - {:#010X}: {}", node.data_start, node.data_end, name);
    }
  }

  fn get_node(&self, name: &str) -> Option<&Node> {
    let hash = hash(name.as_bytes(), self.sfat_header.hash_key);

    self.node_map.get(&hash)
  }

  fn get_from_node(&self, node: &Node) -> &[u8] {
    let start = self.header.data_start as usize + node.data_start as usize;
    let end = self.header.data_start as usize + node.data_end as usize;
    &self.data[start..end]
  }

  pub fn get_file(&self, name: &str) -> Option<&[u8]> {
    let hash = hash(name.as_bytes(), self.sfat_header.hash_key);

    if let Some(node) = self.node_map.get(&hash) {
      let start = self.header.data_start as usize + node.data_start as usize;
      let end = self.header.data_start as usize + node.data_end as usize;
      Some(&self.data[start..end])
    } else {
      None
    }
  }
}

fn hash(name: &[u8], key: u32) -> u32 {
  let mut result = Wrapping(0u32);
  let key = Wrapping(key);
  for b in name {
    result = (result * key) + (Wrapping(*b as u32));
  }
  result.0
}

fn get_alignment(data: &[u8]) -> u32 {
  let len = data.len();
  if data[..4] == *b"SARC" {
    0x2000
  } else if data[..4] == *b"Yaz0" {
    0x80
  } else if data[..4] == *b"FFNT" {
    0x2000
  } else if data[..4] == *b"CFNT" {
    0x80
  } else if matches!(&data[..4], b"CSTM" | b"FSTM" | b"FSTP" | b"CWAV" | b"FWAV") {
    0x20
  } else if matches!(&data[..4], b"Gfx2" | b"FRES" | b"AAHS" | b"BAHS") || data[len - 0x28..len - 0x24] == *b"FLIM" {
    0x2000
  } else if data[..4] == *b"CTPK" {
    0x10
  } else if data[..4] == *b"CGFX" || data[len - 0x28..len - 0x24] == *b"CLIM" {
    0x80
  } else if data[..4] == *b"AAMP" {
    0x8
  } else if matches!(&data[..2], b"YB" | b"BY") || data[..8] == *b"MsgStdBn" {
    0x80
  } else {
    0x4
  }
}

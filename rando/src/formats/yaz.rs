use std::io::Cursor;
use std::io::Read;

use crate::formats::bytes::*;

const YAZ_MAGIC: u32 = 0x59617A30;

#[derive(Debug, Clone)]
struct Header {
  uncompressed_size: u32,
  _reserved_1: u32,
  _reserved_2: u32,
}

fn parse_header<T: Read>(reader: &mut T) -> Result<Header, FormatError> {
  let magic = reader.read_u32_be()?;
  if magic != YAZ_MAGIC {
    return Err(FormatError::BadMagic);
  }
  let uncompressed_size = reader.read_u32_be()?;
  let _reserved_1 = reader.read_u32_be()?;
  let _reserved_2 = reader.read_u32_be()?;
  Ok(Header {
    uncompressed_size,
    _reserved_1,
    _reserved_2,
  })
}

pub fn is_yaz(data: &[u8]) -> bool {
  let mut reader = Cursor::new(data);
  parse_header(&mut reader).is_ok()
}

pub fn decompress(data: &[u8]) -> Result<Vec<u8>, FormatError> {
  let mut reader = Cursor::new(data);
  let header = parse_header(&mut reader)?;
  let mut buf = Vec::with_capacity(header.uncompressed_size as usize);

  while let Ok(group_header) = reader.read_u8() {
    for i in 0..8 {
      if (group_header << i) & 0x80 == 0x80 {
        if let Ok(chunk) = reader.read_u8() {
          buf.push(chunk);
          continue;
        } else {
          break;
        }
      } else if let Ok(first_byte) = reader.read_u8() {
        let first_byte = first_byte as u32;
        let second_byte = reader.read_u8()? as u32;

        let copy_start = buf.len() - (((first_byte & 0x0f) << 8 | second_byte) + 1) as usize;

        let mut copy_amount = ((first_byte >> 4) + 2) as usize;

        if copy_amount == 2 {
          let third_byte = reader.read_u8()?;
          copy_amount = third_byte as usize + 0x12;
        }
        assert!(copy_amount >= 3 && copy_amount <= 0x111);

        for i in 0..copy_amount {
          buf.push(buf[copy_start + i as usize]);
        }
      }
    }
  }

  Ok(buf)
}

pub fn compress(data: &[u8]) -> Vec<u8> {
  let mut buf = Vec::new();
  let mut cursor = Cursor::new(&mut buf);
  cursor.write_u32_be(YAZ_MAGIC).unwrap();
  cursor.write_u32_be(data.len() as u32).unwrap();
  cursor.write_u32_be(0u32).unwrap();
  cursor.write_u32_be(0u32).unwrap();

  let mut eight_count = 0;
  for b in data {
    if eight_count == 0 {
      cursor.write_u8(0xFF).unwrap();
    }

    cursor.write_u8(*b).unwrap();
    eight_count = eight_count + 1;
    if eight_count == 8 {
      eight_count = 0;
    }
  }

  buf
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn test_compress() {
    let data: Vec<_> = (0..20).map(|i| i as u8).collect();

    let compressed = compress(&data);
    let decompressed = decompress(&compressed).unwrap();

    assert_eq!(data, decompressed);
  }
}

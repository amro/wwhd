use std::collections::HashMap;
use std::fs::File;
use std::io::Cursor;
use std::path::PathBuf;

use crate::formats::*;

use self::event_list::EventList;
use self::msbt::Msbt;
use self::rpx::Rpx;

use super::dzx::Dzx;

struct SarcFres {
  name: String,
  inner: String,
  fres: FRES,
  sarc: Sarc,
  pack: Option<usize>,
  out: PathBuf,
}

struct SarcEntry {
  sarc: Sarc,
  name: String,
  pack: Option<usize>,
}

struct Paths {
  source: PathBuf,
  source_pack: PathBuf,
  source_stage: PathBuf,
  dest: PathBuf,
  dest_pack: PathBuf,
  dest_stage: PathBuf,
}

impl Paths {
  fn sarc(&self, name: &str) -> PathBuf {
    self.source_stage.join(name)
  }

  fn sarc_out(&self, name: &str) -> PathBuf {
    self.dest_stage.join(name)
  }

  fn pack(&self, name: &str) -> PathBuf {
    self.source_pack.join(name)
  }

  fn pack_out(&self, name: &str) -> PathBuf {
    self.dest_pack.join(name)
  }

  fn unpack_out(&self, name: &str) -> PathBuf {
    if name.contains("message") {
      self
        .dest
        .join("content")
        .join("Cafe")
        .join("US")
        .join("Message")
        .join("UsEnglish")
        .join(name)
    } else {
      self.dest_stage.join(name)
    }
  }
}

pub struct FileManager {
  dump: bool,
  rpx: Rpx,
  pack_lookup: PackLookup,
  paths: Paths,

  packs: Vec<Sarc>,
  packs2: Vec<(String, Sarc)>,
  sarc: Vec<SarcFres>,
  sarc2: Vec<SarcEntry>,
  msbt: Vec<(usize, Msbt)>,
  dzx: Vec<(usize, Dzx)>,
  event_lists: Vec<(usize, EventList)>,

  fres_lookup: HashMap<String, usize>,
  dzx_lookup: HashMap<&'static str, usize>,
  msbt_lookup: HashMap<&'static str, usize>,
  event_list_lookup: HashMap<&'static str, usize>,
}

pub struct PackLookup {
  names: HashMap<&'static str, &'static str>,
}

impl PackLookup {
  pub fn new() -> Self {
    let mut names = HashMap::new();
    names.insert("sea_Stage.szs", "first_szs_permanent.pack");
    names.insert("sea_Room0.szs", "szs_permanent1.pack");
    names.insert("sea_Room1.szs", "szs_permanent1.pack");
    names.insert("sea_Room11.szs", "szs_permanent1.pack");
    names.insert("sea_Room13.szs", "szs_permanent1.pack");
    names.insert("sea_Room44.szs", "szs_permanent2.pack");
    names.insert("message_msbt.szs", "permanent_2d_UsEnglish.pack");
    names.insert("message2_msbt.szs", "permanent_2d_UsEnglish.pack");
    names.insert("Main_00_msbt.szs", "permanent_2d_UsEnglish.pack");

    PackLookup { names }
  }

  pub fn lookup(&self, path: &str) -> Option<&str> {
    self.names.get(path).copied()
  }
}

fn dzx_name(file: &str) -> &str {
  if file.contains("Stage") {
    "stage.dzs"
  } else {
    "room.dzr"
  }
}

fn msbt_name(name: &str) -> String {
  if let Some(idx) = name.find("_msbt.szs") {
    return format!("{}.msbt", &name[00..idx]);
  }
  panic!("noname: {}", name);
}

fn dzx_names(name: &str) -> (String, &str) {
  if let Some(idx) = name.find("Room") {
    (format!("{}.bfres", &name[idx..name.len() - 4]), "room.dzr")
  } else {
    ("Stage.bfres".into(), "stage.dzs")
  }
}

#[allow(dead_code)]
impl FileManager {
  pub fn new<T: Into<PathBuf>>(source: T, dest: T) -> Self {
    let source: PathBuf = source.into();
    let source_pack = source.join("content").join("Common").join("Pack");
    let source_stage = source.join("content").join("Common").join("Stage");

    let dest: PathBuf = dest.into();
    let dest_pack = dest.join("content").join("Common").join("Pack");
    let dest_stage = dest.join("content").join("Common").join("Stage");

    let rpx_input = source.join("code").join("cking.rpx");
    let rpx = Rpx::read_from(&mut File::open(rpx_input).unwrap()).unwrap();
    FileManager {
      paths: Paths {
        source,
        source_pack,
        source_stage,
        dest,
        dest_pack,
        dest_stage,
      },
      dump: true,
      rpx,
      packs: Vec::new(),
      packs2: Vec::new(),
      sarc: Vec::new(),
      dzx: Vec::new(),
      sarc2: Vec::new(),
      msbt: Vec::new(),
      event_lists: Vec::new(),
      fres_lookup: HashMap::new(),
      dzx_lookup: HashMap::new(),
      msbt_lookup: HashMap::new(),
      event_list_lookup: HashMap::new(),
      pack_lookup: PackLookup::new(),
    }
  }

  pub fn get_rpx_mut(&mut self) -> &mut Rpx {
    &mut self.rpx
  }

  fn load_sarc(&mut self, name: &'static str) -> Result<usize, FormatError> {
    let bfres_name = if let Some(idx) = name.find("Room") {
      let inner = format!("{}.bfres", &name[idx..name.len() - 4]);
      inner
    } else {
      "Stage.bfres".into()
    };
    if let Some(pack_path) = self.pack_lookup.lookup(name) {
      log::info!("Opening {:#?}", self.paths.pack(pack_path));
      let pack = Sarc::from_path(self.paths.pack(pack_path))?;
      let pack_id = self.packs.len();
      log::info!("Opening subpack {:#?}", name);
      if let Some(sd) = pack.get_file(name) {
        let sarc = Sarc::from_buffer(sd.into())?;
        let sarc_id = self.sarc.len();

        if let Some(sif) = sarc.get_file(&bfres_name) {
          let fres = FRES::from_data(sif.into())?;
          self.sarc.push(SarcFres {
            name: name.into(),
            inner: bfres_name,
            fres,
            pack: Some(pack_id),
            sarc,
            out: self.paths.sarc_out(name),
          });
          self.packs.push(pack);
          Ok(sarc_id)
        } else {
          panic!("no fres found in pack")
        }
      } else {
        panic!("no file found in pack")
      }
    } else {
      let sarc_input = self.paths.sarc(name);
      log::info!("Opening {:#?}", sarc_input);
      let sarc = Sarc::from_path(sarc_input)?;
      let sarc_id = self.sarc.len();
      if let Some(sif) = sarc.get_file(&bfres_name) {
        let fres = FRES::from_data(sif.into())?;
        self.sarc.push(SarcFres {
          name: name.into(),
          inner: bfres_name,
          fres,
          pack: None,
          sarc,
          out: self.paths.sarc_out(name),
        });
        Ok(sarc_id)
      } else {
        panic!("no fres found")
      }
    }
  }

  fn load_sarc_2(&mut self, name: &'static str) -> Result<usize, FormatError> {
    if let Some(pack_path) = self.pack_lookup.lookup(name) {
      log::info!("Opening {:#?}", self.paths.pack(pack_path));
      let pack = Sarc::from_path(self.paths.pack(pack_path))?;
      let pack_id = self.packs2.len();
      log::info!("Opening subpack {:#?}", name);
      if let Some(sd) = pack.get_file(name) {
        let sarc = Sarc::from_buffer(sd.into())?;
        let sarc_id = self.sarc2.len();

        self.sarc2.push(SarcEntry {
          sarc,
          name: name.into(),
          pack: Some(pack_id),
        });
        self.packs2.push((pack_path.into(), pack));
        return Ok(sarc_id);
      }
      panic!("no file found in pack")
    } else {
      let sarc_input = self.paths.sarc(name);
      log::info!("Opening {:#?}", sarc_input);
      let sarc = Sarc::from_path(sarc_input)?;
      let sarc_id = self.sarc.len();

      self.sarc2.push(SarcEntry {
        sarc,
        name: name.into(),
        pack: None,
      });
      Ok(sarc_id)
    }
  }

  pub fn get_msbt(&mut self, name: &'static str) -> Result<&mut Msbt, FormatError> {
    if let Some(id) = self.msbt_lookup.get(name) {
      Ok(&mut self.msbt[*id].1)
    } else {
      let inner = msbt_name(name);
      let sarc_id = self.load_sarc_2(name)?;
      let mut cs = Cursor::new(self.sarc2[sarc_id].sarc.get_file(&inner).unwrap());
      let msbt = Msbt::from_reader(&mut cs)?;
      let msbt_id = self.msbt.len();
      self.msbt.push((sarc_id, msbt));
      self.msbt_lookup.insert(name, msbt_id);
      Ok(&mut self.msbt.get_mut(msbt_id).unwrap().1)
    }
  }

  pub fn get_dzx(&mut self, name: &'static str) -> Result<&mut Dzx, FormatError> {
    if let Some(id) = self.dzx_lookup.get(name) {
      Ok(&mut self.dzx[*id].1)
    } else {
      let bfres_name = if let Some(idx) = name.find("Room") {
        let inner = format!("{}.bfres", &name[idx..name.len() - 4]);
        inner
      } else {
        "Stage.bfres".into()
      };
      let embedded_file_name = dzx_name(&bfres_name);
      if let Some(fres_id) = self.fres_lookup.get(&bfres_name) {
        let fres = &self.sarc[*fres_id].fres;
        let mut cs = Cursor::new(fres.embedded_file(embedded_file_name));
        let dzx = Dzx::from_reader(&mut cs)?;
        let dzx_id = self.dzx.len();
        self.dzx.push((*fres_id, dzx));
        self.dzx_lookup.insert(name, dzx_id);
        Ok(&mut self.dzx.get_mut(dzx_id).unwrap().1)
      } else {
        let sarc_id = self.load_sarc(name)?;
        let mut cs = Cursor::new(self.sarc[sarc_id].fres.embedded_file(embedded_file_name));
        let dzx = Dzx::from_reader(&mut cs)?;
        let dzx_id = self.dzx.len();
        self.dzx.push((sarc_id, dzx));
        self.dzx_lookup.insert(name, dzx_id);
        Ok(&mut self.dzx.get_mut(dzx_id).unwrap().1)
      }
    }
  }

  pub fn get_event_list(&mut self, name: &'static str) -> Result<&mut EventList, FormatError> {
    if let Some(id) = self.event_list_lookup.get(name) {
      Ok(&mut self.event_lists[*id].1)
    } else {
      let bfres_name = if let Some(idx) = name.find("Room") {
        let inner = format!("{}.bfres", &name[idx..name.len() - 4]);
        inner
      } else {
        "Stage.bfres".into()
      };
      if let Some(fres_id) = self.fres_lookup.get(&bfres_name) {
        let fres = &self.sarc[*fres_id].fres;
        let mut cs = Cursor::new(fres.embedded_file("event_list.dat"));
        let event_list = EventList::read_from(&mut cs)?;
        let event_list_id = self.event_lists.len();
        self.event_lists.push((*fres_id, event_list));
        self.event_list_lookup.insert(name, event_list_id);
        Ok(&mut self.event_lists.get_mut(event_list_id).unwrap().1)
      } else {
        let sarc_id = self.load_sarc(name)?;
        let mut cs = Cursor::new(self.sarc[sarc_id].fres.embedded_file("event_list.dat"));
        let event_list = EventList::read_from(&mut cs)?;
        let event_list_id = self.event_lists.len();
        self.event_lists.push((sarc_id, event_list));
        self.event_list_lookup.insert(name, event_list_id);
        Ok(&mut self.event_lists.get_mut(event_list_id).unwrap().1)
      }
    }
  }

  pub fn save(mut self) -> Result<(), FormatError> {
    for (name, dzx_id) in self.dzx_lookup {
      let mut new_dzx = Vec::new();
      let (sarc_id, dzx) = self.dzx.get_mut(dzx_id).unwrap();
      dzx.write(&mut Cursor::new(&mut new_dzx))?;
      // dzx.write(&mut File::create(format!("dumps/dzx/{}", name))?)?;
      let name = dzx_name(name);
      self.sarc[*sarc_id].fres.prep_update(name, new_dzx);
    }
    for (_, event_list_id) in self.event_list_lookup {
      let mut new_event_list = Vec::new();
      let (sarc_id, event_list) = self.event_lists.get_mut(event_list_id).unwrap();
      event_list.write(&mut Cursor::new(&mut new_event_list))?;
      self.sarc[*sarc_id].fres.prep_update("event_list.dat", new_event_list);
    }

    for (name, msbt_id) in self.msbt_lookup {
      let mut new_msbt = Vec::new();
      let (sarc_id, msbt) = self.msbt.get_mut(msbt_id).unwrap();
      msbt.write(&mut Cursor::new(&mut new_msbt))?;
      // msbt.write(&mut File::create(format!("dumps/msbt/{}", msbt_name(name)))?)?;
      self.sarc2[*sarc_id].sarc.prep_update(&msbt_name(name), new_msbt);
    }
    for s in self.sarc2 {
      if let Some(pack_id) = s.pack {
        // self.packs2[pack_id].1.prep_removal(&s.name);
        let sarc_output = self.paths.unpack_out(&s.name);
        log::info!("Writing {:#?} outside of pack {}", &sarc_output, &s.name);

        let mut new_sarc = Vec::new();
        // let mut new_sarc = File::create(sarc_output)?;
        s.sarc.write_with_updates(&mut Cursor::new(&mut new_sarc), true)?;
        self.packs2[pack_id].1.prep_update(&s.name, new_sarc);
        // sarc_output
      } else {
        let sarc_output = self.paths.sarc_out(&s.name);
        log::info!("Writing {:#?}", &sarc_output);

        let mut new_sarc = File::create(sarc_output)?;
        s.sarc.write_with_updates(&mut new_sarc, true)?;
        // sarc_output
      }
    }

    for (path, pack) in self.packs2 {
      let pack_output = self.paths.pack_out(&path);
      log::info!("Writing {:#?}", &pack_output);
      let mut new_pack = File::create(pack_output)?;
      pack.write_with_updates(&mut new_pack, false)?;
    }

    for sf in self.sarc {
      let mut new_fres = Vec::new();
      sf.fres.write_with_updates(&mut Cursor::new(&mut new_fres))?;
      let sarc_output = sf.out;
      if let Some(pack_id) = sf.pack {
        // ideally, we'd just write this file back to the pack it came from
        // but that seems to cause the game to run out of memory, probably
        // because the packs are always in memory and we're not compressing
        // it. so instead, remove the file from the pack so the game looks
        // for it in a regular directory instead
        let sarc_output = self.paths.sarc_out(&sf.name);
        log::info!("Writing {:#?} outside of pack", &sarc_output);
        let mut new_sarc = File::create(sarc_output)?;
        sf.sarc.write_with_update(&mut new_sarc, &sf.inner, &new_fres, true)?;
        let ppath = self.pack_lookup.lookup(&sf.name).unwrap();
        let pack_output = self.paths.pack_out(ppath);
        log::info!("Writing {:#?} without {}", &pack_output, &sf.name);
        let pack = &mut self.packs[pack_id];
        let mut new_pack = File::create(pack_output)?;
        pack.write_without_file(&mut new_pack, &sf.name, false)?;
      } else {
        log::info!("Writing {:#?}:{}", &sarc_output, &sf.inner);
        let mut new_sarc = File::create(sarc_output)?;
        sf.sarc.write_with_update(&mut new_sarc, &sf.inner, &new_fres, true)?;
      }
    }
    Ok(())
  }

  pub fn save_rpx(&mut self) -> Result<(), FormatError> {
    let rpx_output = self.paths.dest.join("code").join("cking.rpx");
    let mut output = File::create(rpx_output)?;
    self.rpx.write(&mut output)?;
    Ok(())
  }
}

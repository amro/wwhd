use byteorder::BigEndian;
use byteorder::ReadBytesExt;
use byteorder::WriteBytesExt;

use std::fmt;
use std::io::Read;
use std::io::Seek;
use std::io::Write;
use std::ops::Add;
use std::ops::Rem;
use std::ops::Sub;

use std::io::Error;
use std::string::FromUtf16Error;
use std::string::FromUtf8Error;

pub trait Align {
  fn align(self, to: Self) -> Self;
  fn padding(self, to: Self) -> Self;
}

pub trait Pad {
  fn pad(&mut self, to: u32) -> Result<(), Error> {
    self.pad_with(0x00, to)
  }
  fn pad_with(&mut self, b: u8, to: u32) -> Result<(), Error>;
}

pub trait AlignTo {
  fn align_to(&mut self, to: u32) -> Result<(), Error>;
}

impl<T: Write + Seek> Pad for T {
  fn pad_with(&mut self, b: u8, to: u32) -> Result<(), Error> {
    let padding = self.stream_position()?.padding(to as u64);
    for _ in 0..padding {
      WriteBytesExt::write_u8(self, b)?;
    }
    Ok(())
  }
}

impl<T: Read + Seek> AlignTo for T {
  fn align_to(&mut self, to: u32) -> Result<(), Error> {
    let padding = self.stream_position().unwrap().padding(to as u64);
    self.seek(std::io::SeekFrom::Current(padding as i64))?;
    Ok(())
  }
}

impl<T: Add<Output = T> + Sub<Output = T> + Rem<Output = T> + Copy> Align for T {
  fn align(self, to: T) -> T {
    self + self.padding(to)
  }

  fn padding(self, to: T) -> T {
    (to - (self % to)) % to
  }
}

#[derive(Debug)]
pub enum FormatError {
  BadMagic,
  IO(Error),
  Corrupt(String),
}

impl fmt::Display for FormatError {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "FormatError::{:?}", self)
  }
}

impl std::error::Error for FormatError {}

impl From<Error> for FormatError {
  fn from(e: Error) -> Self {
    FormatError::IO(e)
  }
}

impl From<FromUtf8Error> for FormatError {
  fn from(e: FromUtf8Error) -> Self {
    FormatError::Corrupt(e.to_string())
  }
}

impl From<FromUtf16Error> for FormatError {
  fn from(e: FromUtf16Error) -> Self {
    FormatError::Corrupt(e.to_string())
  }
}

pub trait BeRead: ReadBytesExt {
  fn read_u8(&mut self) -> Result<u8, Error> {
    let mut buf = [0; 1];
    self.read_exact(&mut buf)?;
    Ok(buf[0])
  }

  fn read_u16_be(&mut self) -> Result<u16, Error> {
    self.read_u16::<BigEndian>()
  }

  fn read_i16_be(&mut self) -> Result<i16, Error> {
    self.read_i16::<BigEndian>()
  }

  fn read_u32_be(&mut self) -> Result<u32, Error> {
    self.read_u32::<BigEndian>()
  }

  fn read_i32_be(&mut self) -> Result<i32, Error> {
    self.read_i32::<BigEndian>()
  }

  fn read_f32_be(&mut self) -> Result<f32, Error> {
    self.read_f32::<BigEndian>()
  }
}

pub trait BeWrite: WriteBytesExt {
  fn write_u8(&mut self, v: u8) -> Result<(), Error> {
    self.write_all(&[v])
  }

  fn write_u16_be(&mut self, v: u16) -> Result<(), Error> {
    self.write_u16::<BigEndian>(v)
  }

  fn write_i16_be(&mut self, v: i16) -> Result<(), Error> {
    self.write_i16::<BigEndian>(v)
  }

  fn write_u32_be(&mut self, v: u32) -> Result<(), Error> {
    self.write_u32::<BigEndian>(v)
  }

  fn write_i32_be(&mut self, v: i32) -> Result<(), Error> {
    self.write_i32::<BigEndian>(v)
  }

  fn write_f32_be(&mut self, v: f32) -> Result<(), Error> {
    self.write_f32::<BigEndian>(v)
  }
}

impl<R: std::io::Read + ?Sized> BeRead for R {}
impl<W: std::io::Write + ?Sized> BeWrite for W {}

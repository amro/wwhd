use bitflags::bitflags;
use flate2::bufread::ZlibDecoder;
use lazy_static::lazy_static;

use crate::formats::bytes::*;

use std::ffi::CStr;
use std::io::BufRead;
use std::io::Cursor;
use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Write;

lazy_static! {
  static ref CRC_TABLE: [u32; 256] = {
    let mut crc_table = [0u32; 256];
    for i in 0..256u32 {
      let mut c = i;
      for _ in 0..8 {
        if (c & 1) == 1 {
          c = 0xedb88320 ^ (c >> 1);
        } else {
          c >>= 1;
        }
      }
      crc_table[i as usize] = c;
    }
    crc_table
  };
}

bitflags! {
  struct ShFlags: u32 {
    const SHF_WRITE = 0x1;
    const SHF_ALLOC = 0x2;
    const SHF_EXECINSTR = 0x4;
    const SHF_RPL_COMPRESSED = 0x08000000;
    const SHF_TLS = 0x04000000;
  }
}

#[allow(dead_code)]
#[derive(Eq, PartialEq, Debug, Clone, Copy)]
#[repr(u32)]
enum ShType {
  Null = 0x0,
  /// Program data
  Progbits = 0x1,
  /// Symbol table
  Symtab = 0x2,
  /// String table
  Strtab = 0x3,
  /// Relocation entries with addends
  Rela = 0x4,
  /// Symbol hash table
  Hash = 0x5,
  /// Dynamic linking information
  Dynamic = 0x6,
  /// Notes
  Note = 0x7,
  /// Program space with no data (bss)
  Nobits = 0x8,
  /// Relocation entries, no addends
  Rel = 0x9,
  /// Reserved
  Shlib = 0x0a,
  /// Dynamic linker symbol table
  Dynsym = 0x0b,
  /// Array of constructors
  InitArray = 0x0e,
  /// Array of destructors
  FiniArray = 0x0f,
  /// Array of pre-constructors
  PreinitArray = 0x10,
  /// Section group
  Group = 0x11,
  /// Extended section indices
  SymtabShndx = 0x12,
  /// Number of defined types.
  Num = 0x13,

  RplExports = 0x80000001,
  RplImports = 0x80000002,
  RplCrcs = 0x80000003,
  RplFileInfo = 0x80000004,
}

impl From<u32> for ShType {
  fn from(u: u32) -> ShType {
    if u > 0x13 && u < 0x80000001 && u > 0x80000004 {
      panic!("Invalid ShType");
    }
    unsafe { std::mem::transmute(u) }
  }
}

#[rustfmt::skip]
#[derive(Debug)]
struct FileInfo {
  magic:             u32,
  text_size:         u32,
  text_align:        u32,
  data_size:         u32,
  data_align:        u32,
  load_size:         u32,
  load_align:        u32,
  temp_size:         u32,
  tramp_adjust:      u32,
  sdata_base1:       u32,
  sdata_base2:       u32,
  stack_size:        u32,
  filename:          u32,
  flags:             u32,
  heap_size:         u32,
  tag_offset:        u32,
  min_version:       u32,
  compression_level: u32,
  tramp_addition:    u32,
  file_info_pad:     u32,
  sdk_version:       u32,
  sdk_revision:      u32,
  tls_module_index:  i16,
  tls_align_shift:   u16,
  runtime_size:      u32,
}

impl FileInfo {
  #[rustfmt::skip]
  pub fn read_from<R: Read + Seek>(reader: &mut R) -> Result<Self, FormatError> {
    Ok(FileInfo {
      magic:             reader.read_u32_be()?,
      text_size:         reader.read_u32_be()?,
      text_align:        reader.read_u32_be()?,
      data_size:         reader.read_u32_be()?,
      data_align:        reader.read_u32_be()?,
      load_size:         reader.read_u32_be()?,
      load_align:        reader.read_u32_be()?,
      temp_size:         reader.read_u32_be()?,
      tramp_adjust:      reader.read_u32_be()?,
      sdata_base1:       reader.read_u32_be()?,
      sdata_base2:       reader.read_u32_be()?,
      stack_size:        reader.read_u32_be()?,
      filename:          reader.read_u32_be()?,
      flags:             reader.read_u32_be()?,
      heap_size:         reader.read_u32_be()?,
      tag_offset:        reader.read_u32_be()?,
      min_version:       reader.read_u32_be()?,
      compression_level: reader.read_u32_be()?,
      tramp_addition:    reader.read_u32_be()?,
      file_info_pad:     reader.read_u32_be()?,
      sdk_version:       reader.read_u32_be()?,
      sdk_revision:      reader.read_u32_be()?,
      tls_module_index:  reader.read_i16_be()?,
      tls_align_shift:   reader.read_u16_be()?,
      runtime_size:      reader.read_u32_be()?,
    })
  }

  pub fn write<W: Write + Seek>(&self, writer: &mut W) -> Result<(), FormatError> {
    writer.write_u32_be(self.magic)?;
    writer.write_u32_be(self.text_size)?;
    writer.write_u32_be(self.text_align)?;
    writer.write_u32_be(self.data_size)?;
    writer.write_u32_be(self.data_align)?;
    writer.write_u32_be(self.load_size)?;
    writer.write_u32_be(self.load_align)?;
    writer.write_u32_be(self.temp_size)?;
    writer.write_u32_be(self.tramp_adjust)?;
    writer.write_u32_be(self.sdata_base1)?;
    writer.write_u32_be(self.sdata_base2)?;
    writer.write_u32_be(self.stack_size)?;
    writer.write_u32_be(self.filename)?;
    writer.write_u32_be(self.flags)?;
    writer.write_u32_be(self.heap_size)?;
    writer.write_u32_be(self.tag_offset)?;
    writer.write_u32_be(self.min_version)?;
    writer.write_u32_be(self.compression_level)?;
    writer.write_u32_be(self.tramp_addition)?;
    writer.write_u32_be(self.file_info_pad)?;
    writer.write_u32_be(self.sdk_version)?;
    writer.write_u32_be(self.sdk_revision)?;
    writer.write_i16_be(self.tls_module_index)?;
    writer.write_u16_be(self.tls_align_shift)?;
    writer.write_u32_be(self.runtime_size)?;
    Ok(())
  }
}

#[derive(Debug, Clone)]
#[repr(C)]
struct SectionHeader {
  name: u32,
  section_type: ShType,
  flags: ShFlags,
  addr: u32,
  offset: u32,
  size: u32,
  link: u32,
  target_section: u32,
  align: u32,
  entry_size: u32,
}

#[derive(Debug)]
struct SymtabEntry {
  uknown00: u32,
  symbol_address: u32,
  uknown08: u32,
  info: u8,
  uknown0d: u8,
  section_index: u16,
}

impl SymtabEntry {
  #[rustfmt::skip]
  pub fn read_from<R: Read + Seek>(reader: &mut R) -> Result<Self, FormatError> {
    Ok(SymtabEntry {
      uknown00:       reader.read_u32_be()?,
      symbol_address: reader.read_u32_be()?,
      uknown08:       reader.read_u32_be()?,
      info:           reader.read_u8()?,
      uknown0d:       reader.read_u8()?,
      section_index:  reader.read_u16_be()?,
    })
  }

  pub fn write<W: Write>(&self, writer: &mut W) -> Result<(), FormatError> {
    writer.write_u32_be(self.uknown00)?;
    writer.write_u32_be(self.symbol_address)?;
    writer.write_u32_be(self.uknown08)?;
    writer.write_u8(self.info)?;
    writer.write_u8(self.uknown0d)?;
    writer.write_u16_be(self.section_index)?;
    Ok(())
  }
}

#[repr(u8)]
enum RelocType {
  RelocAddr32 = 1,
  RelocLo16 = 4,
  RelocHi16 = 5,
  RelocHa16 = 6,
  RelocRel24 = 10,
  RelocRel14 = 11,
  DtpMod32 = 68,
  DtpRel32 = 78,
  Rel16Ha = 251,
  Rel16Hi = 252,
  Rel16Lo = 253,
}

impl From<u8> for RelocType {
  fn from(u: u8) -> RelocType {
    match u {
      1 => RelocType::RelocAddr32,
      4 => RelocType::RelocLo16,
      5 => RelocType::RelocHi16,
      6 => RelocType::RelocHa16,
      10 => RelocType::RelocRel24,
      11 => RelocType::RelocRel14,
      68 => RelocType::DtpMod32,
      78 => RelocType::DtpRel32,
      251 => RelocType::Rel16Ha,
      252 => RelocType::Rel16Hi,
      253 => RelocType::Rel16Lo,
      _ => unimplemented!(),
    }
  }
}

#[derive(Debug)]
struct Reloc {
  offset: u32,
  symbol: u32,
  addend: u32,
}

impl Reloc {
  pub fn read_from<R: Read + Seek>(reader: &mut R) -> Result<Self, FormatError> {
    Ok(Reloc {
      offset: reader.read_u32_be()?,
      symbol: reader.read_u32_be()?,
      addend: reader.read_u32_be()?,
    })
  }

  pub fn write<W: Write>(&self, writer: &mut W) -> Result<(), FormatError> {
    writer.write_u32_be(self.offset)?;
    writer.write_u32_be(self.symbol)?;
    writer.write_u32_be(self.addend)?;
    Ok(())
  }

  pub fn type_and_symbol(&self) -> (RelocType, u32) {
    let reloc_type = (self.symbol & 0xff) as u8;
    let reloc_symbol = self.symbol >> 8;
    (reloc_type.into(), reloc_symbol)
  }

  pub fn set_type_and_symbol(&mut self, reloc_type: RelocType, symbol: u32) {
    self.symbol = (reloc_type as u32) | symbol << 8;
  }
}

impl SectionHeader {
  pub fn bytes(&self) -> Vec<u8> {
    let mut buf = Vec::with_capacity(std::mem::size_of::<SectionHeader>());
    let mut cs = Cursor::new(&mut buf);
    self.write(&mut cs).unwrap();
    buf
  }

  #[rustfmt::skip]
  pub fn read_from<R: Read + Seek>(reader: &mut R) -> Result<Self, FormatError> {
    Ok(SectionHeader {
      name:           reader.read_u32_be()?,
      section_type:   reader.read_u32_be()?.into(),
      flags:          ShFlags::from_bits(reader.read_u32_be()?).unwrap(),
      addr:           reader.read_u32_be()?,
      offset:         reader.read_u32_be()?,
      size:           reader.read_u32_be()?,
      link:           reader.read_u32_be()?,
      target_section: reader.read_u32_be()?,
      align:          reader.read_u32_be()?,
      entry_size:     reader.read_u32_be()?,
    })
  }

  pub fn write<W: Write>(&self, writer: &mut W) -> Result<(), FormatError> {
    writer.write_u32_be(self.name)?;
    writer.write_u32_be(self.section_type as u32)?;
    writer.write_u32_be(self.flags.bits())?;
    writer.write_u32_be(self.addr)?;
    writer.write_u32_be(self.offset)?;
    writer.write_u32_be(self.size)?;
    writer.write_u32_be(self.link)?;
    writer.write_u32_be(self.target_section)?;
    writer.write_u32_be(self.align)?;
    writer.write_u32_be(self.entry_size)?;
    Ok(())
  }
}

const MAGIC_ELF: u32 = 0x7f454c46;
const MAGIC_FILEINFO: u32 = 0xcafe0402;

#[rustfmt::skip]
#[derive(Debug)]
struct ElfHeader {
  magic:       u32,
  class:       u8,
  endianness:  u8,
  version:     u8,
  abi:         u8,
  abi_version: u8,
  etype:       u16,
  machine:     u16,
  version2:    u32,
  entry_point: u32,
  ph_off:      u32,
  sh_off:      u32,
  flags:       u32,
  header_size: u16,
  ph_size:     u16,
  ph_num:      u16,
  sh_size:     u16,
  sh_num:      u16,
  sh_str_idx:  u16,
}

impl ElfHeader {
  #[rustfmt::skip]
  pub fn read_from<R: Read + Seek>(reader: &mut R) -> Result<Self, FormatError> {
    let magic = reader.read_u32_be()?;
    if magic != MAGIC_ELF {
      return Err(FormatError::BadMagic);
    }
    let class = reader.read_u8()?;
    if class != 0x1 {
      return Err(FormatError::BadMagic);
    }

    let endianness = reader.read_u8()?;
    if endianness != 0x2 {
      return Err(FormatError::BadMagic);
    }

    let version     = reader.read_u8()?;
    let abi         = reader.read_u8()?;
    let abi_version = reader.read_u8()?;

    reader.align_to(0x10)?;

    let etype       = reader.read_u16_be()?;
    let machine     = reader.read_u16_be()?;

    let version2    = reader.read_u32_be()?;
    let entry_point = reader.read_u32_be()?;

    let ph_off      = reader.read_u32_be()?;
    let sh_off      = reader.read_u32_be()?;
    let flags       = reader.read_u32_be()?;
    let header_size = reader.read_u16_be()?;
    let ph_size     = reader.read_u16_be()?;
    let ph_num      = reader.read_u16_be()?;

    let sh_size     = reader.read_u16_be()?;
    let sh_num      = reader.read_u16_be()?;
    let sh_str_idx  = reader.read_u16_be()?;

    Ok(ElfHeader {
      magic,
      class,
      endianness,
      version,
      abi,
      abi_version,
      etype,
      machine,
      version2,
      entry_point,
      ph_off,
      sh_off,
      flags,
      header_size,
      ph_size,
      ph_num,
      sh_size,
      sh_num,
      sh_str_idx,
    })
  }

  pub fn write<W: Write + Seek>(&self, writer: &mut W) -> Result<(), FormatError> {
    writer.write_u32_be(self.magic)?;
    writer.write_u8(self.class)?;
    writer.write_u8(self.endianness)?;
    writer.write_u8(self.version)?;
    writer.write_u8(self.abi)?;
    writer.write_u8(self.abi_version)?;
    writer.pad(0x10)?;
    writer.write_u16_be(self.etype)?;
    writer.write_u16_be(self.machine)?;
    writer.write_u32_be(self.version2)?;
    writer.write_u32_be(self.entry_point)?;
    writer.write_u32_be(self.ph_off)?;
    writer.write_u32_be(self.sh_off)?;
    writer.write_u32_be(self.flags)?;

    writer.write_u16_be(self.header_size)?;
    writer.write_u16_be(self.ph_size)?;
    writer.write_u16_be(self.ph_num)?;

    writer.write_u16_be(self.sh_size)?;
    writer.write_u16_be(self.sh_num)?;
    writer.write_u16_be(self.sh_str_idx)?;
    Ok(())
  }
}

struct Imports {
  nentries: u32,
  unknown: u32,
  name: String,
}

enum Section {
  Null,
  Text(Vec<u8>),
  Reloc(Vec<Reloc>),
  Symtab(Vec<SymtabEntry>),
  Strtab(Vec<u8>),
  Imports(Imports),
  Crc(Vec<u32>),
  FileInfo(FileInfo),
  Other(Vec<u8>),
}

fn read_vec<R: Read>(reader: &mut R, size: usize) -> Result<Vec<u8>, FormatError> {
  let mut buf = vec![0; size];
  // buf.fill(0);
  reader.read_exact(&mut buf)?;
  Ok(buf)
}

impl Section {
  pub fn read_rela<R: Read + Seek>(header: &SectionHeader, reader: &mut R) -> Result<Section, FormatError> {
    let mut relocs = Vec::new();

    for _ in 0..header.size / header.entry_size {
      relocs.push(Reloc::read_from(reader)?);
    }

    Ok(Section::Reloc(relocs))
  }

  #[rustfmt::skip]
  pub fn read_symtab<R: Read + Seek>(header: &SectionHeader, reader: &mut R) -> Result<Section, FormatError> {
    let mut entries = Vec::new();

    for _ in 0..header.size / header.entry_size {
      entries.push(SymtabEntry::read_from(reader)?);
    }

    Ok(Section::Symtab(entries))
  }

  pub fn read_crc<R: Read + Seek>(header: &SectionHeader, reader: &mut R) -> Result<Section, FormatError> {
    let mut entries = Vec::new();

    for _ in 0..header.size / header.entry_size {
      entries.push(reader.read_u32_be()?)
    }

    Ok(Section::Crc(entries))
  }

  pub fn read_imports<R: BufRead + Seek>(header: &SectionHeader, reader: &mut R) -> Result<Section, FormatError> {
    let nentries = reader.read_u32_be()?;
    let unknown = reader.read_u32_be()?;
    let mut name = Vec::new();
    reader.read_until(0, &mut name)?;

    assert_eq!(header.size - 8, nentries * 8, "{}", nentries);
    Ok(Section::Imports(Imports {
      nentries,
      unknown,
      name: String::from_utf8(name)?,
    }))
  }

  pub fn read_from<R: Read + Seek>(header: &mut SectionHeader, reader: &mut R) -> Result<(Self, u32), FormatError> {
    let size = header.size as usize;
    let data = if header.flags.contains(ShFlags::SHF_RPL_COMPRESSED) {
      let deflated_size = reader.read_u32_be()?;
      header.size = deflated_size;
      let data = read_vec(reader, size - 4)?;
      let mut decoder = ZlibDecoder::new(&data[..]);
      header.flags.set(ShFlags::SHF_RPL_COMPRESSED, false);
      read_vec(&mut decoder, deflated_size as usize)?
    } else if header.offset > 0 {
      read_vec(reader, size)?
    } else {
      Vec::new()
    };
    let crc = crc32(0, &data);
    let mut cs = Cursor::new(&data);
    let section = match header.section_type {
      ShType::Null => todo!(),
      ShType::Progbits => Section::Text(data),
      ShType::Symtab => Section::read_symtab(header, &mut cs)?,
      ShType::Rela => Section::read_rela(header, &mut cs)?,
      ShType::Strtab => Section::Strtab(data),
      ShType::RplExports => todo!(),
      ShType::RplImports => Section::read_imports(header, &mut cs)?,
      ShType::RplCrcs => Section::read_crc(header, &mut cs)?,
      ShType::RplFileInfo => Section::FileInfo(FileInfo::read_from(&mut cs)?),
      _ => Section::Other(data),
    };

    Ok((section, crc))
  }

  pub fn write<W: Write + Seek>(&self, writer: &mut W) -> Result<(), FormatError> {
    match self {
      Section::Null => {}
      Section::Reloc(relocs) => {
        for reloc in relocs {
          reloc.write(writer)?;
        }
      }
      Section::Symtab(entries) => {
        for entry in entries {
          entry.write(writer)?;
        }
      }
      Section::Imports(imports) => {
        writer.write_u32_be(imports.nentries)?;
        writer.write_u32_be(imports.unknown)?;
        writer.write_all(imports.name.as_bytes())?;
        writer.write_u8(0x0)?;
        writer.pad(8 * imports.nentries)?;
      }
      Section::Crc(crcs) => {
        for crc in crcs {
          writer.write_u32_be(*crc)?;
        }
      }
      Section::FileInfo(file_info) => file_info.write(writer)?,
      Section::Text(data) | Section::Strtab(data) | Section::Other(data) => writer.write_all(data)?,
    }
    Ok(())
  }
}

pub struct Rpx {
  header: ElfHeader,
  headers: Vec<SectionHeader>,
  sections: Vec<Section>,
  checksums: Vec<u32>,
  symtab: usize,
  strtab: usize,
  crcs: usize,
  fileinfo: usize,
  text: usize,
  data: usize,
  rodata: usize,
  rela_text: usize,
  rela_data: usize,
  rela_rodata: usize,
  names: Vec<String>,
}

impl Rpx {
  pub fn read_from<R: Read + Seek>(reader: &mut R) -> Result<Self, FormatError> {
    let header = ElfHeader::read_from(reader)?;

    reader.align_to(0x40)?;
    let mut headers = Vec::new();

    for _ in 0..header.sh_num {
      let header = SectionHeader::read_from(reader)?;
      headers.push(header);
    }

    let mut sections = Vec::new();
    let mut checksums = Vec::new();
    let mut symtab = 0;
    let mut strtab = 0;
    let mut crcs = 0;
    let mut fileinfo = 0;

    for (idx, sheader) in headers.iter_mut().enumerate() {
      reader.seek(SeekFrom::Start(sheader.offset as u64))?;
      if sheader.section_type == ShType::Null {
        checksums.push(0);
        sections.push(Section::Null);
        continue;
      }

      let (section, mut checksum) = Section::read_from(sheader, reader)?;
      match section {
        Section::Symtab(_) => symtab = idx,
        Section::Strtab(_) => {
          if idx != header.sh_str_idx as usize {
            strtab = idx
          }
        }
        Section::Crc(_) => {
          crcs = idx;
          checksum = 0;
        }
        Section::FileInfo(_) => fileinfo = idx,
        _ => {}
      }
      sections.push(section);
      checksums.push(checksum);
    }

    let mut names = Vec::new();
    let mut text = 0;
    let mut data = 0;
    let mut rodata = 0;
    let mut rela_text = 0;
    let mut rela_data = 0;
    let mut rela_rodata = 0;

    if let Some(Section::Strtab(shnames)) = sections.get(header.sh_str_idx as usize) {
      for (i, section) in headers.iter().enumerate() {
        let name = CStr::from_bytes_until_nul(&shnames[section.name as usize..])
          .unwrap()
          .to_str()
          .unwrap()
          .to_string();

        if name == ".text" {
          text = i;
        } else if name == ".data" {
          data = i;
        } else if name == ".rodata" {
          rodata = i;
        } else if name == ".rela.text" {
          rela_text = i;
        } else if name == ".rela.data" {
          rela_data = i;
        } else if name == ".rela.rodata" {
          rela_rodata = i;
        }

        names.push(name);
      }
    }

    Ok(Rpx {
      header,
      headers,
      sections,
      checksums,
      crcs,
      strtab,
      symtab,
      fileinfo,
      text,
      data,
      rodata,
      rela_text,
      rela_data,
      rela_rodata,
      names,
    })
  }

  pub fn write<W: Write + Seek>(&self, writer: &mut W) -> Result<(), FormatError> {
    self.header.write(writer)?;
    writer.pad(0x40)?;
    for header in self.headers.iter() {
      header.write(writer)?;
    }
    let mut offsets = vec![(0, 0); self.sections.len()];
    let mut offset_order: Vec<_> = self.headers.iter().enumerate().collect();
    offset_order.sort_by(|a, b| a.1.offset.cmp(&b.1.offset));

    for (section_id, _) in offset_order.iter() {
      // for section in self.sections.iter() {
      let section = self.sections.get(*section_id).unwrap();
      let header = self.headers.get(*section_id).unwrap();
      let offset = if header.section_type == ShType::Null {
        0
      } else {
        writer.stream_position()?
      };
      section.write(writer)?;
      offsets[*section_id] = (offset, writer.stream_position()? - offset);
    }

    writer.seek(SeekFrom::Start(0x40))?;

    for (header, (offset, size)) in self.headers.iter().zip(offsets) {
      let mut updated = header.clone();
      updated.offset = offset as u32;
      updated.size = size as u32;
      updated.write(writer)?;
    }
    Ok(())
  }


  #[rustfmt::skip]
  pub fn set_custom_section(&mut self, base_address: u32, data: Vec<u8>) {
    // self.custom_text_offset = self.data.len() as u32;
    let sh = SectionHeader {
      name:           0,
      section_type:   ShType::Progbits,
      flags:          ShFlags::SHF_ALLOC | ShFlags::SHF_EXECINSTR,
      addr:           base_address,
      offset:         0,
      size:           data.len() as u32,
      link:           0,
      target_section: 0,
      align:          32,
      entry_size:     0,
    };
    self.headers[0] = sh;
    self.sections[0] = Section::Text(data);
  }

  fn addr_to_section(&self, addr: u32) -> usize {
    if addr > 0x02000020 && addr < 0x02900000 {
      self.text
    } else if addr < 0x03000000 {
      0
    } else if addr < 0x1018c0c0 {
      self.rodata
    } else {
      self.data
    }
  }

  fn addr_to_reloc_section(&self, addr: u32) -> usize {
    if addr > 0x02000020 && addr < 0x02900000 {
      self.rela_text
    } else if addr < 0x03000000 {
      0
    } else if addr < 0x1018c0c0 {
      self.rela_rodata
    } else {
      self.rela_data
    }
  }

  pub fn patch(&mut self, addr: u32, values: &[u8]) {
    self.patch_text_section(self.addr_to_section(addr), addr, values);
  }

  pub fn writer(&mut self, addr: u32) -> Cursor<&mut [u8]> {
    let section_id = self.addr_to_section(addr);
    if let Some(Section::Text(ref mut text)) = self.sections.get_mut(section_id) {
      let offset = (addr - self.headers[section_id].addr) as usize;
      Cursor::new(&mut text[offset..])
    } else {
      todo!()
    }
  }

  pub fn reader(&self, addr: u32) -> Cursor<&[u8]> {
    let section_id = self.addr_to_section(addr);
    if let Some(Section::Text(ref text)) = self.sections.get(section_id) {
      let offset = (addr - self.headers[section_id].addr) as usize;
      Cursor::new(&text[offset..])
    } else {
      todo!()
    }
  }

  pub fn add_reloc(&mut self, addr: u32, new_addr: u32) {
    let reloc_id = self.addr_to_reloc_section(addr);
    if let Some(Section::Reloc(ref mut relocs)) = self.sections.get_mut(reloc_id) {
      let mut reloc = Reloc {
        offset: addr,
        symbol: 0,
        addend: new_addr - 0x02000020,
      };
      reloc.set_type_and_symbol(RelocType::RelocAddr32, 2);
      let (t, s) = reloc.type_and_symbol();
      assert_eq!(RelocType::RelocAddr32 as u8, t as u8);
      assert_eq!(2, s);
      relocs.push(reloc);
    }
  }

  pub fn append_text(&mut self, new_text: Vec<u8>) {
    if let Some(Section::Text(ref mut text)) = self.sections.get_mut(self.text) {
      text.extend(new_text);
    }
  }

  pub fn patch_reloc(&mut self, addr: u32, new_addr: u32) {
    let reloc_id = self.addr_to_reloc_section(addr);

    let curr = if addr % 0x4 == 0 {
      self.reader(addr).read_u32_be().unwrap()
    } else {
      self.reader(addr).read_u16_be().unwrap() as u32
    };
    if let Some(Section::Reloc(ref mut relocs)) = self.sections.get_mut(reloc_id) {
      for r in relocs.iter_mut() {
        if r.offset == addr {
          // let reloc_type = r.symbol & 0xff;
          // let reloc_symbol = r.symbol >> 8;
          // // let symbol = &symbols[reloc_symbol as usize];
          // println!("{:#06x} {:#06x} {:#010x} {:#010x}", reloc_type, reloc_symbol, r.offset, r.addend);
          // // println!("{:#?}", symbol);
          // // symbol.uknown00
          // println!("{}", self.get_string_at(symbol.uknown00 as usize).unwrap());
          let (rtype, symbol) = r.type_and_symbol();
          match rtype {
            RelocType::RelocAddr32 => {
              // let offset = (addr - self.headers[text_id].addr) as usize;
              // println!("addr32 {}", symbol);
              let base = curr - r.addend;
              r.addend = new_addr - base;
            }
            RelocType::RelocLo16 => {
              println!("lo16 {}", symbol);
              r.addend = 0x0001ffff & new_addr;
            }
            RelocType::RelocHi16 => todo!(),
            RelocType::RelocHa16 => todo!(),
            RelocType::RelocRel24 => todo!(),
            RelocType::RelocRel14 => todo!(),
            RelocType::DtpMod32 => todo!(),
            RelocType::DtpRel32 => todo!(),
            RelocType::Rel16Ha => todo!(),
            RelocType::Rel16Hi => todo!(),
            RelocType::Rel16Lo => todo!(),
          }
        }
      }
    }
  }

  fn patch_text_section(&mut self, id: usize, addr: u32, values: &[u8]) {
    if let Some(Section::Text(ref mut text)) = self.sections.get_mut(id) {
      let offset = (addr - self.headers[id].addr) as usize;
      text[offset..offset + values.len()].copy_from_slice(values);
    }
  }

  fn get_shstring_at(&self, at: usize) -> Option<&str> {
    if let Some(Section::Strtab(shnames)) = self.sections.get(self.header.sh_str_idx as usize) {
      Some(CStr::from_bytes_until_nul(&shnames[at..]).unwrap().to_str().unwrap())
    } else {
      None
    }
  }

  fn get_string_at(&self, at: usize) -> Option<&str> {
    if let Some(Section::Strtab(shnames)) = self.sections.get(self.strtab) {
      Some(CStr::from_bytes_until_nul(&shnames[at..]).unwrap().to_str().unwrap())
    } else {
      None
    }
  }

  fn get_section_name(&self, idx: u32) -> &str {
    let sheader = self.headers.get(idx as usize).unwrap();
    if let Some(Section::Strtab(shnames)) = self.sections.get(self.header.sh_str_idx as usize) {
      CStr::from_bytes_until_nul(&shnames[sheader.name as usize..])
        .unwrap()
        .to_str()
        .unwrap()
    } else {
      unreachable!()
    }
  }

  pub fn print_listing(&self) {
    println!(
      "[Nr] {:<20} {:<14} {:<10} {:<10} {:<10} {:<10} {:<10}",
      "Name", "Type", "Addr", "Off", "Size", "Checksum", "Crc"
    );
    if let Some(Section::Crc(crcs)) = self.sections.get(self.crcs) {
      for (i, crc) in crcs.iter().enumerate() {
        println!(
          "[{:02}] {: <20} {: <14} {: <10} {: <10} {: <10} {: <10} {: <10} {}",
          i,
          self.get_section_name(i as u32),
          // self.get_section_name(i as u32),
          fmt(self.headers[i].section_type),
          hexfmt(self.headers[i].addr),
          hexfmt(self.headers[i].offset),
          hexfmt(self.headers[i].size),
          hexfmt(self.checksums[i]),
          hexfmt(*crc),
          *crc == self.checksums[i]
        );
      }
    }
    if let Some(Section::FileInfo(fileinfo)) = self.sections.get(self.fileinfo) {
      println!("{:?}", fileinfo);
      // println!("{}", self.get_string_at(fileinfo.ukn30 as usize).unwrap());
    }
  }
}

fn fmt(t: ShType) -> String {
  format!("{:?}", t)
}

fn hexfmt(u: u32) -> String {
  format!("{:#010x}", u)
}

fn _as_slice<T: Sized>(p: &T) -> &[u8] {
  unsafe { ::std::slice::from_raw_parts((p as *const T) as *const u8, ::std::mem::size_of::<T>()) }
}

fn crc32(mut crc: u32, data: &[u8]) -> u32 {
  crc = !crc;
  for c in data {
    crc = (crc >> 8) ^ CRC_TABLE[((crc ^ *c as u32) & 0xff) as usize];
  }
  !crc
}

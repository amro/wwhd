use std::fs::File;
use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Write;

use crate::BeRead;
use crate::BeWrite;
use crate::FormatError;
use crate::Pad;

const GET_DANCE: [u8; 12] = *b"059get_dance";
const GET_ITEM: [u8; 11] = *b"011get_item";

pub struct Event {
  name: [u8; 0x20],
  event_index: i32,
  unknown1: u32,
  priority: u32,
  actor_indices: [i32; 0x14],
  num_actors: u32,
  starting_flags: [i32; 2],
  ending_flags: [i32; 3],
  play_jingle: u8,
  zero_initialized_runtime_data: [u8; 0x1b],
}

impl Event {
  pub const DATA_SIZE: u32 = 0xb0;

  pub fn read<R: Read>(reader: &mut R) -> Result<Event, FormatError> {
    let mut name = [0u8; 0x20];
    reader.read_exact(&mut name)?;
    let event_index = reader.read_i32_be()?;
    let unknown1 = reader.read_u32_be()?;
    let priority = reader.read_u32_be()?;
    let mut actor_indices = [-1i32; 0x14];
    for i in 0..0x14 {
      actor_indices[i] = reader.read_i32_be()?;
    }
    let num_actors = reader.read_u32_be()?;
    let mut starting_flags = [-1i32; 2];
    for i in 0..0x2 {
      starting_flags[i] = reader.read_i32_be()?;
    }

    let mut ending_flags = [-1i32; 3];
    for i in 0..0x3 {
      ending_flags[i] = reader.read_i32_be()?;
    }
    let play_jingle = reader.read_u8()?;
    let zero_initialized_runtime_data = [0u8; 0x1b];

    Ok(Event {
      name,
      event_index,
      unknown1,
      priority,
      actor_indices,
      num_actors,
      starting_flags,
      ending_flags,
      play_jingle,
      zero_initialized_runtime_data,
    })
  }

  pub fn write<W: Write + Seek>(&self, writer: &mut W) -> Result<(), FormatError> {
    writer.write_all(&self.name)?;
    writer.write_i32_be(self.event_index)?;
    writer.write_u32_be(self.unknown1)?;
    writer.write_u32_be(self.priority)?;
    for idx in self.actor_indices {
      writer.write_i32_be(idx)?;
    }
    writer.write_u32_be(self.num_actors)?;

    for flag in self.starting_flags {
      writer.write_i32_be(flag)?;
    }

    for flag in self.ending_flags {
      writer.write_i32_be(flag)?;
    }

    writer.write_u8(self.play_jingle)?;

    writer.write_all(&self.zero_initialized_runtime_data)?;

    Ok(())
  }
}

pub struct Actor {
  name: [u8; 0x20],
  staff_id: u32,
  actor_index: i32,
  flag_id_to_set: i32,
  staff_type: u32,
  initial_action_index: i32,
  zero_initialized_runtime_data: [u8; 0x1c],
}

impl Actor {
  pub const DATA_SIZE: u32 = 0x50;

  pub fn read<R: Read>(reader: &mut R) -> Result<Self, FormatError> {
    let mut name = [0u8; 0x20];
    reader.read_exact(&mut name)?;
    let staff_id = reader.read_u32_be()?;
    let actor_index = reader.read_i32_be()?;
    let flag_id_to_set = reader.read_i32_be()?;
    let staff_type = reader.read_u32_be()?;
    let initial_action_index = reader.read_i32_be()?;
    let zero_initialized_runtime_data = [0u8; 0x1c];

    Ok(Actor {
      name,
      staff_id,
      actor_index,
      flag_id_to_set,
      staff_type,
      initial_action_index,
      zero_initialized_runtime_data,
    })
  }

  pub fn write<W: Write + Seek>(&self, writer: &mut W) -> Result<(), FormatError> {
    writer.write_all(&self.name)?;
    writer.write_u32_be(self.staff_id)?;
    writer.write_i32_be(self.actor_index)?;
    writer.write_i32_be(self.flag_id_to_set)?;
    writer.write_u32_be(self.staff_type)?;
    writer.write_i32_be(self.initial_action_index)?;

    writer.write_all(&self.zero_initialized_runtime_data)?;

    Ok(())
  }
}

pub struct Action {
  name: [u8; 0x20],
  duplicate_id: u32,
  action_index: i32,
  starting_flags: [i32; 3],
  flag_id_to_set: i32,
  first_property_index: i32,
  next_action_index: i32,
  zero_initialized_runtime_data: [u8; 0x10],
}

impl Action {
  pub const DATA_SIZE: u32 = 0x50;

  pub fn read<R: Read>(reader: &mut R) -> Result<Action, FormatError> {
    let mut name = [0u8; 0x20];
    reader.read_exact(&mut name)?;
    let duplicate_id = reader.read_u32_be()?;
    let action_index = reader.read_i32_be()?;
    let mut starting_flags = [-1i32; 3];
    for i in 0..0x3 {
      starting_flags[i] = reader.read_i32_be()?;
    }
    let flag_id_to_set = reader.read_i32_be()?;
    let first_property_index = reader.read_i32_be()?;
    let next_action_index = reader.read_i32_be()?;

    let zero_initialized_runtime_data = [0u8; 0x10];

    Ok(Action {
      name,
      duplicate_id,
      action_index,
      starting_flags,
      flag_id_to_set,
      first_property_index,
      next_action_index,
      zero_initialized_runtime_data,
    })
  }

  pub fn write<W: Write + Seek>(&self, writer: &mut W) -> Result<(), FormatError> {
    writer.write_all(&self.name)?;
    writer.write_u32_be(self.duplicate_id)?;
    writer.write_i32_be(self.action_index)?;

    for flag in self.starting_flags {
      writer.write_i32_be(flag)?;
    }
    writer.write_i32_be(self.flag_id_to_set)?;
    writer.write_i32_be(self.first_property_index)?;
    writer.write_i32_be(self.next_action_index)?;

    writer.write_all(&self.zero_initialized_runtime_data)?;

    Ok(())
  }
}

pub struct Property {
  name: [u8; 0x20],
  property_index: i32,
  data_type: u32,
  data_index: u32,
  data_size: u32,
  next_property_index: i32,
  zero_initialized_runtime_data: [u8; 0xc],
}

impl Property {
  pub const DATA_SIZE: u32 = 0x40;

  pub fn read<R: Read>(reader: &mut R) -> Result<Self, FormatError> {
    let mut name = [0u8; 0x20];
    reader.read_exact(&mut name)?;
    let property_index = reader.read_i32_be()?;
    let data_type = reader.read_u32_be()?;
    let data_index = reader.read_u32_be()?;
    let data_size = reader.read_u32_be()?;
    let next_property_index = reader.read_i32_be()?;
    let zero_initialized_runtime_data = [0u8; 0xc];

    Ok(Property {
      name,
      property_index,
      data_type,
      data_index,
      data_size,
      next_property_index,
      zero_initialized_runtime_data,
    })
  }

  pub fn write<W: Write + Seek>(&self, writer: &mut W) -> Result<(), FormatError> {
    writer.write_all(&self.name)?;
    writer.write_i32_be(self.property_index)?;
    writer.write_u32_be(self.data_type)?;
    writer.write_u32_be(self.data_index)?;
    writer.write_u32_be(self.data_size)?;
    writer.write_i32_be(self.next_property_index)?;

    writer.write_all(&self.zero_initialized_runtime_data)?;

    Ok(())
  }
}

#[derive(Debug)]
struct Header {
  event_list_offset: u32,
  num_events: u32,
  actor_list_offset: u32,
  num_actors: u32,
  action_list_offset: u32,
  num_actions: u32,
  property_list_offset: u32,
  num_properties: u32,
  float_list_offset: u32,
  num_floats: u32,
  int_list_offset: u32,
  num_ints: u32,
  string_list_offset: u32,
  string_list_total_size: u32,
}

impl Header {
  pub fn read_from<R: Read + Seek>(reader: &mut R) -> Result<Self, FormatError> {
    Ok(Header {
      event_list_offset: reader.read_u32_be()?,
      num_events: reader.read_u32_be()?,
      actor_list_offset: reader.read_u32_be()?,
      num_actors: reader.read_u32_be()?,
      action_list_offset: reader.read_u32_be()?,
      num_actions: reader.read_u32_be()?,
      property_list_offset: reader.read_u32_be()?,
      num_properties: reader.read_u32_be()?,
      float_list_offset: reader.read_u32_be()?,
      num_floats: reader.read_u32_be()?,
      int_list_offset: reader.read_u32_be()?,
      num_ints: reader.read_u32_be()?,
      string_list_offset: reader.read_u32_be()?,
      string_list_total_size: reader.read_u32_be()?,
    })
  }

  pub fn write<W: Write + Seek>(&self, writer: &mut W) -> Result<(), FormatError> {
    writer.write_u32_be(self.event_list_offset)?;
    writer.write_u32_be(self.num_events)?;
    writer.write_u32_be(self.actor_list_offset)?;
    writer.write_u32_be(self.num_actors)?;
    writer.write_u32_be(self.action_list_offset)?;
    writer.write_u32_be(self.num_actions)?;
    writer.write_u32_be(self.property_list_offset)?;
    writer.write_u32_be(self.num_properties)?;
    writer.write_u32_be(self.float_list_offset)?;
    writer.write_u32_be(self.num_floats)?;
    writer.write_u32_be(self.int_list_offset)?;
    writer.write_u32_be(self.num_ints)?;
    writer.write_u32_be(self.string_list_offset)?;
    writer.write_u32_be(self.string_list_total_size)?;
    writer.pad(0x40)?;
    Ok(())
  }
}

pub struct EventList {
  header: Header,
  events: Vec<Event>,
  actors: Vec<Actor>,
  actions: Vec<Action>,
  properties: Vec<Property>,
  floats: Vec<f32>,
  ints: Vec<i32>,
  string_block: Vec<u8>,
}

impl EventList {
  pub fn from_file(path: &str) -> Result<Self, FormatError> {
    let mut f = File::open(path)?;
    Self::read_from(&mut f)
  }

  pub fn read_from<R: Read + Seek>(reader: &mut R) -> Result<Self, FormatError> {
    let header = Header::read_from(reader)?;

    _ = reader.read_u32_be()?;
    _ = reader.read_u32_be()?;

    let mut events = Vec::with_capacity(header.num_events as usize);

    for event_index in 0..header.num_events {
      let offset = header.event_list_offset + event_index * Event::DATA_SIZE;
      reader.seek(SeekFrom::Start(offset as u64))?;
      events.push(Event::read(reader)?);
    }

    let mut actors = Vec::with_capacity(header.num_actors as usize);

    for actor_index in 0..header.num_actors {
      let offset = header.actor_list_offset + actor_index * Actor::DATA_SIZE;
      reader.seek(SeekFrom::Start(offset as u64))?;
      actors.push(Actor::read(reader)?);
    }

    let mut actions = Vec::with_capacity(header.num_actions as usize);

    for action_index in 0..header.num_actions {
      let offset = header.action_list_offset + action_index * Action::DATA_SIZE;
      reader.seek(SeekFrom::Start(offset as u64))?;
      actions.push(Action::read(reader)?);
    }

    let mut properties = Vec::with_capacity(header.num_properties as usize);

    for property_index in 0..header.num_properties {
      let offset = header.property_list_offset + property_index * Property::DATA_SIZE;
      reader.seek(SeekFrom::Start(offset as u64))?;
      properties.push(Property::read(reader)?);
    }

    let mut floats = Vec::new();

    for float_index in 0..header.num_floats {
      let offset = header.float_list_offset + float_index * 4;
      reader.seek(SeekFrom::Start(offset as u64))?;
      floats.push(reader.read_f32_be()?);
    }

    let mut ints = Vec::new();

    for int_index in 0..header.num_ints {
      let offset = header.int_list_offset + int_index * 4;
      reader.seek(SeekFrom::Start(offset as u64))?;
      ints.push(reader.read_i32_be()?);
    }

    let mut string_block = vec![0; header.string_list_total_size as usize];
    reader.seek(SeekFrom::Start(header.string_list_offset as u64))?;
    reader.read_exact(&mut string_block)?;

    Ok(EventList {
      header,
      events,
      actors,
      actions,
      properties,
      floats,
      ints,
      string_block,
    })
  }

  pub fn write<W: Write + Seek>(&self, writer: &mut W) -> Result<(), FormatError> {
    self.header.write(writer)?;

    assert_eq!(self.header.event_list_offset, writer.stream_position()? as u32);
    for event in self.events.iter() {
      event.write(writer)?;
    }

    assert_eq!(self.header.actor_list_offset, writer.stream_position()? as u32);
    for actor in self.actors.iter() {
      actor.write(writer)?;
    }

    assert_eq!(self.header.action_list_offset, writer.stream_position()? as u32);
    for action in self.actions.iter() {
      action.write(writer)?;
    }

    assert_eq!(self.header.property_list_offset, writer.stream_position()? as u32);
    for property in self.properties.iter() {
      property.write(writer)?;
    }

    assert_eq!(self.header.float_list_offset, writer.stream_position()? as u32);
    for float in self.floats.iter() {
      writer.write_f32_be(*float)?;
    }

    assert_eq!(self.header.int_list_offset, writer.stream_position()? as u32);
    for int in self.ints.iter() {
      writer.write_i32_be(*int)?;
    }

    assert_eq!(self.header.string_list_offset, writer.stream_position()? as u32);
    writer.write_all(&self.string_block)?;

    Ok(())
  }

  pub fn list_events(&self) {
    for event in self.events.iter() {
      println!("{}", std::str::from_utf8(&event.name).unwrap());
    }
  }

  pub fn print_event_name(&self, id: u32) {
    println!("{}", std::str::from_utf8(&self.events[id as usize].name).unwrap());
  }

  pub fn print_event_actions(&self, id: u32, act: u32) {
    let actor = self
      .actors
      .get(
        *self
          .events
          .get(id as usize)
          .unwrap()
          .actor_indices
          .get(act as usize)
          .unwrap() as usize,
      )
      .unwrap();
    let mut action_id = actor.initial_action_index;
    while action_id != -1 {
      println!(
        "{}",
        std::str::from_utf8(&self.actions[action_id as usize].name).unwrap()
      );
      action_id = self.actions.get(action_id as usize).unwrap().next_action_index;
    }
    println!("{}", std::str::from_utf8(&self.events[id as usize].name).unwrap());
  }

  pub fn get_event_action_name(&mut self, event: u32, actor: u32, action: u32) -> Option<[u8; 0x20]> {
    let actor = self
      .actors
      .get(*self.events.get(event as usize)?.actor_indices.get(actor as usize)? as usize)?;
    let mut action_id = actor.initial_action_index;
    for _ in 0..action {
      action_id = self.actions.get(action_id as usize)?.next_action_index;
    }
    Some(self.actions.get(action_id as usize)?.name)
  }

  pub fn get_event_action(&mut self, event: u32, actor: u32, action: u32) -> Option<u32> {
    let actor = self
      .actors
      .get(*self.events.get(event as usize)?.actor_indices.get(actor as usize)? as usize)?;
    let mut action_id = actor.initial_action_index;
    for _ in 0..action {
      action_id = self.actions.get(action_id as usize)?.next_action_index;
    }
    let prop_id = self.actions.get(action_id as usize)?.first_property_index;
    Some(self.ints[self.properties.get(prop_id as usize)?.data_index as usize] as u32)
  }

  pub fn update_event_action(&mut self, event: u32, actor: u32, action: u32, item: u32) -> Option<()> {
    let actor = self
      .actors
      .get(*self.events.get(event as usize)?.actor_indices.get(actor as usize)? as usize)?;
    let mut action_id = actor.initial_action_index;
    for _ in 0..action {
      action_id = self.actions.get(action_id as usize)?.next_action_index;
    }
    let prop_id = self.actions.get(action_id as usize)?.first_property_index;
    if (0x6d..=0x72).contains(&item) {
      self.actions.get_mut(action_id as usize)?.name = [0; 0x20];
      self.actions.get_mut(action_id as usize)?.name[0..12].copy_from_slice(&GET_DANCE);
      self.ints[self.properties.get(prop_id as usize)?.data_index as usize] = (item - 0x6d) as i32;
    } else {
      self.actions.get_mut(action_id as usize)?.name = [0; 0x20];
      self.actions.get_mut(action_id as usize)?.name[0..11].copy_from_slice(&GET_ITEM);
      self.ints[self.properties.get(prop_id as usize)?.data_index as usize] = item as i32;
    }
    Some(())
  }
}

use core::fmt;
use std::collections::HashMap;
use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Write;

use crate::logic::DroppedItem;

use super::Align;
use super::BeRead;
use super::BeWrite;
use super::FormatError;
use super::Pad;

type ChunkName = [u8; 4];

#[repr(u8)]
#[derive(Eq, PartialEq, Clone, Copy, Debug)]
pub enum ChunkLayer {
  All,
  Layer0,
  Layer1,
  Layer2,
  Layer3,
  Layer4,
  Layer5,
  Layer6,
  Layer7,
  Layer8,
  Layer9,
  LayerA,
  LayerB,
}

#[derive(Clone, Copy)]
struct ChunkHeader {
  name: ChunkName,
  num_entries: u32,
  offset: u32,
}

impl ChunkHeader {
  pub fn read<R: Read + Seek>(reader: &mut R) -> Result<ChunkHeader, FormatError> {
    let mut name = [0u8; 4];
    reader.read_exact(&mut name)?;
    Ok(ChunkHeader {
      name,
      num_entries: reader.read_u32_be()?,
      offset: reader.read_u32_be()?,
    })
  }
}

fn chunk_layer(b: u8) -> ChunkLayer {
  match b {
    b'0' => ChunkLayer::Layer0,
    b'1' => ChunkLayer::Layer1,
    b'2' => ChunkLayer::Layer2,
    b'3' => ChunkLayer::Layer3,
    b'4' => ChunkLayer::Layer4,
    b'5' => ChunkLayer::Layer5,
    b'6' => ChunkLayer::Layer6,
    b'7' => ChunkLayer::Layer7,
    b'8' => ChunkLayer::Layer8,
    b'9' => ChunkLayer::Layer9,
    b'a' => ChunkLayer::LayerA,
    b'b' => ChunkLayer::LayerB,
    _ => panic!("bad layer {}", b),
  }
}

enum ChunkBody {
  Actr(ChunkLayer, Vec<Actr>),
  Tres(ChunkLayer, Vec<Tres>),
  Ship(Vec<Ship>),
  Scob(ChunkLayer, Vec<Scob>),
  RoomTable(Vec<Rtbl>, Vec<u8>),
  Unknown(Vec<u8>),
  None,
}

struct Chunk {
  header: ChunkHeader,
  body: ChunkBody,
}

pub struct Actr {
  pub name: [u8; 8],
  pub params: u32,
  pub x_coord: f32,
  pub y_coord: f32,
  pub z_coord: f32,
  pub aux_params_1: u16,
  pub y_rot: u16,
  pub z_rot: u16,
  pub enemy_number: u16,
}

pub struct Rtbl {
  pub offset: u32,
  pub num_rooms: u8,
  pub reverb_amount: u8,
  pub does_time_pass: u8,
  pub unknown: u8,
  pub room_offset: u32,
}

#[derive(Clone)]
pub struct Tres {
  pub name: [u8; 8],
  pub params: u32,
  pub x_coord: f32,
  pub y_coord: f32,
  pub z_coord: f32,
  pub room_num: u16,
  pub y_rot: u16,
  pub chest_item: u8,
  pub flag_id: u8,
}

#[derive(Clone)]
pub struct Ship {
  pub x_coord: f32,
  pub y_coord: f32,
  pub z_coord: f32,
  pub y_rot: u16,
  pub ship_id: u8,
  pub unknown: u8,
}

#[derive(Clone)]
pub struct Scob {
  pub name: [u8; 8],
  pub params: u32,
  pub x_coord: f32,
  pub y_coord: f32,
  pub z_coord: f32,
  pub aux_params_1: u16,
  pub y_rot: u16,
  pub z_rot: u16,
  pub enemy_number: u16,
  pub x_scale: u8,
  pub y_scale: u8,
  pub z_scale: u8,
}

fn lowest_bit(u: u32) -> u32 {
  for i in 0..32 {
    if u & (1 << i) > 0 {
      return i;
    }
  }
  panic!("invalid mask");
}

type ParamMask = u32;
type ZMask = u16;
pub struct DaItem;
pub struct DaBossItem;
pub struct DaTsubo;

#[rustfmt::skip]
impl DaItem {
  pub fn matches(name: [u8; 8]) -> bool {
    name == *b"item\0\0\0\0"
  }
  pub const ITEM_ID                 : ParamMask = 0x000000ff;
  pub const ITEM_PICKUP_FLAG        : ParamMask = 0x0000ff00;
  pub const ENABLE_SPAWN_SWITCH     : ParamMask = 0x00ff0000;
  pub const BEHAVIOR_TYPE           : ParamMask = 0x03000000;
  pub const ITEM_ACTION             : ParamMask = 0xfc000000;
  pub const ENABLE_ACTIVATION_SWITCH: ZMask = 0x00ff;
}

#[rustfmt::skip]
impl DaBossItem {
  pub fn matches(name: [u8; 8]) -> bool {
    name == *b"Bitem\0\0\0"
  }
  pub const STAGE_ID: ParamMask = 0x000000ff;
  // custom param
  pub const ITEM_ID : ParamMask = 0x0000ff00;
}

#[rustfmt::skip]
impl DaTsubo {
  pub fn matches(name: [u8; 8]) -> bool {
    matches!(&name, b"Kmtub\0\0\0" | b"kotubo\0\0" | b"ootubo1\0" | b"Ktaru\0\0\0" | b"Odokuro\0")
  }

  pub const DROPPED_ITEM_ID           : ParamMask = 0x0000003f;
  pub const BEHAVIOR_TYPE             : ParamMask = 0x00003f00;
  pub const UNKNOWN_3                 : ParamMask = 0x00000c00;
  pub const ITEM_PICKUP_FLAG          : ParamMask = 0x007f0000;
  pub const TYPE                      : ParamMask = 0x0f000000;
  pub const INVINCIBLE_WHEN_OFF_CAMERA: ParamMask = 0x07000000;
  pub const DO_NOT_GROUND_ON_SPAWN    : ParamMask = 0x08000000;
  pub const ENABLE_SPAWN_SWITCH       : ZMask = 0x00ff;
  // custom param
  pub const ITEM_ID                   : ZMask = 0xff00;
}

impl Actr {
  pub fn utf8_name(&self) -> &str {
    std::str::from_utf8(&self.name).unwrap()
  }
  pub fn get_param(&self, mask: ParamMask) -> u8 {
    let shift = lowest_bit(mask);
    ((self.params & mask) >> shift) as u8
  }

  pub fn set_param(&mut self, mask: ParamMask, p: u8) {
    let shift = lowest_bit(mask);
    self.params = (self.params & !mask) | (((p as u32) << shift) & mask);
  }

  pub fn get_z_param(&self, mask: ZMask) -> u8 {
    let shift = lowest_bit(mask as u32) as u16;
    ((self.z_rot & mask) >> shift) as u8
  }

  pub fn set_z_param(&mut self, mask: ZMask, p: u8) {
    let shift = lowest_bit(mask as u32) as u16;
    self.z_rot = (self.z_rot & !mask) | (((p as u16) << shift) & mask);
  }

  pub fn set_pot_item_id(&mut self, item: u8) {
    self.params = (self.params & 0xffffff00) | item as u32;
  }

  pub fn get_pot_item_id(&self) -> u8 {
    (self.params & 0x0000003f) as u8
  }

  pub fn plyr_data(&self) {
    log::info!("PLYR ===========");
    log::info!("Name: {}", std::str::from_utf8(&self.name).unwrap());
    log::info!("Id: {:#02X}", self.z_rot & 0x00ff);
    log::info!("Coords: {}, {}, {}", self.x_coord, self.y_coord, self.z_coord);
    log::info!("Params: {:#08X}", self.params);
    log::info!("Room number: {}", self.params & 0x0000003f);
    log::info!("Spawn type: {:#02X}", (self.params & 0x0000f000) >> 12);
    log::info!("EVNT: {:#02X}", (self.params & 0xff000000) >> 24);
  }
}

impl Scob {
  pub fn set_switch(&mut self, switch: u8) {
    self.params |= switch as u32;
  }

  pub fn get_switch(&self) -> u8 {
    (self.params & 0x000000ff) as u8
  }

  pub fn set_behavior_type(&mut self, behavior: u8) {
    self.params |= (behavior as u32) << 16;
  }

  pub fn get_behavior_type(&self) -> u8 {
    ((self.params & 0x00030000) >> 16) as u8
  }
}

impl Tres {
  pub fn set_chest_type(&mut self, chest_type: u8) {
    self.params |= (chest_type as u32) << 20;
  }

  pub fn get_chest_type(&self) -> u8 {
    ((self.params & 0x00f00000) >> 20) as u8
  }

  pub fn set_opened_flag(&mut self, flag: u8) {
    self.params |= (flag as u32) << 7;
  }

  pub fn get_opened_flag(&self) -> u8 {
    ((self.params & 0x00000f80) >> 7) as u8
  }
}

impl fmt::Debug for Actr {
  fn fmt(&self, writer: &mut fmt::Formatter) -> Result<(), fmt::Error> {
    writeln!(writer, "ACTR ===========")?;
    writeln!(writer, "Name: {}", std::str::from_utf8(&self.name).unwrap())?;
    writeln!(writer, "Params: {:#08X}", self.params)?;
    writeln!(writer, "Coords: {}, {}, {}", self.x_coord, self.y_coord, self.z_coord)?;
    writeln!(writer, "Pot item: {:?}", DroppedItem::from_id(self.get_pot_item_id()))
  }
}

impl fmt::Debug for Scob {
  fn fmt(&self, writer: &mut fmt::Formatter) -> Result<(), fmt::Error> {
    writeln!(writer, "SCOB ===========")?;
    writeln!(writer, "Name: {}", std::str::from_utf8(&self.name).unwrap())?;
    writeln!(writer, "Params: {:#08X}", self.params)?;
    writeln!(writer, "Behavior Type: {}", self.get_behavior_type())?;
    writeln!(writer, "Switch to set: {:#X}", self.get_switch())?;
    writeln!(writer, "Coords: {}, {}, {}", self.x_coord, self.y_coord, self.z_coord)
  }
}

impl fmt::Debug for Ship {
  fn fmt(&self, writer: &mut fmt::Formatter) -> Result<(), fmt::Error> {
    writeln!(writer, "SHIP ===========")?;
    writeln!(writer, "Coords: {}, {}, {}", self.x_coord, self.y_coord, self.z_coord)?;
    writeln!(writer, "Id: {}", self.ship_id)
  }
}

impl fmt::Debug for Tres {
  fn fmt(&self, writer: &mut fmt::Formatter) -> Result<(), fmt::Error> {
    writeln!(writer, "Name: {}", std::str::from_utf8(&self.name).unwrap())?;
    writeln!(writer, "Params: {:#08X}", self.params)?;
    writeln!(writer, "Chest Type: {}", self.get_chest_type())?;
    writeln!(writer, "Opened flag: {:#X}", self.get_opened_flag())?;
    writeln!(writer, "Coords: {}, {}, {}", self.x_coord, self.y_coord, self.z_coord)?;
    writeln!(writer, "Room: {:#06X}", self.room_num)?;
    writeln!(writer, "Item: {:#04X}", self.chest_item)?;
    writeln!(writer, "Flag: {:#04X}", self.flag_id)
  }
}

#[allow(dead_code)]
impl Chunk {
  pub fn from_header(header: ChunkHeader) -> Self {
    Chunk {
      header,
      body: ChunkBody::None,
    }
  }
  pub fn size(&self) -> u32 {
    match &self.body {
      ChunkBody::Actr(_, actors) => (actors.len() * 0x20) as u32,
      ChunkBody::Tres(_, tres) => (tres.len() * 0x20) as u32,
      ChunkBody::Scob(_, scob) => (scob.len() * 0x24) as u32,
      ChunkBody::Ship(ship) => (ship.len() * 0x10) as u32,
      ChunkBody::RoomTable(rtbl, rooms) => (rtbl.len() * 12 + rooms.len()).align(4) as u32,
      ChunkBody::Unknown(data) => data.len() as u32,
      ChunkBody::None => panic!("chunk has no body"),
    }
  }
  pub fn read_body<R: Read + Seek>(&mut self, reader: &mut R, size: u32) -> Result<(), FormatError> {
    match &self.header.name[0..3] {
      b"ACT" => {
        reader.seek(SeekFrom::Start(self.header.offset as u64))?;
        let mut actors = Vec::new();
        for _ in 0..self.header.num_entries {
          let mut name = [0u8; 8];
          reader.read_exact(&mut name)?;
          let params = reader.read_u32_be()?;
          let x_coord = reader.read_f32_be()?;
          let y_coord = reader.read_f32_be()?;
          let z_coord = reader.read_f32_be()?;
          let aux_params_1 = reader.read_u16_be()?;
          let y_rot = reader.read_u16_be()?;
          let z_rot = reader.read_u16_be()?;
          let enemy_number = reader.read_u16_be()?;
          actors.push(Actr {
            name,
            params,
            x_coord,
            y_coord,
            z_coord,
            aux_params_1,
            y_rot,
            z_rot,
            enemy_number,
          });
        }

        let layer = if self.header.name[3] == b'R' {
          ChunkLayer::All
        } else {
          chunk_layer(self.header.name[3])
        };

        self.body = ChunkBody::Actr(layer, actors);
      }
      b"SCO" => {
        reader.seek(SeekFrom::Start(self.header.offset as u64))?;
        let mut scobs = Vec::new();
        for _ in 0..self.header.num_entries {
          let mut name = [0u8; 8];
          reader.read_exact(&mut name)?;
          let params = reader.read_u32_be()?;
          let x_coord = reader.read_f32_be()?;
          let y_coord = reader.read_f32_be()?;
          let z_coord = reader.read_f32_be()?;
          let aux_params_1 = reader.read_u16_be()?;
          let y_rot = reader.read_u16_be()?;
          let z_rot = reader.read_u16_be()?;
          let enemy_number = reader.read_u16_be()?;
          let x_scale = reader.read_u8()?;
          let y_scale = reader.read_u8()?;
          let z_scale = reader.read_u8()?;
          let _padding = reader.read_u8()?;
          scobs.push(Scob {
            name,
            params,
            x_coord,
            y_coord,
            z_coord,
            aux_params_1,
            y_rot,
            z_rot,
            enemy_number,
            x_scale,
            y_scale,
            z_scale,
          });
        }

        let layer = if self.header.name[3] == b'B' {
          ChunkLayer::All
        } else {
          chunk_layer(self.header.name[3])
        };
        self.body = ChunkBody::Scob(layer, scobs);
      }
      b"SHI" => {
        reader.seek(SeekFrom::Start(self.header.offset as u64))?;
        let mut ships = Vec::new();
        for _ in 0..self.header.num_entries {
          let x_coord = reader.read_f32_be()?;
          let y_coord = reader.read_f32_be()?;
          let z_coord = reader.read_f32_be()?;
          let y_rot = reader.read_u16_be()?;
          let ship_id = reader.read_u8()?;
          let unknown = reader.read_u8()?;
          ships.push(Ship {
            x_coord,
            y_coord,
            z_coord,
            y_rot,
            ship_id,
            unknown,
          });
        }
        self.body = ChunkBody::Ship(ships);
      }
      b"TRE" => {
        reader.seek(SeekFrom::Start(self.header.offset as u64))?;
        let num_entries = self.header.num_entries;
        let mut tres = Vec::new();
        for _ in 0..num_entries {
          let mut name = [0u8; 8];
          reader.read_exact(&mut name)?;
          let params = reader.read_u32_be()?;
          let x_coord = reader.read_f32_be()?;
          let y_coord = reader.read_f32_be()?;
          let z_coord = reader.read_f32_be()?;

          let room_num = reader.read_u16_be()?;
          let y_rot = reader.read_u16_be()?;
          let chest_item = reader.read_u8()?;
          let flag_id = reader.read_u8()?;
          let _padding = reader.read_u16_be()?;
          tres.push(Tres {
            name,
            params,
            x_coord,
            y_coord,
            z_coord,
            room_num,
            y_rot,
            chest_item,
            flag_id,
          });
        }

        let layer = if self.header.name[3] == b'S' {
          ChunkLayer::All
        } else {
          chunk_layer(self.header.name[3])
        };

        self.body = ChunkBody::Tres(layer, tres);
      }
      b"RTB" => {
        let mut rtbl = Vec::new();
        let mut rooms = Vec::new();
        for i in 0..self.header.num_entries {
          reader.seek(SeekFrom::Start((self.header.offset + i * 4) as u64))?;
          let sub_offset = reader.read_u32_be()?;
          reader.seek(SeekFrom::Start(sub_offset as u64))?;
          let num_rooms = reader.read_u8()?;
          let reverb_amount = reader.read_u8()?;
          let does_time_pass = reader.read_u8()?;
          let unknown = reader.read_u8()?;
          let list_offset = reader.read_u32_be()?;
          let room_offset = rooms.len() as u32;
          reader.seek(SeekFrom::Start(list_offset as u64))?;
          for _ in 0..num_rooms {
            rooms.push(reader.read_u8()?);
          }

          // TODO: bad things will happen if rooms are out of order
          rtbl.push(Rtbl {
            offset: sub_offset,
            num_rooms,
            reverb_amount,
            does_time_pass,
            unknown,
            room_offset,
          });
        }

        self.body = ChunkBody::RoomTable(rtbl, rooms);
      }
      _ => {
        reader.seek(SeekFrom::Start(self.header.offset as u64))?;
        let mut data = vec![0u8; size as usize];
        reader.read_exact(&mut data)?;
        self.body = ChunkBody::Unknown(data);
      }
    }
    Ok(())
  }

  pub fn write_header<W: Write + Seek>(&self, writer: &mut W) -> Result<(), FormatError> {
    let num_entries = match &self.body {
      ChunkBody::Tres(_, v) => v.len() as u32,
      ChunkBody::Scob(_, v) => v.len() as u32,
      ChunkBody::Actr(_, v) => v.len() as u32,
      _ => self.header.num_entries,
    };
    writer.write_all(&self.header.name)?;
    writer.write_u32_be(num_entries)?;
    writer.write_u32_be(self.header.offset)?;
    Ok(())
  }

  pub fn write<W: Write + Seek>(&self, writer: &mut W) -> Result<(), FormatError> {
    match &self.body {
      ChunkBody::Actr(_, actors) => {
        for actr in actors {
          writer.write_all(&actr.name)?;
          writer.write_u32_be(actr.params)?;
          writer.write_f32_be(actr.x_coord)?;
          writer.write_f32_be(actr.y_coord)?;
          writer.write_f32_be(actr.z_coord)?;
          writer.write_u16_be(actr.aux_params_1)?;
          writer.write_u16_be(actr.y_rot)?;
          writer.write_u16_be(actr.z_rot)?;
          writer.write_u16_be(actr.enemy_number)?;
        }
      }
      ChunkBody::Scob(_, scobs) => {
        for scob in scobs {
          writer.write_all(&scob.name)?;
          writer.write_u32_be(scob.params)?;
          writer.write_f32_be(scob.x_coord)?;
          writer.write_f32_be(scob.y_coord)?;
          writer.write_f32_be(scob.z_coord)?;
          writer.write_u16_be(scob.aux_params_1)?;
          writer.write_u16_be(scob.y_rot)?;
          writer.write_u16_be(scob.z_rot)?;
          writer.write_u16_be(scob.enemy_number)?;
          writer.write_u8(scob.x_scale)?;
          writer.write_u8(scob.y_scale)?;
          writer.write_u8(scob.z_scale)?;
          writer.write_u8(0xff)?;
        }
      }
      ChunkBody::Tres(_, tres) => {
        for t in tres {
          writer.write_all(&t.name)?;
          writer.write_u32_be(t.params)?;
          writer.write_f32_be(t.x_coord)?;
          writer.write_f32_be(t.y_coord)?;
          writer.write_f32_be(t.z_coord)?;
          writer.write_u16_be(t.room_num)?;
          writer.write_u16_be(t.y_rot)?;
          writer.write_u8(t.chest_item)?;
          writer.write_u8(t.flag_id)?;
          writer.write_u16_be(0xffff)?;
        }
      }
      ChunkBody::Ship(ships) => {
        for ship in ships {
          writer.write_f32_be(ship.x_coord)?;
          writer.write_f32_be(ship.y_coord)?;
          writer.write_f32_be(ship.z_coord)?;
          writer.write_u16_be(ship.y_rot)?;
          writer.write_u8(ship.ship_id)?;
          writer.write_u8(ship.unknown)?;
        }
      }
      ChunkBody::RoomTable(rtbl, rooms) => {
        let offset = writer.stream_position()? as usize;
        let subentry_start = offset + rtbl.len() * 4;
        let room_start = (offset + rtbl.len() * 12) as u32;
        for i in 0..rtbl.len() {
          writer.write_u32_be((subentry_start + i * 8) as u32)?;
        }
        for r in rtbl.iter() {
          writer.write_u8(r.num_rooms)?;
          writer.write_u8(r.reverb_amount)?;
          writer.write_u8(r.does_time_pass)?;
          writer.write_u8(r.unknown)?;
          writer.write_u32_be(room_start + r.room_offset)?;
        }
        for room in rooms.iter() {
          writer.write_u8(*room)?;
        }
        writer.pad_with(0xff, 4)?;
      }
      ChunkBody::Unknown(data) => {
        writer.write_all(data)?;
      }
      ChunkBody::None => panic!("chunk with no body"),
    }

    Ok(())
  }

  pub fn treasure(&self) -> (ChunkLayer, &Vec<Tres>) {
    if let ChunkBody::Tres(layer, ref tres) = self.body {
      (layer, tres)
    } else {
      unreachable!()
    }
  }

  pub fn treasure_mut(&mut self) -> (ChunkLayer, &mut Vec<Tres>) {
    if let ChunkBody::Tres(layer, ref mut tres) = self.body {
      (layer, tres)
    } else {
      unreachable!()
    }
  }

  pub fn scobs(&self) -> (ChunkLayer, &Vec<Scob>) {
    if let ChunkBody::Scob(layer, ref tres) = self.body {
      (layer, tres)
    } else {
      unreachable!()
    }
  }

  pub fn scobs_mut(&mut self) -> (ChunkLayer, &mut Vec<Scob>) {
    if let ChunkBody::Scob(layer, ref mut tres) = self.body {
      (layer, tres)
    } else {
      unreachable!()
    }
  }

  pub fn actors(&self) -> (ChunkLayer, &Vec<Actr>) {
    if let ChunkBody::Actr(layer, ref tres) = self.body {
      (layer, tres)
    } else {
      unreachable!()
    }
  }

  pub fn actors_mut(&mut self) -> (ChunkLayer, &mut Vec<Actr>) {
    if let ChunkBody::Actr(layer, ref mut tres) = self.body {
      (layer, tres)
    } else {
      unreachable!()
    }
  }
}

pub struct Dzx {
  chunks: HashMap<ChunkName, Chunk>,
  header_order: Vec<ChunkName>,
  body_order: Vec<ChunkName>,
}

#[allow(dead_code)]
impl Dzx {
  pub fn from_reader<R: Read + Seek>(reader: &mut R) -> Result<Self, FormatError> {
    let chunk_count = reader.read_u32_be()?;
    let mut chunks = HashMap::new();
    let mut body_order = Vec::new();
    let mut header_order = Vec::new();
    for _ in 0..chunk_count {
      let header = ChunkHeader::read(reader)?;
      header_order.push(header.name);
      chunks.insert(header.name, Chunk::from_header(header));
    }

    let mut sorted: Vec<ChunkHeader> = chunks.values().map(|v| v.header).collect();
    sorted.sort_by(|a, b| a.offset.cmp(&b.offset));
    let mut last_chunk_offset = 0;
    let mut last_chunk = *b"0000";
    let mut total_chunk_sizes = HashMap::new();
    for header in sorted.iter() {
      body_order.push(header.name);
      if last_chunk_offset != 0 {
        let size = header.offset - last_chunk_offset;
        total_chunk_sizes.insert(last_chunk, size);
      }
      last_chunk = header.name;
      last_chunk_offset = header.offset;
    }
    reader.seek(SeekFrom::End(0))?;
    total_chunk_sizes.insert(last_chunk, (reader.stream_position()? as u32) - last_chunk_offset);

    for chunk in chunks.values_mut() {
      let name = chunk.header.name;
      chunk.read_body(reader, *total_chunk_sizes.get(&name).unwrap())?;
    }

    Ok(Dzx {
      chunks,
      header_order,
      body_order,
    })
  }

  pub fn write<W: Write + Seek>(&mut self, writer: &mut W) -> Result<(), FormatError> {
    let chunk_count = self.chunks.len() as u32;
    writer.write_u32_be(chunk_count)?;

    let header_size = (chunk_count * 12) + 4;
    // the first offset will always come right after the header.
    // it will already be aligned since the header size is a multiple of 4
    let mut offset = header_size;

    for name in self.body_order.iter() {
      if let Some(chunk) = self.chunks.get_mut(name) {
        chunk.header.offset = offset;
        offset += chunk.size();
        offset = offset.align(4);
      } else {
        return Err(FormatError::Corrupt("chunk in header but missing from body".into()));
      }
    }

    for name in self.header_order.iter() {
      if let Some(chunk) = self.chunks.get_mut(name) {
        chunk.write_header(writer)?;
      }
    }

    for name in self.body_order.iter() {
      writer.pad(4)?;
      if let Some(chunk) = self.chunks.get_mut(name) {
        chunk.write(writer)?;
      }
    }

    writer.pad_with(0xff, 0x20)?;

    Ok(())
  }

  pub fn describe_actors(&self) {
    for chunk in self.chunks.values() {
      if let ChunkBody::Actr(layer, ref actors) = chunk.body {
        println!("At {:?}", layer);
        for actr in actors {
          println!("{}", actr.utf8_name());
        }
        println!("-------");
      }
    }
  }

  pub fn get_actors(&self, layer: ChunkLayer) -> Option<&Vec<Actr>> {
    self
      .chunks
      .values()
      .find(|c| matches!(c.body, ChunkBody::Actr(l, _) if l == layer))
      .map(|c| c.actors().1)
  }

  pub fn get_actors_mut(&mut self, layer: ChunkLayer) -> Option<&mut Vec<Actr>> {
    self
      .chunks
      .values_mut()
      .find(|c| matches!(c.body, ChunkBody::Actr(l, _) if l == layer))
      .map(|c| c.actors_mut().1)
  }

  pub fn get_scobs(&self, layer: ChunkLayer) -> Option<&Vec<Scob>> {
    self
      .chunks
      .values()
      .find(|c| matches!(c.body, ChunkBody::Scob(l, _) if l == layer))
      .map(|c| c.scobs().1)
  }

  pub fn get_scobs_mut(&mut self, layer: ChunkLayer) -> Option<&mut Vec<Scob>> {
    self
      .chunks
      .values_mut()
      .find(|c| matches!(c.body, ChunkBody::Scob(l, _) if l == layer))
      .map(|c| c.scobs_mut().1)
  }

  pub fn get_tres(&self, layer: ChunkLayer) -> Option<&Vec<Tres>> {
    self
      .chunks
      .values()
      .find(|c| matches!(c.body, ChunkBody::Tres(l, _) if l == layer))
      .map(|c| c.treasure().1)
  }

  pub fn get_tres_mut(&mut self, layer: ChunkLayer) -> Option<&mut Vec<Tres>> {
    self
      .chunks
      .values_mut()
      .find(|c| matches!(c.body, ChunkBody::Tres(l, _) if l == layer))
      .map(|c| c.treasure_mut().1)
  }

  pub fn add_new_tres(&mut self) -> &mut Vec<Tres> {
    self.chunks.insert(
      *b"TRES",
      Chunk {
        header: ChunkHeader {
          name: *b"TRES",
          num_entries: 0,
          offset: 0,
        },
        body: ChunkBody::Tres(ChunkLayer::All, Vec::new()),
      },
    );

    self.header_order.push(*b"TRES");
    self.body_order.push(*b"TRES");

    self.get_tres_mut(ChunkLayer::All).unwrap()
  }
}

#[cfg(test)]
mod tests {
  use std::io::Cursor;

  use crate::dzx::lowest_bit;
  use crate::dzx::Actr;

  use super::Dzx;
  use std::sync::Once;

  static INIT: Once = Once::new();

  fn setup() {
    INIT.call_once(|| {
      pretty_env_logger::init();
    });
  }

  #[test]
  fn test_roundtrip() {
    setup();
    if let Ok(sample) = std::fs::read("samples/LinkUG_Room0.dzr") {
      let mut cs = Cursor::new(sample);
      let mut parsed = Dzx::from_reader(&mut cs).unwrap();
      let out = Vec::new();
      let mut out_cs = Cursor::new(out);
      parsed.write(&mut out_cs).unwrap();
      assert_eq!(cs.into_inner(), out_cs.into_inner());
    }
  }

  #[test]
  fn actor_params() {
    assert_eq!(0, lowest_bit(1));
    assert_eq!(1, lowest_bit(2));
    assert_eq!(2, lowest_bit(4));
    assert_eq!(8, lowest_bit(0xff00));
    let mut actor = Actr {
      name: *b"00000000",
      params: 0x0,
      x_coord: 0.0,
      y_coord: 0.0,
      z_coord: 0.0,
      aux_params_1: 0,
      y_rot: 0,
      z_rot: 0,
      enemy_number: 0,
    };

    actor.set_param(0xff, 0xaa);
    assert_eq!(0xaa, actor.get_param(0xff));
    actor.set_param(0xff00, 0xbb);
    assert_eq!(0xbb, actor.get_param(0xff00));

    assert_eq!(0xbbaa, actor.params);

    actor.set_z_param(0xff, 0xaa);
    assert_eq!(0xaa, actor.get_z_param(0xff));
    actor.set_z_param(0xff00, 0xbb);
    assert_eq!(0xbb, actor.get_z_param(0xff00));

    assert_eq!(0xbbaa, actor.z_rot);
  }
}

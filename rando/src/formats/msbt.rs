use std::collections::HashMap;
use std::fs::File;
use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Write;

use crate::formats::Align;
use crate::formats::Pad;
use crate::AlignTo;
use crate::BeRead;
use crate::BeWrite;
use crate::FormatError;

const MAGIC_MSBT: [u8; 8] = *b"MsgStdBn";
const MAGIC_LBL1: u32 = 0x4c424c31;
const MAGIC_ATR1: u32 = 0x41545231;
const MAGIC_TSY1: u32 = 0x54535931;
const MAGIC_TXT2: u32 = 0x54585432;

struct Label {
  label: String,
  text: u32,
}

#[derive(Clone)]
struct Attr {
  // unknown1: u32,
  // item_price: u16,
  // unknown2: u16,
  // item_id: u8,
  // unknown3: u8,
  // msg_id: u32,
  // unknown4: u8,
  // unknown5: u32,
  unknown: [u8; 19],
  text: u32,
}

pub struct Msbt {
  hash_table_size: u32,
  buckets: Vec<Vec<u32>>,
  labels: Vec<Label>,
  attrs: Vec<Attr>,
  attr_strings: Vec<String>,
  tsy1: Vec<u32>,
  texts: Vec<String>,
}

fn hash_label(label: &str) -> u32 {
  let mut hash = 0u64;
  for c in label.bytes() {
    hash *= 0x492;
    hash += c as u64;
    hash &= 0xffffffff;
  }
  hash as u32
}

impl Msbt {
  pub fn print_label(&self, label: &str) {
    let bucket = hash_label(label) % self.hash_table_size;
    for l in self.buckets[bucket as usize].iter() {
      let lbl = self.labels.get(*l as usize).unwrap();
      if label == lbl.label {
        self.print_s((*l) as usize);
        // println!("{}", self.texts[lbl.text as usize]);
      }
    }
  }

  fn label_by_label(&self, label: &str) -> Option<usize> {
    let bucket = hash_label(label) % self.hash_table_size;
    for l in self.buckets[bucket as usize].iter() {
      let lbl = self.labels.get(*l as usize).unwrap();
      if label == lbl.label {
        return Some(*l as usize);
      }
    }
    None
  }

  pub fn print_s(&self, i: usize) {
    if let Some(label) = self.labels.get(i) {
      println!("Label: {}", label.label);
      print!("Attrs: ");
      for a in self.attrs[label.text as usize].unknown {
        print!("{:02x} ", a);
      }
      println!();
      // println!("Attr string: {}", self.attr_strings[self.attrs[label.text as usize].text as usize]);
      println!("TSY1: {:#010x}", self.tsy1[label.text as usize]);
      println!("len: {:#06x}", self.texts[label.text as usize].len());
      // println!("{}", self.texts[label.text as usize]);
      println!();
    }
  }
  pub fn from_file(path: &str) -> Result<Self, FormatError> {
    let mut f = File::open(path)?;
    Msbt::from_reader(&mut f)
  }

  pub fn from_reader<R: Read + Seek>(reader: &mut R) -> Result<Self, FormatError> {
    let mut magic = [0u8; 8];
    reader.read_exact(&mut magic)?;

    if magic != MAGIC_MSBT {
      return Err(FormatError::BadMagic);
    }

    let bom = reader.read_u16_be()?;
    if bom != 0xfeff {
      return Err(FormatError::BadMagic);
    }

    _ = reader.read_u16_be()?;
    _ = reader.read_u16_be()?;
    let num_sections = reader.read_u16_be()?;
    if num_sections != 4 {
      return Err(FormatError::BadMagic);
    }
    _ = reader.read_u16_be()?;
    let _file_size = reader.read_u32_be()?;

    _ = reader.read_u16_be()?;
    _ = reader.read_u32_be()?;
    _ = reader.read_u32_be()?;

    let magic = reader.read_u32_be()?;

    if magic != MAGIC_LBL1 {
      return Err(FormatError::BadMagic);
    }

    let _label_table_size = reader.read_u32_be()?;

    _ = reader.read_u32_be()?;
    _ = reader.read_u32_be()?;

    let label_offset_start = reader.stream_position()? as usize;

    let hash_table_size = reader.read_u32_be()?;

    let mut labels = Vec::new();
    let mut buckets = vec![Vec::new(); hash_table_size as usize];

    for i in 0..hash_table_size {
      let off = label_offset_start + 4 + (8 * i as usize);
      reader.seek(SeekFrom::Start(off as u64))?;
      let (lbl_c, lbl_o) = (reader.read_u32_be()?, reader.read_u32_be()?);
      reader.seek(SeekFrom::Start(lbl_o as u64 + label_offset_start as u64))?;
      for _ in 0..lbl_c {
        let sz = reader.read_u8()?;
        let mut buf = vec![0; sz as usize];
        reader.read_exact(&mut buf)?;
        let text_offset = reader.read_u32_be()?;
        labels.push(Label {
          label: String::from_utf8(buf.clone())?,
          text: text_offset,
        });
        buckets[i as usize].push((labels.len() - 1) as u32);
      }
    }

    reader.align_to(0x10)?;

    // atr1

    let magic = reader.read_u32_be()?;
    if magic != MAGIC_ATR1 {
      return Err(FormatError::BadMagic);
    }

    let attr_table_size = reader.read_u32_be()? as usize;

    _ = reader.read_u32_be()?;
    _ = reader.read_u32_be()?;

    let attr_offset_start = reader.stream_position()? as usize;

    let attr_offset_count = reader.read_u32_be()? as usize;
    let attr_data_size = reader.read_u32_be()? as usize;

    if attr_data_size != 0x17 {
      return Err(FormatError::BadMagic);
    }

    let mut attrs = Vec::new();
    let mut attr_strings = Vec::new();
    let mut seen_attr_strings = HashMap::new();
    let mut utf16buf = Vec::new();
    for i in 0..attr_offset_count {
      utf16buf.clear();
      let off = attr_offset_start + 8 + (0x17 * i);
      reader.seek(SeekFrom::Start(off as u64))?;
      let mut attr = Attr { unknown: [0; 19], text: 0 };
      reader.read_exact(&mut attr.unknown)?;
      let attr_o = reader.read_u32_be()?;
      if let Some(id) = seen_attr_strings.get(&attr_o) {
        attr.text = *id;
      } else {
        reader.seek(SeekFrom::Start(attr_o as u64 + attr_offset_start as u64))?;

        loop {
          let a = reader.read_u16_be()?;
          if a == 0 {
            break;
          }
          utf16buf.push(a);
        }
        attr.text = attr_strings.len() as u32;
        seen_attr_strings.insert(attr_o, attr.text);
        attr_strings.push(String::from_utf16(&utf16buf)?);
      }
      attrs.push(attr);
    }
    reader.seek(SeekFrom::Start((attr_table_size + attr_offset_start).align(0x10) as u64))?;

    let magic = reader.read_u32_be()?;
    if magic != MAGIC_TSY1 {
      return Err(FormatError::BadMagic);
    }

    let tsy_size = reader.read_u32_be()? as u64;

    let cur = reader.stream_position()?;

    let mut tsy1 = Vec::with_capacity(tsy_size as usize / 4);
    _ = reader.read_u32_be()?;
    _ = reader.read_u32_be()?;
    for _ in 0..tsy_size / 4 {
      tsy1.push(reader.read_u32_be()?);
    }
    reader.seek(SeekFrom::Start((8 + tsy_size + cur).align(0x10)))?;

    let magic = reader.read_u32_be()?;
    if magic != 0x54585432 {
      return Err(FormatError::BadMagic);
    }

    let txt2_size = reader.read_u32_be()? as u64;
    _ = reader.read_u32_be()?;
    _ = reader.read_u32_be()?;

    let txt2_offset_start = reader.stream_position()?;
    let txt2_num_entries = reader.read_u32_be()?;

    let mut texts = Vec::new();
    let mut offsets = Vec::new();

    for _ in 0..txt2_num_entries {
      offsets.push(reader.read_u32_be().unwrap() as u64);
    }

    for t in 0..txt2_num_entries {
      utf16buf.clear();
      let offset_start = txt2_offset_start + offsets[t as usize];
      reader.seek(SeekFrom::Start(offset_start))?;
      let offset_end = if t + 1 < txt2_num_entries {
        txt2_offset_start + offsets[(t + 1) as usize]
      } else {
        txt2_offset_start + txt2_size
      };

      let mut utf16buf = vec![0; (offset_end - offset_start) as usize];
      reader.read_exact(&mut utf16buf)?;

      texts.push(String::from_utf16be(&utf16buf)?);
    }

    Ok(Msbt {
      hash_table_size,
      buckets,
      labels,
      attrs,
      attr_strings,
      texts,
      tsy1,
    })
  }

  pub fn new_entry_from(&mut self, existing: &str, label: &str, content: &str) -> Result<(), ()> {
    let existing = self.label_by_label(existing).unwrap();
    let existing_text_id = self.labels[existing].text as usize;

    let bucket = hash_label(label) % self.hash_table_size;
    let label_id = self.labels.len();
    let text_id = self.texts.len();

    self.texts.push(format!("{}\0", content));
    self.labels.push(Label {
      label: label.into(),
      text: text_id as u32,
    });
    self.attrs.push(self.attrs.get(existing_text_id).cloned().unwrap());
    self.tsy1.push(self.tsy1.get(existing_text_id).cloned().unwrap());

    self.buckets[bucket as usize].push(label_id as u32);
    Ok(())
  }

  pub fn write<W: Write + Seek>(&self, writer: &mut W) -> Result<(), FormatError> {
    writer.write_all(&MAGIC_MSBT)?;
    writer.write_u16_be(0xfeff)?;
    writer.write_u32_be(0x103)?;
    writer.write_u16_be(0x4)?;
    writer.write_u16_be(0x0)?;

    // we will come back and update this
    let file_size_offset = writer.stream_position()?;
    writer.write_u32_be(0xfffff)?;

    writer.pad_with(0x00, 0x10)?;

    // lbl1
    writer.write_u32_be(MAGIC_LBL1)?;

    // we will come back and update this
    let lbl1_size_offset = writer.stream_position()?;
    writer.write_u32_be(0xfffff)?;

    writer.pad_with(0x00, 0x10)?;
    let lbl1_start = writer.stream_position()?;

    writer.write_u32_be(self.hash_table_size)?;

    let mut lbl_offsets = Vec::new();
    // let bucke_offset_start = 4;
    let lbl_offset_start = 4 + 8 * self.hash_table_size;
    let mut tally = 0;
    for lbl in self.labels.iter() {
      lbl_offsets.push(lbl_offset_start + tally);
      tally += lbl.label.len() as u32 + 1;
    }

    // let mut bucket_offsets = Vec::new();

    // for i in 0..self.hash_table_size {
    //   bucket_offsets.push(bucket_offset_start + tally);
    //   for l in self.buckets[i as usize].iter() {
    //     self.labels[l as usize].label
    //     }
    //   tally += self.buckets[i as usize].len() *
    // }

    for i in 0..self.hash_table_size {
      writer.write_u32_be(self.buckets[i as usize].len() as u32)?;
      // we will come back and update this
      writer.write_u32_be(0xff)?;
    }

    let mut bucket_offsets = Vec::new();

    let mut btally = (self.hash_table_size * 8) + 4_u32;
    for i in 0..self.hash_table_size {
      bucket_offsets.push(btally);
      for l in self.buckets[i as usize].iter() {
        let lbl = &self.labels[*l as usize];
        writer.write_u8(lbl.label.len() as u8)?;
        writer.write_all(lbl.label.as_bytes())?;
        writer.write_u32_be(lbl.text)?;
        btally += (lbl.label.len() + 5) as u32;
      }
    }

    writer.pad_with(0xab, 0x10)?;
    let lbl1_end = writer.stream_position()?;

    for i in 0..self.hash_table_size {
      writer.seek(SeekFrom::Start(lbl1_start + 8 + (8 * i) as u64))?;
      writer.write_u32_be(bucket_offsets[i as usize])?;
    }

    writer.seek(SeekFrom::Start(lbl1_size_offset))?;
    writer.write_u32_be((lbl1_end - lbl1_start) as u32)?;
    writer.seek(SeekFrom::Start(lbl1_end))?;

    // atr1
    writer.write_u32_be(MAGIC_ATR1)?;

    // we will come back and update this
    let atr1_size_offset = writer.stream_position()?;
    writer.write_u32_be(0xfffff)?;
    writer.pad_with(0x00, 0x10)?;
    let atr1_start = writer.stream_position()?;

    writer.write_u32_be(self.attrs.len() as u32)?;
    writer.write_u32_be(0x17)?;

    for attr in self.attrs.iter() {
      writer.write_all(&attr.unknown)?;
      // we will adjust this later
      writer.write_u32_be(0xdeadbeef)?;
    }

    let mut aoffsets = Vec::new();
    let mut atally = 0x17 * (self.attrs.len() as u32) + 8;
    for t in self.attr_strings.iter() {
      aoffsets.push(atally);
      let utf16: Vec<u16> = t.encode_utf16().collect();
      for b in utf16 {
        writer.write_u16_be(b)?;
        atally += 2;
      }
      writer.write_u16_be(0x00)?;
      atally += 2;
      // atally += ((t.len() * 2) + 2) as u32;
    }

    let atr1_end = writer.stream_position()?;

    for i in 0..self.attrs.len() {
      writer.seek(SeekFrom::Start(0x13 + atr1_start + 8 + (i as u64 * 0x17)))?;
      writer.write_u32_be(aoffsets[self.attrs[i].text as usize])?;
    }

    writer.seek(SeekFrom::Start(atr1_size_offset))?;
    writer.write_u32_be((atr1_end - atr1_start) as u32)?;
    writer.seek(SeekFrom::Start(atr1_end))?;

    writer.pad_with(0xab, 0x10)?;

    //tsy1
    writer.write_u32_be(MAGIC_TSY1)?;
    writer.write_u32_be(self.tsy1.len() as u32 * 4)?;
    writer.pad_with(0x00, 0x10)?;
    for flag in self.tsy1.iter() {
      writer.write_u32_be(*flag)?;
    }
    writer.pad_with(0xab, 0x10)?;

    //txt2
    writer.write_u32_be(MAGIC_TXT2)?;

    // we will come back and update this
    let txt2_size_offset = writer.stream_position()?;
    writer.write_u32_be(0xfffff)?;

    writer.pad_with(0x00, 0x10)?;
    let txt2_start = writer.stream_position()?;

    writer.write_u32_be(self.texts.len() as u32)?;

    for _ in self.texts.iter() {
      writer.write_u32_be(0xff)?;
    }

    let mut toffsets = Vec::new();
    let mut ttally = 4 + (self.texts.len() * 4) as u32;
    for t in self.texts.iter() {
      toffsets.push(ttally);
      let utf16: Vec<u16> = t.encode_utf16().collect();
      for b in utf16 {
        writer.write_u16_be(b)?;
        ttally += 2;
      }
    }

    let txt2_end = writer.stream_position()?;

    writer.pad_with(0xab, 0x10)?;
    let file_end = writer.stream_position()?;

    for (i, toff) in toffsets.iter().enumerate() {
      writer.seek(SeekFrom::Start(txt2_start + 4 + i as u64 * 0x4))?;
      writer.write_u32_be(*toff)?;
    }

    writer.seek(SeekFrom::Start(txt2_size_offset))?;
    writer.write_u32_be((txt2_end - txt2_start) as u32)?;

    writer.seek(SeekFrom::Start(file_size_offset))?;
    writer.write_u32_be(file_end as u32)?;

    Ok(())
  }
}

#[cfg(test)]
mod tests {
  use std::io::Cursor;

  use crate::msbt::Msbt;

  #[test]
  fn roundtrip() {
    if let Ok(sample) = std::fs::read("samples/message.msbt") {
      let mut cs = Cursor::new(sample);
      let parsed = Msbt::from_reader(&mut cs).unwrap();
      let out = Vec::new();
      let mut out_cs = Cursor::new(out);
      parsed.write(&mut out_cs).unwrap();
      assert_eq!(cs.into_inner(), out_cs.into_inner());
    }
  }
}

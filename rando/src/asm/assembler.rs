#![allow(non_upper_case_globals)]

use crate::asm::map::Symbols;
use crate::formats::*;

use std::io::Cursor;

#[allow(non_camel_case_types)]
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
#[repr(u8)]
pub enum Register {
  r0 = 0,
  r1 = 1,
  r2 = 2,
  r3 = 3,
  r4 = 4,
  r5 = 5,
  r6 = 6,
  r7 = 7,
  r8 = 8,
  r9 = 9,
  r10 = 10,
  r11 = 11,
  r12 = 12,
  r13 = 13,
  r14 = 14,
  r15 = 15,
  r19 = 19,
  r20 = 20,
  r27 = 27,
  r28 = 28,
  r29 = 29,
  r30 = 30,
  r31 = 31,
}

#[derive(Debug, Clone, Copy)]
#[repr(u32)]
enum Opcode {
  B = 18 << 26,
  BC = 16 << 26,
  CMPI = 11 << 26,
  ADDI = 14 << 26,
  ADDIS = 15 << 26,
  RLWINM = 21 << 26,
  STW = 36 << 26,
  STWU = 37 << 26,
  STB = 38 << 26,
  AND = 31 << 26,
  LHZ = 40 << 26,
  MULLI = 7 << 26,
}

fn opcode(o: u32) -> u32 {
  o << 26
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Address {
  Absolute(u32),
  Label(String),
}

impl Into<Address> for u32 {
  fn into(self) -> Address {
    Address::Absolute(self)
  }
}

impl Into<Address> for &str {
  fn into(self) -> Address {
    Address::Label(self.into())
  }
}

#[allow(non_camel_case_types)]
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Instruction {
  add(Register, Register, Register),
  addi(Register, Register, u16),
  addis(Register, Register, u16),
  b(Address),
  bl(Address),
  blr,
  bctr,
  bctrl,
  beq(Address),
  bne(Address),
  cmpw(Register, Register),
  cmpwi(Register, u16),
  li(Register, u16),
  lis(Register, u16),
  long(u32),
  nop,
  rlwinm(Register, Register, i16, i16, i16),
  rlwinmd(Register, Register, i16, i16, i16),
  mflr(Register),
  mtlr(Register),
  mtctr(Register),
  mulli(Register, Register, u16),
  mr(Register, Register),
  lbz(Register, i16, Register),
  lbzu(Register, i16, Register),
  lhz(Register, i16, Register),
  lwz(Register, i16, Register),
  lwzu(Register, i16, Register),
  stw(Register, i16, Register),
  stb(Register, i16, Register),
  stwu(Register, i16, Register),
  subi(Register, Register, u16),
}

use std::fmt;

impl fmt::Display for Instruction {
  fn fmt(&self, formatter: &mut fmt::Formatter) -> Result<(), fmt::Error> {
    match self {
      Instruction::long(d) => write!(formatter, ".long {:#X}", d),
      _ => Ok(()),
    }
  }
}

fn encode_branch(at: i32, li: i32, aa: u32, lk: u32) -> u32 {
  let mask: u32 = 0b00000011111111111111111111111100;
  let li = li - at;
  let li = li as u32;
  lk & 0x01 | (aa & 0x01) << 1 | (li & mask) | Opcode::B as u32
}

fn encode_branch_conditional(at: i32, bo: u32, bi: u32, bd: i32, aa: u32, lk: u32) -> u32 {
  let mask: u32 = 0b00000000000000001111111111111100;
  let bd = bd - at;
  let bd = bd as u32;
  lk & 0x01 | (aa & 0x01) << 1 | (bd & mask) | bi << 16 | bo << 21 | Opcode::BC as u32
}

fn encode_cmp(d: u8, l: u8, a: Register, b: Register) -> u32 {
  let d = d as u32;
  let l = l as u32;
  let a = a as u32;
  let b = b as u32;
  b << 11 | a << 16 | l << 20 | d << 23 | Opcode::AND as u32
}

fn encode_cmpi(bf: u32, l: u32, a: Register, si: u16) -> u32 {
  let ra = a as u32;
  let si = si as u32;
  si & 0xffff | ra << 16 | l << 21 | bf << 23 | Opcode::CMPI as u32
}

fn mfspr(t: Register, spr: u16) -> u32 {
  let rt = t as u32;
  let spr = spr as u32;
  (339 << 1) | spr << 11 | rt << 21 | Opcode::AND as u32
}

fn mtspr(s: Register, spr: u16) -> u32 {
  let s = s as u32;
  let spr = spr as u32;
  (467 << 1) | spr << 11 | s << 21 | Opcode::AND as u32
}

pub fn encode_mulli(d: Register, a: Register, simm: u16) -> u32 {
  let d = d as u32;
  let a = a as u32;
  let simm = simm as u32;
  simm & 0xffff | (a << 16) | (d << 21) | Opcode::MULLI as u32
}

pub fn encode_add(d: Register, a: Register, b: Register) -> u32 {
  let d = d as u32;
  let a = a as u32;
  let b = b as u32;
  266 << 1 | (b << 11) | (a << 16) | d << 21 | Opcode::AND as u32
}

pub fn encode_addi(t: Register, a: Register, si: u16) -> u32 {
  let t = t as u32;
  let a = a as u32;
  let si = si as u32;
  si & 0xffff | (a << 16) | (t << 21) | Opcode::ADDI as u32
}

pub fn encode_addis(t: Register, a: Register, si: u16) -> u32 {
  let t = t as u32;
  let a = a as u32;
  let si = si as u32;
  si & 0xffff | (a << 16) | (t << 21) | Opcode::ADDIS as u32
}

pub fn encode_lhz(rd: Register, d: i16, ra: Register) -> u32 {
  let rd = rd as u32;
  let ra = ra as u32;
  let d = d as u32;

  d & 0xffff | (ra << 16) | (rd << 21) | Opcode::LHZ as u32
}

pub fn encode_lbz(t: Register, a: i16, d: Register) -> u32 {
  let t = t as u32;
  let a = a as u32;
  let d = d as u32;

  a & 0xffff | (d << 16) | (t << 21) | opcode(34)
}

pub fn encode_lbzu(t: Register, a: i16, d: Register) -> u32 {
  let t = t as u32;
  let a = a as u32;
  let d = d as u32;

  a & 0xffff | (d << 16) | (t << 21) | opcode(35)
}

pub fn encode_lwz(t: Register, d: i16, a: Register) -> u32 {
  let t = t as u32;
  let a = a as u32;
  let d = d as u32;

  d & 0xffff | (a << 16) | (t << 21) | opcode(32)
}

pub fn encode_lwzu(t: Register, d: i16, a: Register) -> u32 {
  let t = t as u32;
  let a = a as u32;
  let d = d as u32;

  d & 0xffff | (a << 16) | (t << 21) | opcode(33)
}

pub fn encode_rlwinm(a: Register, s: Register, sh: i16, mb: i16, me: i16, rc: u8) -> u32 {
  let a = a as u32;
  let s = s as u32;
  let sh = sh as u32;
  let mb = mb as u32;
  let me = me as u32;
  let rc = rc as u32;

  rc | (me << 1) | (mb << 6) | (sh << 11) | (a << 16) | (s << 21) | Opcode::RLWINM as u32
}

pub fn or(a: Register, s: Register, b: Register) -> u32 {
  let s = s as u32;
  let a = a as u32;
  let b = b as u32;
  (444 << 1) | (b << 11) | (a << 16) | (s << 21) | Opcode::AND as u32
}

// pub fn ptr_ha(addr: u32) -> u16 {
//   ((addr & 0xFFFF0000) >> 16) as u16
// }

// pub fn ptr_l(addr: u32) -> u16 {
//   (addr & 0xFFFF) as u16
// }

pub fn encode_instructions(base_address: u32, symbols: &Symbols, instructions: &[Instruction]) -> Vec<u8> {
  let mut out = Vec::with_capacity(instructions.len() * 4);
  let mut cs = Cursor::new(&mut out);
  let mut address = base_address;
  let resolve = |addr: &Address| -> u32 {
    match addr {
      Address::Absolute(a) => *a,
      Address::Label(l) => symbols.get(l),
    }
  };
  for i in instructions {
    match i.clone() {
      Instruction::add(a, b, c) => {
        cs.write_u32_be(encode_add(a, b, c)).unwrap();
      }
      Instruction::mulli(a, b, simm) => {
        cs.write_u32_be(encode_mulli(a, b, simm)).unwrap();
      }
      Instruction::long(d) => {
        cs.write_u32_be(d).unwrap();
      }
      Instruction::addi(t, a, si) => {
        cs.write_u32_be(encode_addi(t, a, si)).unwrap();
      }
      Instruction::addis(t, a, si) => {
        cs.write_u32_be(encode_addis(t, a, si)).unwrap();
      }
      Instruction::b(t) => {
        cs.write_u32_be(encode_branch(address as i32, resolve(&t) as i32, 0, 0)).unwrap();
      }
      Instruction::bl(t) => {
        cs.write_u32_be(encode_branch(address as i32, resolve(&t) as i32, 0, 1)).unwrap();
      }
      Instruction::beq(t) => {
        cs.write_u32_be(encode_branch_conditional(address as i32, 12, 2, resolve(&t) as i32, 0, 0))
          .unwrap();
      }
      Instruction::bne(t) => {
        cs.write_u32_be(encode_branch_conditional(address as i32, 4, 2, resolve(&t) as i32, 0, 0))
          .unwrap();
      }
      Instruction::blr => {
        cs.write_u32_be(0x4e800020).unwrap();
      }
      Instruction::bctr => {
        cs.write_u32_be(0x4e800420).unwrap();
      }
      Instruction::bctrl => {
        cs.write_u32_be(0x4e800421).unwrap();
      }
      Instruction::cmpw(r, si) => {
        cs.write_u32_be(encode_cmp(0, 0, r, si)).unwrap();
      }
      Instruction::cmpwi(r, si) => {
        cs.write_u32_be(encode_cmpi(0, 0, r, si)).unwrap();
      }
      Instruction::rlwinm(a, s, sh, mb, me) => {
        cs.write_u32_be(encode_rlwinm(a, s, sh, mb, me, 0)).unwrap();
      }
      Instruction::rlwinmd(a, s, sh, mb, me) => {
        cs.write_u32_be(encode_rlwinm(a, s, sh, mb, me, 1)).unwrap();
      }
      Instruction::nop => {
        cs.write_u32_be(or(Register::r0, Register::r0, Register::r0)).unwrap();
      }
      Instruction::li(t, si) => {
        cs.write_u32_be(encode_addi(t, Register::r0, si)).unwrap();
      }
      Instruction::lis(t, si) => {
        cs.write_u32_be(encode_addis(t, Register::r0, si)).unwrap();
      }
      Instruction::mflr(t) => {
        cs.write_u32_be(mfspr(t, 0b0100000000)).unwrap();
      }
      Instruction::mtlr(t) => {
        cs.write_u32_be(mtspr(t, 0b0100000000)).unwrap();
      }
      Instruction::mtctr(t) => {
        cs.write_u32_be(mtspr(t, 0b0100100000)).unwrap();
      }
      Instruction::mr(s, a) => {
        cs.write_u32_be(or(s, a, a)).unwrap();
      }
      Instruction::lbz(t, d, a) => {
        cs.write_u32_be(encode_lbz(t, d, a)).unwrap();
      }
      Instruction::lbzu(t, d, a) => {
        cs.write_u32_be(encode_lbzu(t, d, a)).unwrap();
      }
      Instruction::lhz(t, d, a) => {
        cs.write_u32_be(encode_lhz(t, d, a)).unwrap();
      }
      Instruction::lwz(t, d, a) => {
        cs.write_u32_be(encode_lwz(t, d, a)).unwrap();
      }
      Instruction::lwzu(t, d, a) => {
        cs.write_u32_be(encode_lwzu(t, d, a)).unwrap();
      }
      Instruction::stb(t, d, a) => {
        let t = t as u32;
        let a = a as u32;
        let d = d as u32;

        cs.write_u32_be(d & 0xffff | (a << 16) | (t << 21) | Opcode::STB as u32).unwrap();
      }
      Instruction::stw(t, d, a) => {
        let t = t as u32;
        let a = a as u32;
        let d = d as u32;

        cs.write_u32_be(d & 0xffff | (a << 16) | (t << 21) | Opcode::STW as u32).unwrap();
      }
      Instruction::stwu(t, d, a) => {
        let t = t as u32;
        let a = a as u32;
        let d = d as u32;

        cs.write_u32_be(d & 0xffff | (a << 16) | (t << 21) | Opcode::STWU as u32).unwrap();
      }
      Instruction::subi(t, a, si) => {
        cs.write_u32_be(encode_addi(t, a, -(si as i16) as u16)).unwrap();
      }
    }
    address += 0x4;
  }

  out
}

#[cfg(test)]
mod tests {
  use super::Register::*;
  use super::*;
  #[test]
  pub fn encode_beq() {
    assert_eq!(0x41820050, encode_branch_conditional(0x02000048, 12, 2, 0x02000098, 0, 0));
    assert_eq!(0x418201ac, encode_branch_conditional(0x0254ec40, 12, 2, 0x0254edec, 0, 0));
  }

  #[test]
  pub fn encodes_lxz() {
    assert_eq!(0xa00b0002, encode_lhz(r0, 0x2, r11));
    assert_eq!(0x8001000c, encode_lwz(r0, 0xc, r1));
    assert_eq!(0x88aa5bbb, encode_lbz(r5, 0x5bbb, r10));
  }

  #[test]
  pub fn encodes_rlwinm() {
    assert_eq!(0x54e9801e, encode_rlwinm(r9, r7, 0x10, 0x0, 0xf, 0));
  }

  #[test]
  pub fn encodes_rlwinmd() {
    assert_eq!(0x54e9801e + 1, encode_rlwinm(r9, r7, 0x10, 0x0, 0xf, 1));
  }

  #[test]
  pub fn encode_bl() {
    assert_eq!(0x4873aca9, encode_branch(0x02000068, 0x0273ad10, 0, 1));
  }

  #[test]
  pub fn encodes_addi() {
    assert_eq!(0x398c0001, encode_addi(r12, r12, 0x1));
  }

  #[test]
  pub fn encodes_cmpw() {
    assert_eq!(0x7c041800, encode_cmp(0, 0, r4, r3));
  }

  #[test]
  pub fn encodes_add() {
    assert_eq!(0x7fa60214, encode_add(r29, r6, r0));
  }

  #[test]
  pub fn encodes_mulli() {
    assert_eq!(0x1fa60010, encode_mulli(r29, r6, 0x10));
  }

  #[test]
  pub fn encode_bne() {
    assert_eq!(0x40820018, encode_branch_conditional(0x0211e744, 4, 2, 0x0211e75c, 0, 0));
  }

  #[test]
  pub fn encode_cmpwi() {
    assert_eq!(0x2c090100, encode_cmpi(0, 0, r9, 0x100));
  }
}

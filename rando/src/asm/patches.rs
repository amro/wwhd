use crate::asm::assembler::Instruction::*;
use crate::asm::assembler::Register::*;
use crate::asm::assembler::*;
use crate::asm::map::Symbols;
use crate::logic::items::ARC_NAME_POINTERS;
use crate::logic::Item;
use crate::logic::Item::*;
use crate::rpx::Rpx;
use crate::BeRead;
use crate::BeWrite;

use std::io::Read;
use std::io::Write;

enum Patch {
  Text(u32, Vec<Instruction>),
  Reloc(u32, u32),
}

fn patch(address: u32, instr: &[Instruction]) -> Patch {
  Patch::Text(address, instr.into())
}

fn reloc(address: u32, new_addr: u32) -> Patch {
  Patch::Reloc(address, new_addr)
}

fn data(data: u32) -> Instruction {
  Instruction::long(data)
}

fn custom_save_init() -> Vec<Patch> {
  vec![
    // patch(0x0270B620, &[bl("custom_save_func".into())]),
    patch(0x02721980, &[bl("custom_func_save".into())]),
    // patch(0x025B9A2C, &[bl("custom_save_func".into())]),
    // patch(0x025B9A54, &[bl("custom_save_func".into())]),
    // patch(0x025B9A50, &[
    //  bl("custom_save_func".into()),
    // ]),
  ]
}

fn patch_boss_items() -> Vec<Patch> {
  vec![
    // createItemForBoss
    patch(0x025d8a70, &[nop]),
    patch(0x025d8a84, &[li(r5, 0x15)]),
    // createDisappear
    // patch(0x025d9a0c, &[nop]),

    // daDisappear_Execute
    patch(0x0212456c, &[lbz(r4, 0x0b0, r12)]),
    patch(0x02124578, &[li(r7, 0x0)]),
    patch(0x02124580, &[li(r8, 0x0)]),
    // daBossItem_Create
    patch(0x020d0ae4, &[lbz(r4, 0x01ce, r30)]),
    // item_func_utuwa_heart
    patch(0x0254dca8, &[nop]),
    patch(0x0254dcd8, &[nop]),
    // remove after boss cutscene
    patch(0x24d602c, &[li(r3, 1), blr]),
  ]
}

fn change_get_item_sound() -> Vec<Patch> {
  vec![patch(0x023dbd88, &[b(Address::Absolute(0x023dbdb0))])]
}

fn remove_intro() -> Vec<Patch> {
  vec![patch(0x25aeef4, &[nop]), patch(0x25aef08, &[nop])]
}

fn patch_pots() -> Vec<Patch> {
  vec![
    // daTsubo::Act_c::damaged
    patch(0x024cc08c, &[b(Address::Label("create_pot_item".into()))]),
    patch(0x024cc134, &[b(Address::Label("create_pot_item".into()))]),
  ]
}

fn patch_tott_dance() -> Vec<Patch> {
  vec![patch(0x22efb2c, &[li(r0, 1)])]
}

// The death zone in between Forest Haven and Forbidden Woods disappears once you have Farore's Pearl.
// This makes it frustrating to make the trip to Forbidden Woods since you have to go all the way through Forest Haven every time you fail.
// So we change this void to always be there, even after you own Farore's Pearl.
fn keep_fh_deathzone() -> Vec<Patch> {
  vec![patch(0x24b2790, &[li(r3, 0x0)])]
}

fn patch_map_markers() -> Vec<Patch> {
  vec![patch(0x02678b74, &[blr])]
}

fn reveal_full_sea_chart() -> Vec<Patch> {
  vec![patch(0x025b7e9c, &[li(r12, 1)])]
}

fn set_starting_spawn_id(id: u16) -> Vec<Patch> {
  vec![patch(0x025b5090, &[li(r0, id)]), patch(0x025b50cc, &[li(r0, id)])]
}

fn set_starting_room_index(id: u16) -> Vec<Patch> {
  vec![patch(0x025b508c, &[li(r10, id)]), patch(0x025b50c8, &[li(r10, id)])]
}

fn patch_korl() -> Vec<Patch> {
  vec![
    patch(0x02474b58, &[b(0x02474bb0.into())]),
    patch(0x02474b7c, &[b(0x02474bb0.into())]),
    patch(0x02474b9c, &[b(0x02474f10.into())]),
    // TODO: this prevents KoRL from redirecting link,
    // but also lets link leave the map.
    patch(0x02474ec0, &[li(r3, 0)]),
    // allow link to enter korl
    patch(0x0247a58c, &[b(0x0247a5cc.into())]),
  ]
}

pub fn patch_starting_items(rpx: &mut Rpx, symbols: &Symbols, items: &[Item]) {
  let starting_items = symbols.get("STARTING_ITEMS");
  for (i, item) in items.iter().enumerate() {
    rpx.writer(starting_items + i as u32).write_u8(*item as u8).unwrap();
  }
}

fn patch_progressive_items(symbols: &Symbols) -> Vec<Patch> {
  let mut patches = Vec::new();
  patches.push(patch(0x025d7d68, &[bl("convert_progressive_item_id_for_createDemoItem".into())]));
  // TODO: these get overwritten by relocations at runtime, so we need a way to do this that works
  let swords = &[
    Item::HerosSword,
    Item::MasterSword,
    Item::MasterSwordHalfPower,
    Item::MasterSwordFullPower,
  ];
  let bows = &[Item::HerosBow, Item::FireAndIceArrows, Item::LightArrow];
  let wallets = &[Item::WalletUpgrade, Item::WalletUpgrade2];
  let bomb_bags = &[Item::BombBag60, Item::BombBag90];
  let quivers = &[Item::Quiver60, Item::Quiver99];
  let item_func_start = 0x101e3a98;

  for sword in swords {
    let addr = item_func_start + 4 * sword.id() as u32;
    let func = symbols.get("item_func_progressive_sword");
    patches.push(reloc(addr, func));
  }

  for bow in bows {
    let addr = item_func_start + 4 * bow.id() as u32;
    let func = symbols.get("item_func_progressive_bow");
    patches.push(reloc(addr, func));
  }

  for wallet in wallets {
    let addr = item_func_start + 4 * wallet.id() as u32;
    let func = symbols.get("item_func_progressive_wallet");
    patches.push(reloc(addr, func));
  }

  for bomb_bag in bomb_bags {
    let addr = item_func_start + 4 * bomb_bag.id() as u32;
    let func = symbols.get("item_func_progressive_bomb_bag");
    patches.push(reloc(addr, func));
  }

  for quiver in quivers {
    let addr = item_func_start + 4 * quiver.id() as u32;
    let func = symbols.get("item_func_progressive_quiver");
    patches.push(reloc(addr, func));
  }

  patches
}

/// Some items are missing get item models and/or field models,
/// so we copy over data from other items to make them usable
fn patch_missing_models(rpx: &mut Rpx, symbols: &Symbols) -> Vec<Patch> {
  let item_resources_offset = symbols.get("item_resource");
  let field_item_resources_offset = symbols.get("field_item_res");

  let copy_to_field_data = |rpx: &mut Rpx, source: Item, dest: Item| {
    let source_offset = item_resources_offset + source.id() as u32 * 0x24;
    let dest_offset = field_item_resources_offset + dest.id() as u32 * 0x1c;

    let mut data1 = vec![0u8; 0xd];
    let mut data2 = vec![0u8; 0x4];

    let arcname = ARC_NAME_POINTERS[source.id() as usize];
    if arcname == 0 {
      log::warn!("{:?} has no arcname pointer", source)
    }
    rpx.reader(source_offset + 0x08).read_exact(&mut data1).unwrap();
    rpx.reader(source_offset + 0x1c).read_exact(&mut data2).unwrap();

    rpx.writer(dest_offset).write_u32_be(arcname).unwrap();
    rpx.writer(dest_offset + 0x04).write_all(&data1).unwrap();
    rpx.writer(dest_offset + 0x14).write_all(&data2).unwrap();
  };

  let copy_to_item_data = |rpx: &mut Rpx, source: Item, dest: Item| {
    let source_offset = item_resources_offset + source.id() as u32 * 0x24;
    let dest_offset = item_resources_offset + dest.id() as u32 * 0x24;

    let mut data1 = vec![0u8; 0xd];
    let mut data2 = vec![0u8; 0x4];

    let arcname = ARC_NAME_POINTERS[source.id() as usize];
    if arcname == 0 {
      log::warn!("{:?} has no arcname pointer", source)
    }
    rpx.reader(source_offset + 0x08).read_exact(&mut data1).unwrap();
    rpx.reader(source_offset + 0x1c).read_exact(&mut data2).unwrap();

    rpx.writer(dest_offset).write_u32_be(arcname).unwrap();
    rpx.writer(dest_offset + 0x08).write_all(&data1).unwrap();
    rpx.writer(dest_offset + 0x1c).write_all(&data2).unwrap();
  };

  for sword in &[MasterSword, MasterSwordHalfPower, MasterSwordFullPower] {
    copy_to_item_data(rpx, HerosSword, *sword);
    copy_to_field_data(rpx, HerosSword, *sword);
  }

  for song in &[
    BalladOfGales,
    SongOfPassing,
    CommandMelody,
    EarthGodsLyric,
    WindsRequiem,
    WindGodsAria,
  ] {
    copy_to_item_data(rpx, WindWaker, *song);
    copy_to_field_data(rpx, WindWaker, *song);
  }

  copy_to_field_data(rpx, Hookshot, Hookshot);

  copy_to_item_data(rpx, GreenPotion, MagicMeterUpgrade);
  copy_to_field_data(rpx, GreenPotion, MagicMeterUpgrade);

  // overwritten by relocation
  // rpx.patch(0x0217f1d4, &[0xc0, 0x2c, 0x1e, 0xd8]);
  // rpx.patch_reloc(0x0217f1d2, 0x10011ed8);
  rpx.patch_reloc(0x0217f1d6, 0x10011ed8);

  let item_info_list_start = symbols.get("item_info");
  for i in 0..0xff + 1 {
    let addr = item_info_list_start + i * 4;
    let y = rpx.reader(addr + 1).read_u8().unwrap();
    if y == 0 {
      rpx.writer(addr + 1).write_u8(0x28).unwrap();
    }

    let radius = rpx.reader(addr + 2).read_u8().unwrap();
    if radius == 0 {
      rpx.writer(addr + 2).write_u8(0x28).unwrap();
    }
  }

  vec![patch(0x0217defc, &[b(Address::Absolute(0x217e18c))])]
}

pub fn apply_all_patches(rpx: &mut Rpx, symbols: &Symbols) {
  let mut patches = Vec::new();

  // for DRI: 0x2, 13
  // patches.extend(set_starting_spawn_id(0));
  // patches.extend(set_starting_room_index(11));
  patches.extend(set_starting_spawn_id(0x2));
  patches.extend(set_starting_room_index(13));
  patches.extend(patch_boss_items());
  patches.extend(patch_pots());
  patches.extend(remove_intro());
  patches.extend(change_get_item_sound());
  patches.extend(patch_tott_dance());
  patches.extend(keep_fh_deathzone());

  patches.extend(custom_save_init());
  patches.extend(patch_korl());
  patches.extend(patch_map_markers());
  patches.extend(reveal_full_sea_chart());
  patches.extend(patch_progressive_items(symbols));
  patches.extend(patch_missing_models(rpx, symbols));

  for p in patches {
    match p {
      Patch::Text(addr, data) => rpx.patch(addr, &encode_instructions(addr, symbols, &data)),
      Patch::Reloc(addr, new_addr) => rpx.patch_reloc(addr, new_addr),
    }
  }
}

pub fn put_item(rpx: &mut Rpx, addr: u32, item: Item) {
  rpx.patch(addr, &[item as u8]);
}

use std::collections::HashMap;
use std::io::Read;
use std::io::Seek;

use regex::Regex;

use crate::FormatError;

pub struct Symbols(HashMap<String, u32>);

impl Symbols {
  pub fn read_from<R: Read + Seek>(reader: &mut R) -> Result<Self, FormatError> {
    let mut full = String::new();
    reader.read_to_string(&mut full)?;
    let custom_symbols = Regex::new(r"(?m)^\s+0x([[:xdigit:]]+)\s+([[a-zA-Z1-9_]]+)$").unwrap();
    let mut symbols = HashMap::new();
    for (_, [address, symbol]) in custom_symbols.captures_iter(&full).map(|c| c.extract()) {
      log::info!("{}: {}", symbol, address);
      symbols.insert(symbol.into(), u32::from_str_radix(address, 16).unwrap());
    }

    let game_symbols = Regex::new(r"(?m)^\s+\w+\s+([[a-zA-Z1-9_]]+) = 0x([[:xdigit:]]+)$").unwrap();

    for (_, [symbol, address]) in game_symbols.captures_iter(&full).map(|c| c.extract()) {
      if address.starts_with('_') {
        continue;
      }
      log::info!("{}: {}", symbol, address);
      symbols.insert(symbol.into(), u32::from_str_radix(address, 16).unwrap());
    }

    Ok(Symbols(symbols))
  }

  pub fn get(&self, symbol: &str) -> u32 {
    *(self.0.get(symbol).expect(symbol))
  }
}

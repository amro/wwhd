#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[allow(dead_code)]
#[rustfmt::skip]
pub enum Item {
  Heart                     = 0x00,
  GreenRupee                = 0x01,
  BlueRupee                 = 0x02,
  YellowRupee               = 0x03,
  RedRupee                  = 0x04,
  PurpleRupee               = 0x05,
  OrangeRupee               = 0x06,
  PieceOfHeart              = 0x07,
  HeartContainer            = 0x08,
  SmallMagicJar             = 0x09,
  LargeMagicJar             = 0x0a,
  Bombs5                    = 0x0b,
  Bombs10                   = 0x0c,
  Bombs20                   = 0x0d,
  Bombs30                   = 0x0e,
  SilverRupee               = 0x0f,
  Arrows10                  = 0x10,
  Arrows20                  = 0x11,
  Arrows30                  = 0x12,
  DragonRoostCavernSmallKey = 0x13,
  Unknown14                 = 0x14,
  SmallKey                  = 0x15,
  Fairy                     = 0x16,
  PieceOfHeart2             = 0x17,
  PieceOfHeart3             = 0x18,
  PieceOfHeart4             = 0x19,
  YellowRupeeJoke           = 0x1a,
  ForbiddenWoodsSmallKey    = 0x1b,
  ForbiddenWoodsBigKey      = 0x1c,
  ForbiddenWoodsMap         = 0x1d,
  TripleHeart               = 0x1e,
  JoyPendant                = 0x1f,
  Telescope                 = 0x20,
  TingleTuner               = 0x21,
  WindWaker                 = 0x22,
  PictoBox                  = 0x23,
  SpoilsBag                 = 0x24,
  GrapplingHook             = 0x25,
  DeluxePictoBox            = 0x26,
  HerosBow                  = 0x27,
  PowerBracelets            = 0x28,
  IronBoots                 = 0x29,
  MagicArmor                = 0x2a,
  ForbiddenWoodsCompass     = 0x2b,
  BaitBag                   = 0x2c,
  Boomerang                 = 0x2d,
  Barehand                  = 0x2e,
  Hookshot                  = 0x2f,
  DeliveryBag               = 0x30,
  Bombs                     = 0x31,
  HerosClothes              = 0x32,
  SkullHammer               = 0x33,
  DekuLeaf                  = 0x34,
  FireAndIceArrows          = 0x35,
  LightArrow                = 0x36,
  HerosNewClothes           = 0x37,
  HerosSword                = 0x38,
  MasterSword               = 0x39,
  MasterSwordHalfPower      = 0x3a,
  HerosShield               = 0x3b,
  MirrorShield              = 0x3c,
  RecoveredHerosSword       = 0x3d,
  MasterSwordFullPower      = 0x3e,
  PieceOfHeartAlternate     = 0x3f,
  TowerOfTheGodsSmallKey    = 0x40,
  TowerOfTheGodsBigKey      = 0x41,
  PiratesCharm              = 0x42,
  HerosCharm                = 0x43,
  TowerOfTheGodsMap         = 0x44,
  SkullNecklace             = 0x45,
  BokoBabaSeed              = 0x46,
  GoldenFeather             = 0x47,
  KnightsCrest              = 0x48,
  RedChuJelly               = 0x49,
  GreenChuJelly             = 0x4a,
  BlueChuJelly              = 0x4b,
  DungeonMap                = 0x4c,
  Compass                   = 0x4d,
  BigKey                    = 0x4e,
  EmptyElixirBottle         = 0x4f,
  EmptyBottle               = 0x50,
  RedPotion                 = 0x51,
  GreenPotion               = 0x52,
  BluePotion                = 0x53,
  ElixirSoupHalf            = 0x54,
  ElixirSoup                = 0x55,
  BottledWater              = 0x56,
  FairyInBottle             = 0x57,
  ForestFirefly             = 0x58,
  ForestWater               = 0x59,
  Bottle1                   = 0x5a,
  DragonRoostCavernCompass  = 0x5b,
  DragonRoostCavernBigKey   = 0x5c,
  DragonRoostCavernMap      = 0x5d,
  WindTempleBigKey          = 0x5e,
  WindTempleMap             = 0x5f,
  WindTempleCompass         = 0x60,
  TriforceShard1            = 0x61,
  TriforceShard2            = 0x62,
  TriforceShard3            = 0x63,
  TriforceShard4            = 0x64,
  TriforceShard5            = 0x65,
  TriforceShard6            = 0x66,
  TriforceShard7            = 0x67,
  TriforceShard8            = 0x68,
  NayrusPearl               = 0x69,
  DinsPearl                 = 0x6a,
  FaroresPearl              = 0x6b,
  Knowledge                 = 0x6c,
  WindsRequiem              = 0x6d,
  BalladOfGales             = 0x6e,
  CommandMelody             = 0x6f,
  EarthGodsLyric            = 0x70,
  WindGodsAria              = 0x71,
  SongOfPassing             = 0x72,
  TowerOfTheGodsCompass     = 0x73,
  ForsakenFortressMap       = 0x74,
  ForsakenFortressCompass   = 0x75,
  EarthTempleSmallKey       = 0x76,
  SwiftSail                 = 0x77,
  Sail                      = 0x78,
  TriforceChart1Deciphered  = 0x79,
  TriforceChart2Deciphered  = 0x7a,
  TriforceChart3Deciphered  = 0x7b,
  TriforceChart4Deciphered  = 0x7c,
  TriforceChart5Deciphered  = 0x7d,
  TriforceChart6Deciphered  = 0x7e,
  TriforceChart7Deciphered  = 0x7f,
  TriforceChart8Deciphered  = 0x80,
  EarthTempleBigKey         = 0x81,
  AllPurposeBait            = 0x82,
  HyoiPear                  = 0x83,
  Esa1                      = 0x84,
  Esa2                      = 0x85,
  Esa3                      = 0x86,
  Esa4                      = 0x87,
  Esa5                      = 0x88,
  MagicBean                 = 0x89,
  BirdEsa10                 = 0x8a,
  EarthTempleMap            = 0x8b,
  TownFlower                = 0x8c,
  SeaFlower                 = 0x8d,
  ExoticFlower              = 0x8e,
  HerosFlag                 = 0x8f,
  BigCatchFlag              = 0x90,
  BigSaleFlag               = 0x91,
  Pinwheel                  = 0x92,
  SickleMoonFlag            = 0x93,
  SkullTowerIdol            = 0x94,
  FountainIdol              = 0x95,
  PostmanStatue             = 0x96,
  ShopGuruStatue            = 0x97,
  FathersLetter             = 0x98,
  NoteToMom                 = 0x99,
  MaggiesLetter             = 0x9a,
  MoblinsLetter             = 0x9b,
  CabanaDeed                = 0x9c,
  ComplimentaryID           = 0x9d,
  FillUpCoupon              = 0x9e,
  SalvageItem1              = 0x9f,
  SalvageItem2              = 0xa0,
  SalvageItem3              = 0xa1,
  UnknownA2                 = 0xa2,
  DragonTingleStatue        = 0xa3,
  ForbiddenTingleStatue     = 0xa4,
  GoddessTingleStatue       = 0xa5,
  EarthTingleStatue         = 0xa6,
  WindTingleStatue          = 0xa7,
  EarthTempleCompass        = 0xa8,
  WindTempleSmallKey        = 0xa9,
  HurricaneSpin             = 0xaa,
  WalletUpgrade             = 0xab,
  WalletUpgrade2            = 0xac,
  BombBag60                 = 0xad,
  BombBag90                 = 0xae,
  Quiver60                  = 0xaf,
  Quiver99                  = 0xb0,
  MagicPowerUp              = 0xb1,
  MagicMeterUpgrade         = 0xb2,
  TingleRupee1              = 0xb3,
  TingleRupee2              = 0xb4,
  TingleRupee3              = 0xb5,
  TingleRupee4              = 0xb6,
  TingleRupee5              = 0xb7,
  TingleRupee6              = 0xb8,
  Lithograph1               = 0xb9,
  Lithograph2               = 0xba,
  Lithograph3               = 0xbb,
  Lithograph4               = 0xbc,
  Lithograph5               = 0xbd,
  Lithograph6               = 0xbe,
  UnknownBF                 = 0xbf,
  UnknownC0                 = 0xc0,
  UnknownC1                 = 0xc1,
  SubmarineChart            = 0xc2,
  BeedlesChart              = 0xc3,
  PlatformChart             = 0xc4,
  LightRingChart            = 0xc5,
  SecretCaveChart           = 0xc6,
  SeaHeartsChart            = 0xc7,
  IslandHeartsChart         = 0xc8,
  GreatFairyChart           = 0xc9,
  OctoChart                 = 0xca,
  IncredibleChart           = 0xcb,
  TreasureChart7            = 0xcc,
  TreasureChart27           = 0xcd,
  TreasureChart21           = 0xce,
  TreasureChart13           = 0xcf,
  TreasureChart32           = 0xd0,
  TreasureChart19           = 0xd1,
  TreasureChart41           = 0xd2,
  TreasureChart26           = 0xd3,
  TreasureChart8            = 0xd4,
  TreasureChart37           = 0xd5,
  TreasureChart25           = 0xd6,
  TreasureChart17           = 0xd7,
  TreasureChart36           = 0xd8,
  TreasureChart22           = 0xd9,
  TreasureChart9            = 0xda,
  GhostShipChart            = 0xdb,
  TinglesChart              = 0xdc,
  TreasureChart14           = 0xdd,
  TreasureChart10           = 0xde,
  TreasureChart40           = 0xdf,
  TreasureChart3            = 0xe0,
  TreasureChart4            = 0xe1,
  TreasureChart28           = 0xe2,
  TreasureChart16           = 0xe3,
  TreasureChart18           = 0xe4,
  TreasureChart34           = 0xe5,
  TreasureChart29           = 0xe6,
  TreasureChart1            = 0xe7,
  TreasureChart35           = 0xe8,
  TreasureChart12           = 0xe9,
  TreasureChart6            = 0xea,
  TreasureChart24           = 0xeb,
  TreasureChart39           = 0xec,
  TreasureChart38           = 0xed,
  TreasureChart2            = 0xee,
  TreasureChart33           = 0xef,
  TreasureChart31           = 0xf0,
  TreasureChart23           = 0xf1,
  TreasureChart5            = 0xf2,
  TreasureChart20           = 0xf3,
  TreasureChart30           = 0xf4,
  TreasureChart15           = 0xf5,
  TreasureChart11           = 0xf6,
  TriforceChart8            = 0xf7,
  TreasureChart45           = 0xf8,
  TriforceChart6            = 0xf9,
  TriforceChart5            = 0xfa,
  TriforceChart4            = 0xfb,
  TriforceChart3            = 0xfc,
  TriforceChart2            = 0xfd,
  TriforceChart1            = 0xfe,
  NoEntryFF                 = 0xff,
}

#[allow(non_upper_case_globals)]
impl Item {
  pub const ProgressiveBow: Item = Item::HerosBow;
  pub const ProgressiveBombBag: Item = Item::BombBag60;
  pub const ProgressiveSword: Item = Item::HerosSword;

  pub fn id(&self) -> u8 {
    *self as u8
  }

  pub fn from_id(id: u8) -> Item {
    unsafe { std::mem::transmute(id) }
  }
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[allow(dead_code)]
pub enum DroppedItem {
  Heart = 0x00,
  GreenRupee = 0x01,
  BlueRupee = 0x02,
  YellowRupee = 0x03,
  RedRupee = 0x04,
  PurpleRupee = 0x05,
  OrangeRupee = 0x06,
  PieceOfHeart = 0x07,
  HeartContainer = 0x08,
  SmallMagicJar = 0x09,
  LargeMagicJar = 0x0a,
  Bombs5 = 0x0b,
  Bombs10 = 0x0c,
  Bombs20 = 0x0d,
  Bombs30 = 0x0e,
  SilverRupee = 0x0f,
  Arrows10 = 0x10,
  Arrows20 = 0x11,
  Arrows30 = 0x12,
  Unknown13 = 0x13,
  Unknown14 = 0x14,
  SmallKey = 0x15,
  Fairy = 0x16,
  Unknown17 = 0x17,
  Unknown18 = 0x18,
  Unknown19 = 0x19,
  YellowRupeeJokeMessage = 0x1a,
  Unknown1B = 0x1b,
  Unknown1C = 0x1c,
  Unknown1D = 0x1d,
  ThreeHearts = 0x1e,
  JoyPendant = 0x1f,

  RandomDropType00 = 0x20,
  RandomDropType01 = 0x21,
  RandomDropType02 = 0x22,
  RandomDropType03 = 0x23,
  RandomDropType04 = 0x24,
  RandomDropType05 = 0x25,
  RandomDropType06 = 0x26,
  RandomDropType07 = 0x27,
  RandomDropType08 = 0x28,
  RandomDropType09 = 0x29,
  RandomDropType0A = 0x2a,
  MultiDropType0B = 0x2b,
  MultiDropType0C = 0x2c,
  MultiDropType0D = 0x2d,
  MultiDropType0E = 0x2e,
  MultiDropType0F = 0x2f,
  MultiDropType10 = 0x30,
  MultiDropType11 = 0x31,
  MultiDropType12 = 0x32,
  MultiDropType13 = 0x33,
  MultiDropType14 = 0x34,
  RandomDropType15 = 0x35,
  RandomDropType16 = 0x36,
  RandomDropType17 = 0x37,
  RandomDropType18 = 0x38,
  RandomDropType19 = 0x39,
  RandomDropType1A = 0x3a,
  RandomDropType1B = 0x3b,
  RandomDropType1C = 0x3c,
  RandomDropType1D = 0x3d,
  RandomDropType1E = 0x3e,

  NoItem = 0x3f,
}

impl DroppedItem {
  pub fn id(&self) -> u8 {
    *self as u8
  }

  pub fn from_id(id: u8) -> DroppedItem {
    unsafe { std::mem::transmute(id) }
  }
}

#[rustfmt::skip]
pub const ARC_NAME_POINTERS: [u32; 0x100] = [
  0x00000000, // 0x00          Heart
  0x00000000, // 0x01          GreenRupee
  0x00000000, // 0x02          BlueRupee
  0x00000000, // 0x03          YellowRupee
  0x00000000, // 0x04          RedRupee
  0x00000000, // 0x05          PurpleRupee
  0x00000000, // 0x06          OrangeRupee
  0x00000000, // 0x07          PieceOfHeart
  0x00000000, // 0x08          HeartContainer
  0x00000000, // 0x09          SmallMagicJar
  0x00000000, // 0x0a          LargeMagicJar
  0x00000000, // 0x0b          Bombs5
  0x00000000, // 0x0c          Bombs10
  0x00000000, // 0x0d          Bombs20
  0x00000000, // 0x0e          Bombs30
  0x00000000, // 0x0f          SilverRupee
  0x00000000, // 0x10          Arrows10
  0x00000000, // 0x11          Arrows20
  0x00000000, // 0x12          Arrows30
  0x00000000, // 0x13          NoEntry13
  0x00000000, // 0x14          Unknown14
  0x1004e448, // 0x15 VkeyN    SmallKey
  0x00000000, // 0x16          Fairy
  0x00000000, // 0x17          NoEntry17
  0x00000000, // 0x18          NoEntry18
  0x00000000, // 0x19          NoEntry19
  0x00000000, // 0x1a          YellowRupeeJoke
  0x00000000, // 0x1b          NoEntry1B
  0x00000000, // 0x1c          NoEntry1C
  0x00000000, // 0x1d          NoEntry1D
  0x00000000, // 0x1e          TripleHeart
  0x00000000, // 0x1f          JoyPendant
  0x00000000, // 0x20          Telescope
  0x00000000, // 0x21          TingleTuner
  0x1004e578, // 0x22 Vtact    WindWaker
  0x00000000, // 0x23          PictoBox
  0x1004e4d0, // 0x24 VbagE    SpoilsBag
  0x1004e548, // 0x25 Vrope    GrapplingHook
  0x00000000, // 0x26          DeluxePictoBox
  0x00000000, // 0x27          HerosBow
  0x00000000, // 0x28          PowerBracelets
  0x00000000, // 0x29          IronBoots
  0x00000000, // 0x2a          MagicArmor
  0x00000000, // 0x2b          WaterBoots
  0x1004e4d8, // 0x2c VbagF    BaitBag
  0x1004e6b0, // 0x2d Boom     Boomerang
  0x00000000, // 0x2e          Barehand
  0x1004e4e8, // 0x2f Vhook    Hookshot
  0x1004e4c8, // 0x30 VbagH    DeliveryBag
  0x00000000, // 0x31          Bombs
  0x00000000, // 0x32          HerosClothes
  0x00000000, // 0x33          SkullHammer
  0x1004e4c0, // 0x34 Vleaf    DekuLeaf
  0x00000000, // 0x35          FireAndIceArrows
  0x00000000, // 0x36          LightArrow
  0x00000000, // 0x37          HerosNewClothes
  0x1004e558, // 0x38 VswoN    HerosSword
  0x00000000, // 0x39          MasterSword
  0x00000000, // 0x3a          MasterSwordHalfPower
  0x00000000, // 0x3b          HerosShield
  0x00000000, // 0x3c          MirrorShield
  0x00000000, // 0x3d          RecoveredHerosSword
  0x00000000, // 0x3e          MasterSwordFullPower
  0x00000000, // 0x3f          PieceOfHeartAlternate
  0x00000000, // 0x40          NoEntry40
  0x00000000, // 0x41          NoEntry41
  0x00000000, // 0x42          PiratesCharm
  0x00000000, // 0x43          HerosCharm
  0x00000000, // 0x44          GrassBall
  0x00000000, // 0x45          SkullNecklace
  0x00000000, // 0x46          BokoBabaSeed
  0x00000000, // 0x47          GoldenFeather
  0x00000000, // 0x48          KnightsCrest
  0x00000000, // 0x49          RedChuJelly
  0x00000000, // 0x4a          GreenChuJelly
  0x00000000, // 0x4b          BlueChuJelly
  0x1004e4b8, // 0x4c VdunM    DungeonMap
  0x1004e4b0, // 0x4d VdunC    Compass
  0x1004e698, // 0x4e key_01   BigKey
  0x00000000, // 0x4f          EmptyElixirBottle
  0x00000000, // 0x50          EmptyBottle
  0x1004e538, // 0x51 Med_r    RedPotion
  0x1004e530, // 0x52 Med_g    GreenPotion
  0x1004e528, // 0x53 Med_b    BluePotion
  0x00000000, // 0x54          ElixirSoupHalf
  0x00000000, // 0x55          ElixirSoup
  0x00000000, // 0x56          BottledWater
  0x00000000, // 0x57          FairyInBottle
  0x00000000, // 0x58          ForestFirefly
  0x00000000, // 0x59          ForestWater
  0x00000000, // 0x5a          Bottle1
  0x00000000, // 0x5b          Bottle2
  0x00000000, // 0x5c          Bottle3
  0x00000000, // 0x5d          Bottle4
  0x00000000, // 0x5e          Bottle5
  0x00000000, // 0x5f          Bottle6
  0x00000000, // 0x60          Bottle7
  0x00000000, // 0x61          TriforceShard1
  0x00000000, // 0x62          TriforceShard2
  0x00000000, // 0x63          TriforceShard3
  0x00000000, // 0x64          TriforceShard4
  0x00000000, // 0x65          TriforceShard5
  0x00000000, // 0x66          TriforceShard6
  0x00000000, // 0x67          TriforceShard7
  0x00000000, // 0x68          TriforceShard8
  0x1004ec24, // 0x69 Shinju_n NayrusPearl
  0x1004ec3c, // 0x6a Shinju_d DinsPearl
  0x1004ec48, // 0x6b Shinju_f FaroresPearl
  0x00000000, // 0x6c          Knowledge
  0x00000000, // 0x6d          WindsRequiem
  0x00000000, // 0x6e          BalladOfGales
  0x00000000, // 0x6f          CommandMelody
  0x00000000, // 0x70          EarthGodsLyric
  0x00000000, // 0x71          WindGodsAria
  0x00000000, // 0x72          SongOfPassing
  0x00000000, // 0x73          NoEntry73
  0x00000000, // 0x74          NoEntry74
  0x00000000, // 0x75          NoEntry75
  0x00000000, // 0x76          NoEntry76
  0x00000000, // 0x77          SwiftSail
  0x00000000, // 0x78          Sail
  0x00000000, // 0x79          TriforceChart1Deciphered
  0x00000000, // 0x7a          TriforceChart2Deciphered
  0x00000000, // 0x7b          TriforceChart3Deciphered
  0x00000000, // 0x7c          TriforceChart4Deciphered
  0x00000000, // 0x7d          TriforceChart5Deciphered
  0x00000000, // 0x7e          TriforceChart6Deciphered
  0x00000000, // 0x7f          TriforceChart7Deciphered
  0x00000000, // 0x80          TriforceChart8Deciphered
  0x00000000, // 0x81          NoEntry81
  0x00000000, // 0x82          AllPurposeBait
  0x00000000, // 0x83          HyoiPear
  0x00000000, // 0x84          Esa1
  0x00000000, // 0x85          Esa2
  0x00000000, // 0x86          Esa3
  0x00000000, // 0x87          Esa4
  0x00000000, // 0x88          Esa5
  0x00000000, // 0x89          MagicBean
  0x00000000, // 0x8a          BirdEsa10
  0x00000000, // 0x8b          NoEntry8B
  0x00000000, // 0x8c          TownFlower
  0x00000000, // 0x8d          SeaFlower
  0x00000000, // 0x8e          ExoticFlower
  0x00000000, // 0x8f          HerosFlag
  0x00000000, // 0x90          BigCatchFlag
  0x00000000, // 0x91          BigSaleFlag
  0x00000000, // 0x92          Pinwheel
  0x00000000, // 0x93          SickleMoonFlag
  0x00000000, // 0x94          SkullTowerIdol
  0x00000000, // 0x95          FountainIdol
  0x00000000, // 0x96          PostmanStatue
  0x00000000, // 0x97          ShopGuruStatue
  0x00000000, // 0x98          FathersLetter
  0x00000000, // 0x99          NoteToMom
  0x00000000, // 0x9a          MaggiesLetter
  0x00000000, // 0x9b          MoblinsLetter
  0x00000000, // 0x9c          CabanaDeed
  0x00000000, // 0x9d          ComplimentaryID
  0x00000000, // 0x9e          FillUpCoupon
  0x00000000, // 0x9f          SalvageItem1
  0x00000000, // 0xa0          SalvageItem2
  0x00000000, // 0xa1          SalvageItem3
  0x00000000, // 0xa2          UnknownA2
  0x00000000, // 0xa3          DragonTingleStatue
  0x00000000, // 0xa4          ForbiddenTingleStatue
  0x00000000, // 0xa5          GoddessTingleStatue
  0x00000000, // 0xa6          EarthTingleStatue
  0x00000000, // 0xa7          WindTingleStatue
  0x00000000, // 0xa8          NoEntryA8
  0x00000000, // 0xa9          NoEntryA9
  0x00000000, // 0xaa          HurricaneSpin
  0x00000000, // 0xab          WalletUpgrade
  0x00000000, // 0xac          WalletUpgrade2
  0x00000000, // 0xad          BombBag60
  0x00000000, // 0xae          BombBag90
  0x00000000, // 0xaf          Quiver60
  0x00000000, // 0xb0          Quiver99
  0x00000000, // 0xb1          MagicPowerUp
  0x00000000, // 0xb2          MagicMeterUpgrade
  0x00000000, // 0xb3          TingleRupee1
  0x00000000, // 0xb4          TingleRupee2
  0x00000000, // 0xb5          TingleRupee3
  0x00000000, // 0xb6          TingleRupee4
  0x00000000, // 0xb7          TingleRupee5
  0x00000000, // 0xb8          TingleRupee6
  0x00000000, // 0xb9          Lithograph1
  0x00000000, // 0xba          Lithograph2
  0x00000000, // 0xbb          Lithograph3
  0x00000000, // 0xbc          Lithograph4
  0x00000000, // 0xbd          Lithograph5
  0x00000000, // 0xbe          Lithograph6
  0x00000000, // 0xbf          UnknownBF
  0x00000000, // 0xc0          UnknownC0
  0x00000000, // 0xc1          UnknownC1
  0x00000000, // 0xc2          SubmarineChart
  0x00000000, // 0xc3          BeedlesChart
  0x00000000, // 0xc4          PlatformChart
  0x00000000, // 0xc5          LightRingChart
  0x00000000, // 0xc6          SecretCaveChart
  0x00000000, // 0xc7          SeaHeartsChart
  0x00000000, // 0xc8          IslandHeartsChart
  0x00000000, // 0xc9          GreatFairyChart
  0x00000000, // 0xca          OctoChart
  0x00000000, // 0xcb          IncredibleChart
  0x00000000, // 0xcc          TreasureChart7
  0x00000000, // 0xcd          TreasureChart27
  0x00000000, // 0xce          TreasureChart21
  0x00000000, // 0xcf          TreasureChart13
  0x00000000, // 0xd0          TreasureChart32
  0x00000000, // 0xd1          TreasureChart19
  0x00000000, // 0xd2          TreasureChart41
  0x00000000, // 0xd3          TreasureChart26
  0x00000000, // 0xd4          TreasureChart8
  0x00000000, // 0xd5          TreasureChart37
  0x00000000, // 0xd6          TreasureChart25
  0x00000000, // 0xd7          TreasureChart17
  0x00000000, // 0xd8          TreasureChart36
  0x00000000, // 0xd9          TreasureChart22
  0x00000000, // 0xda          TreasureChart9
  0x00000000, // 0xdb          GhostShipChart
  0x00000000, // 0xdc          TinglesChart
  0x00000000, // 0xdd          TreasureChart14
  0x00000000, // 0xde          TreasureChart10
  0x00000000, // 0xdf          TreasureChart40
  0x00000000, // 0xe0          TreasureChart3
  0x00000000, // 0xe1          TreasureChart4
  0x00000000, // 0xe2          TreasureChart28
  0x00000000, // 0xe3          TreasureChart16
  0x00000000, // 0xe4          TreasureChart18
  0x00000000, // 0xe5          TreasureChart34
  0x00000000, // 0xe6          TreasureChart29
  0x00000000, // 0xe7          TreasureChart1
  0x00000000, // 0xe8          TreasureChart35
  0x00000000, // 0xe9          TreasureChart12
  0x00000000, // 0xea          TreasureChart6
  0x00000000, // 0xeb          TreasureChart24
  0x00000000, // 0xec          TreasureChart39
  0x00000000, // 0xed          TreasureChart38
  0x00000000, // 0xee          TreasureChart2
  0x00000000, // 0xef          TreasureChart33
  0x00000000, // 0xf0          TreasureChart31
  0x00000000, // 0xf1          TreasureChart23
  0x00000000, // 0xf2          TreasureChart5
  0x00000000, // 0xf3          TreasureChart20
  0x00000000, // 0xf4          TreasureChart30
  0x00000000, // 0xf5          TreasureChart15
  0x00000000, // 0xf6          TreasureChart11
  0x00000000, // 0xf7          TriforceChart8
  0x00000000, // 0xf8          TreasureChart45
  0x00000000, // 0xf9          TriforceChart6
  0x00000000, // 0xfa          TriforceChart5
  0x00000000, // 0xfb          TriforceChart4
  0x00000000, // 0xfc          TriforceChart3
  0x00000000, // 0xfd          TriforceChart2
  0x00000000, // 0xfe          TriforceChart1
  0x00000000, // 0xff          NoEntryFF
];

#[rustfmt::skip]
pub const TEXTURE_NAME_POINTERS: [u32; 0x100] = [
  0x00000000, // 0x00                 Heart
  0x00000000, // 0x01                 GreenRupee
  0x00000000, // 0x02                 BlueRupee
  0x00000000, // 0x03                 YellowRupee
  0x00000000, // 0x04                 RedRupee
  0x00000000, // 0x05                 PurpleRupee
  0x00000000, // 0x06                 OrangeRupee
  0x00000000, // 0x07                 PieceOfHeart
  0x00000000, // 0x08                 HeartContainer
  0x00000000, // 0x09                 SmallMagicJar
  0x00000000, // 0x0a                 LargeMagicJar
  0x00000000, // 0x0b                 Bombs5
  0x00000000, // 0x0c                 Bombs10
  0x00000000, // 0x0d                 Bombs20
  0x00000000, // 0x0e                 Bombs30
  0x00000000, // 0x0f                 SilverRupee
  0x00000000, // 0x10                 Arrows10
  0x00000000, // 0x11                 Arrows20
  0x00000000, // 0x12                 Arrows30
  0x00000000, // 0x13                 NoEntry13
  0x00000000, // 0x14                 Unknown14
  0x1004e984, // 0x15 get_key.bti     SmallKey
  0x00000000, // 0x16                 Fairy
  0x00000000, // 0x17                 NoEntry17
  0x00000000, // 0x18                 NoEntry18
  0x00000000, // 0x19                 NoEntry19
  0x00000000, // 0x1a                 YellowRupeeJoke
  0x00000000, // 0x1b                 NoEntry1B
  0x00000000, // 0x1c                 NoEntry1C
  0x00000000, // 0x1d                 NoEntry1D
  0x00000000, // 0x1e                 TripleHeart
  0x00000000, // 0x1f                 JoyPendant
  0x00000000, // 0x20                 Telescope
  0x00000000, // 0x21                 TingleTuner
  0x00000000, // 0x22                 WindWaker
  0x00000000, // 0x23                 PictoBox
  0x00000000, // 0x24                 SpoilsBag
  0x00000000, // 0x25                 GrapplingHook
  0x00000000, // 0x26                 DeluxePictoBox
  0x00000000, // 0x27                 HerosBow
  0x00000000, // 0x28                 PowerBracelets
  0x00000000, // 0x29                 IronBoots
  0x00000000, // 0x2a                 MagicArmor
  0x00000000, // 0x2b                 WaterBoots
  0x00000000, // 0x2c                 BaitBag
  0x00000000, // 0x2d                 Boomerang
  0x00000000, // 0x2e                 Barehand
  0x00000000, // 0x2f                 Hookshot
  0x00000000, // 0x30                 DeliveryBag
  0x00000000, // 0x31                 Bombs
  0x00000000, // 0x32                 HerosClothes
  0x00000000, // 0x33                 SkullHammer
  0x00000000, // 0x34                 DekuLeaf
  0x00000000, // 0x35                 FireAndIceArrows
  0x00000000, // 0x36                 LightArrow
  0x00000000, // 0x37                 HerosNewClothes
  0x00000000, // 0x38                 HerosSword
  0x00000000, // 0x39                 MasterSword
  0x00000000, // 0x3a                 MasterSwordHalfPower
  0x00000000, // 0x3b                 HerosShield
  0x00000000, // 0x3c                 MirrorShield
  0x00000000, // 0x3d                 RecoveredHerosSword
  0x00000000, // 0x3e                 MasterSwordFullPower
  0x00000000, // 0x3f                 PieceOfHeartAlternate
  0x00000000, // 0x40                 NoEntry40
  0x00000000, // 0x41                 NoEntry41
  0x00000000, // 0x42                 PiratesCharm
  0x00000000, // 0x43                 HerosCharm
  0x00000000, // 0x44                 GrassBall
  0x00000000, // 0x45                 SkullNecklace
  0x00000000, // 0x46                 BokoBabaSeed
  0x00000000, // 0x47                 GoldenFeather
  0x00000000, // 0x48                 KnightsCrest
  0x00000000, // 0x49                 RedChuJelly
  0x00000000, // 0x4a                 GreenChuJelly
  0x00000000, // 0x4b                 BlueChuJelly
  0x1004e8a4, // 0x4c dungeon_map.bti DungeonMap
  0x1004ea2c, // 0x4d compass.bti     Compass
  0x1004ed88, // 0x4e boss_key.bti    BigKey
  0x00000000, // 0x4f                 EmptyElixirBottle
  0x00000000, // 0x50                 EmptyBottle
  0x00000000, // 0x51                 RedPotion
  0x00000000, // 0x52                 GreenPotion
  0x00000000, // 0x53                 BluePotion
  0x00000000, // 0x54                 ElixirSoupHalf
  0x00000000, // 0x55                 ElixirSoup
  0x00000000, // 0x56                 BottledWater
  0x00000000, // 0x57                 FairyInBottle
  0x00000000, // 0x58                 ForestFirefly
  0x00000000, // 0x59                 ForestWater
  0x00000000, // 0x5a                 Bottle1
  0x00000000, // 0x5b                 Bottle2
  0x00000000, // 0x5c                 Bottle3
  0x00000000, // 0x5d                 Bottle4
  0x00000000, // 0x5e                 Bottle5
  0x00000000, // 0x5f                 Bottle6
  0x00000000, // 0x60                 Bottle7
  0x00000000, // 0x61                 TriforceShard1
  0x00000000, // 0x62                 TriforceShard2
  0x00000000, // 0x63                 TriforceShard3
  0x00000000, // 0x64                 TriforceShard4
  0x00000000, // 0x65                 TriforceShard5
  0x00000000, // 0x66                 TriforceShard6
  0x00000000, // 0x67                 TriforceShard7
  0x00000000, // 0x68                 TriforceShard8
  0x00000000, // 0x69                 NayrusPearl
  0x00000000, // 0x6a                 DinsPearl
  0x00000000, // 0x6b                 FaroresPearl
  0x00000000, // 0x6c                 Knowledge
  0x00000000, // 0x6d                 WindsRequiem
  0x00000000, // 0x6e                 BalladOfGales
  0x00000000, // 0x6f                 CommandMelody
  0x00000000, // 0x70                 EarthGodsLyric
  0x00000000, // 0x71                 WindGodsAria
  0x00000000, // 0x72                 SongOfPassing
  0x00000000, // 0x73                 NoEntry73
  0x00000000, // 0x74                 NoEntry74
  0x00000000, // 0x75                 NoEntry75
  0x00000000, // 0x76                 NoEntry76
  0x00000000, // 0x77                 SwiftSail
  0x00000000, // 0x78                 Sail
  0x00000000, // 0x79                 TriforceChart1Deciphered
  0x00000000, // 0x7a                 TriforceChart2Deciphered
  0x00000000, // 0x7b                 TriforceChart3Deciphered
  0x00000000, // 0x7c                 TriforceChart4Deciphered
  0x00000000, // 0x7d                 TriforceChart5Deciphered
  0x00000000, // 0x7e                 TriforceChart6Deciphered
  0x00000000, // 0x7f                 TriforceChart7Deciphered
  0x00000000, // 0x80                 TriforceChart8Deciphered
  0x00000000, // 0x81                 NoEntry81
  0x00000000, // 0x82                 AllPurposeBait
  0x00000000, // 0x83                 HyoiPear
  0x00000000, // 0x84                 Esa1
  0x00000000, // 0x85                 Esa2
  0x00000000, // 0x86                 Esa3
  0x00000000, // 0x87                 Esa4
  0x00000000, // 0x88                 Esa5
  0x00000000, // 0x89                 MagicBean
  0x00000000, // 0x8a                 BirdEsa10
  0x00000000, // 0x8b                 NoEntry8B
  0x00000000, // 0x8c                 TownFlower
  0x00000000, // 0x8d                 SeaFlower
  0x00000000, // 0x8e                 ExoticFlower
  0x00000000, // 0x8f                 HerosFlag
  0x00000000, // 0x90                 BigCatchFlag
  0x00000000, // 0x91                 BigSaleFlag
  0x00000000, // 0x92                 Pinwheel
  0x00000000, // 0x93                 SickleMoonFlag
  0x00000000, // 0x94                 SkullTowerIdol
  0x00000000, // 0x95                 FountainIdol
  0x00000000, // 0x96                 PostmanStatue
  0x00000000, // 0x97                 ShopGuruStatue
  0x00000000, // 0x98                 FathersLetter
  0x00000000, // 0x99                 NoteToMom
  0x00000000, // 0x9a                 MaggiesLetter
  0x00000000, // 0x9b                 MoblinsLetter
  0x00000000, // 0x9c                 CabanaDeed
  0x00000000, // 0x9d                 ComplimentaryID
  0x00000000, // 0x9e                 FillUpCoupon
  0x00000000, // 0x9f                 SalvageItem1
  0x00000000, // 0xa0                 SalvageItem2
  0x00000000, // 0xa1                 SalvageItem3
  0x00000000, // 0xa2                 UnknownA2
  0x00000000, // 0xa3                 DragonTingleStatue
  0x00000000, // 0xa4                 ForbiddenTingleStatue
  0x00000000, // 0xa5                 GoddessTingleStatue
  0x00000000, // 0xa6                 EarthTingleStatue
  0x00000000, // 0xa7                 WindTingleStatue
  0x00000000, // 0xa8                 NoEntryA8
  0x00000000, // 0xa9                 NoEntryA9
  0x00000000, // 0xaa                 HurricaneSpin
  0x00000000, // 0xab                 WalletUpgrade
  0x00000000, // 0xac                 WalletUpgrade2
  0x00000000, // 0xad                 BombBag60
  0x00000000, // 0xae                 BombBag90
  0x00000000, // 0xaf                 Quiver60
  0x00000000, // 0xb0                 Quiver99
  0x00000000, // 0xb1                 MagicPowerUp
  0x00000000, // 0xb2                 MagicMeterUpgrade
  0x00000000, // 0xb3                 TingleRupee1
  0x00000000, // 0xb4                 TingleRupee2
  0x00000000, // 0xb5                 TingleRupee3
  0x00000000, // 0xb6                 TingleRupee4
  0x00000000, // 0xb7                 TingleRupee5
  0x00000000, // 0xb8                 TingleRupee6
  0x00000000, // 0xb9                 Lithograph1
  0x00000000, // 0xba                 Lithograph2
  0x00000000, // 0xbb                 Lithograph3
  0x00000000, // 0xbc                 Lithograph4
  0x00000000, // 0xbd                 Lithograph5
  0x00000000, // 0xbe                 Lithograph6
  0x00000000, // 0xbf                 UnknownBF
  0x00000000, // 0xc0                 UnknownC0
  0x00000000, // 0xc1                 UnknownC1
  0x00000000, // 0xc2                 SubmarineChart
  0x00000000, // 0xc3                 BeedlesChart
  0x00000000, // 0xc4                 PlatformChart
  0x00000000, // 0xc5                 LightRingChart
  0x00000000, // 0xc6                 SecretCaveChart
  0x00000000, // 0xc7                 SeaHeartsChart
  0x00000000, // 0xc8                 IslandHeartsChart
  0x00000000, // 0xc9                 GreatFairyChart
  0x00000000, // 0xca                 OctoChart
  0x00000000, // 0xcb                 IncredibleChart
  0x00000000, // 0xcc                 TreasureChart7
  0x00000000, // 0xcd                 TreasureChart27
  0x00000000, // 0xce                 TreasureChart21
  0x00000000, // 0xcf                 TreasureChart13
  0x00000000, // 0xd0                 TreasureChart32
  0x00000000, // 0xd1                 TreasureChart19
  0x00000000, // 0xd2                 TreasureChart41
  0x00000000, // 0xd3                 TreasureChart26
  0x00000000, // 0xd4                 TreasureChart8
  0x00000000, // 0xd5                 TreasureChart37
  0x00000000, // 0xd6                 TreasureChart25
  0x00000000, // 0xd7                 TreasureChart17
  0x00000000, // 0xd8                 TreasureChart36
  0x00000000, // 0xd9                 TreasureChart22
  0x00000000, // 0xda                 TreasureChart9
  0x00000000, // 0xdb                 GhostShipChart
  0x00000000, // 0xdc                 TinglesChart
  0x00000000, // 0xdd                 TreasureChart14
  0x00000000, // 0xde                 TreasureChart10
  0x00000000, // 0xdf                 TreasureChart40
  0x00000000, // 0xe0                 TreasureChart3
  0x00000000, // 0xe1                 TreasureChart4
  0x00000000, // 0xe2                 TreasureChart28
  0x00000000, // 0xe3                 TreasureChart16
  0x00000000, // 0xe4                 TreasureChart18
  0x00000000, // 0xe5                 TreasureChart34
  0x00000000, // 0xe6                 TreasureChart29
  0x00000000, // 0xe7                 TreasureChart1
  0x00000000, // 0xe8                 TreasureChart35
  0x00000000, // 0xe9                 TreasureChart12
  0x00000000, // 0xea                 TreasureChart6
  0x00000000, // 0xeb                 TreasureChart24
  0x00000000, // 0xec                 TreasureChart39
  0x00000000, // 0xed                 TreasureChart38
  0x00000000, // 0xee                 TreasureChart2
  0x00000000, // 0xef                 TreasureChart33
  0x00000000, // 0xf0                 TreasureChart31
  0x00000000, // 0xf1                 TreasureChart23
  0x00000000, // 0xf2                 TreasureChart5
  0x00000000, // 0xf3                 TreasureChart20
  0x00000000, // 0xf4                 TreasureChart30
  0x00000000, // 0xf5                 TreasureChart15
  0x00000000, // 0xf6                 TreasureChart11
  0x00000000, // 0xf7                 TriforceChart8
  0x00000000, // 0xf8                 TreasureChart45
  0x00000000, // 0xf9                 TriforceChart6
  0x00000000, // 0xfa                 TriforceChart5
  0x00000000, // 0xfb                 TriforceChart4
  0x00000000, // 0xfc                 TriforceChart3
  0x00000000, // 0xfd                 TriforceChart2
  0x00000000, // 0xfe                 TriforceChart1
  0x00000000, // 0xff                 NoEntryFF
];

use lazy_static::lazy_static;

use crate::dzx::ChunkLayer;
use crate::logic::Item;

#[derive(Debug, Clone, Copy)]
pub enum Category {
  Gifts,
  Dungeons,
  DragonRoostCavern,
  Windfall,
  Sidequests,
  Minigames,
  Exploration,
  Collection,
  Secrets,
  Tingle,
}

pub struct Location {
  pub name: &'static str,
  pub original: Item,
  pub targets: &'static [PatchTarget],
  pub categories: &'static [Category],
}

pub enum PatchTarget {
  Rpx(u32),
  Chest(Szs, LayeredIndex),
  Actor(Szs, LayeredIndex),
  Event(Szs, u32, u32, u32),
}

fn make_locations(locs: Vec<(&'static str, Item, &'static [PatchTarget], &'static [Category])>) -> Vec<Location> {
  locs
    .into_iter()
    .map(|t| Location {
      name: t.0,
      original: t.1,
      targets: t.2,
      categories: t.3,
    })
    .collect()
}

#[derive(Debug, Clone, Copy)]
pub enum LayeredIndex {
  LX(u8),
  L0(u8),
  L1(u8),
  L2(u8),
}

impl LayeredIndex {
  pub fn to_layer_and_index(self) -> (ChunkLayer, u8) {
    match self {
      LX(i) => (ChunkLayer::All, i),
      L0(i) => (ChunkLayer::Layer0, i),
      L1(i) => (ChunkLayer::Layer1, i),
      L2(i) => (ChunkLayer::Layer2, i),
    }
  }
}

use Category::*;
use PatchTarget::*;
#[derive(Clone, Copy, Debug)]
pub enum Szs {
  NewD2,
  NewD2R3,
  NewD2R6,
  NewD2R0,
  NewD2R13,
  NewD2R10,
  Omori,
  OmoriR0,
  Dra09,
  Sea,
  Nezumi,
}

#[rustfmt::skip]
impl Szs {
  pub fn to_filename(self) -> &'static str {
    match self {
        NewD2    => "M_NewD2_Stage.szs",
        NewD2R0  => "M_NewD2_Room0.szs",
        NewD2R3  => "M_NewD2_Room3.szs",
        NewD2R6  => "M_NewD2_Room6.szs",
        NewD2R13 => "M_NewD2_Room13.szs",
        NewD2R10 => "M_NewD2_Room10.szs",
        Omori    => "Omori_Stage.szs",
        OmoriR0  => "Omori_Room0.szs",
        Dra09    => "M_Dra09_Stage.szs",
        Sea      => "sea_Stage.szs",
        Nezumi   => "Pnezumi_Stage.szs",
    }
  }
}

use Item::*;
use LayeredIndex::*;
use Szs::*;

enum Loc {}

#[rustfmt::skip]
lazy_static! {
  pub static ref LOCATIONS: Vec<Location> = make_locations(vec![
    ("Windfall Island - Maggie Free Item",                                         MaggiesLetter,       &[Rpx  (0x0227c2a3)],                &[Gifts]),
    ("Windfall Island - Maggie Delivery Reward",                                   PieceOfHeart,        &[Rpx  (0x0227c2df)],                &[Sidequests]),
    ("Windfall Island - Ivan - Catch Bees",                                        PieceOfHeart,        &[Rpx  (0x02298bff)],                &[Sidequests]),
    ("Windfall Island - Pompie and Vera - Lenzo and Minenco Photo",                TreasureChart24,     &[Rpx  (0x101c399f)],                &[Sidequests]),
    ("Windfall Island - Minenco - Miss Windfall Photo",                            TreasureChart33,     &[Rpx  (0x101c39a3)],                &[Sidequests]),
    ("Windfall Island - People - POH",                                             PieceOfHeart,        &[Rpx  (0x101c399a)],                &[]),
    ("Windfall Island - People - POH",                                             PieceOfHeart,        &[Rpx  (0x101c39a7)],                &[]),
    ("Windfall Island - People - POH",                                             PieceOfHeart,        &[Rpx  (0x101c39ab)],                &[]),
    ("Windfall Island - Kamo - Full Moon Photo",                                   TreasureChart31,     &[Rpx  (0x101c39af)],                &[Sidequests]),
    ("Windfall Island - Jail - Tingle - Gift 1",                                   TingleTuner,         &[Rpx  (0x022e98ef)],                &[Gifts]),
    ("Windfall Island - Jail - Tingle - Gift 2",                                   TinglesChart,        &[Rpx  (0x022e9923)],                &[Gifts]),
    ("Windfall Island - Jail - Maze Chest",                                        PictoBox,            &[Chest(Nezumi,  LX(0x00))],         &[Gifts]),
    ("Spectacle Island - Barrel Shooting - Reward 1",                              PieceOfHeart,        &[Rpx  (0x1001c194)],                &[Minigames]),
    ("Spectacle Island - Barrel Shooting - Reward 2",                              PieceOfHeart,        &[Rpx  (0x1001c195)],                &[Minigames]),
    ("Spectacle Island - Barrel Shooting - Reward 3",                              TreasureChart17,     &[Rpx  (0x1001c196)],                &[Minigames]),
    ("Salvage Corp Gift",                                                          TreasureChart34,     &[Rpx  (0x101c63bf)],                &[Gifts]),
    ("Forest Haven - On Tree Branch",                                              DekuLeaf,            &[Actor(OmoriR0, L0(0x0)),
                                                                                                          Actor(OmoriR0, L1(0xB))],          &[Exploration]),
    ("Windfall Island - Mrs. Marie - Catch Killer Bees",                           PurpleRupee,         &[Rpx  (0x0223fc7f)],                &[Sidequests]),
    ("Windfall Island - Mrs. Marie - Give 1 Joy Pendant",                          RedRupee,            &[Rpx  (0x0223fd93)],                &[Collection]),
    ("Windfall Island - Mrs. Marie - Give 21 Joy Pendants",                        CabanaDeed,          &[Rpx  (0x0223fdc3)],                &[Collection]),
    ("Windfall Island - Mrs. Marie - Give 40 Joy Pendants",                        TreasureChart45,     &[Rpx  (0x0223febb)],                &[Collection]),

    ("Tott - Teach Rhythm",                                                        SongOfPassing,       &[Event(Sea, 0x0e3, 0x1, 0x3)],      &[Gifts]),

    // DRC
    ("Dragon Roost Cavern - First Room",                                           SmallKey,            &[Chest(NewD2,    LX(0x05))],        &[Dungeons, DragonRoostCavern]),
    ("Dragon Roost Cavern - Alcove With Water Jugs",                               DungeonMap,          &[Chest(NewD2,    LX(0x08))],        &[Dungeons, DragonRoostCavern]),
    ("Dragon Roost Cavern - Water Jug on Upper Shelf",                             JoyPendant,          &[Actor(NewD2R13, LX(0x05))],        &[Dungeons, DragonRoostCavern, Secrets]),
    ("Dragon Roost Cavern - Boarded Up Chest",                                     SmallKey,            &[Chest(NewD2,    LX(0x06))],        &[Dungeons, DragonRoostCavern]),
    ("Dragon Roost Cavern - Chest Across Lava Pit",                                TreasureChart11,     &[Chest(NewD2,    LX(0x0a))],        &[Dungeons, DragonRoostCavern]),
    ("Dragon Roost Cavern - Rat Room",                                             Compass,             &[Chest(NewD2,    LX(0x07))],        &[Dungeons, DragonRoostCavern]),
    ("Dragon Roost Cavern - Rat Room Boarded Up Chest",                            SmallKey,            &[Chest(NewD2,    LX(0x0b))],        &[Dungeons, DragonRoostCavern]),
    ("Dragon Roost Cavern - Bird's Nest",                                          SmallKey,            &[Actor(NewD2R3,  LX(0x16))],        &[Dungeons, DragonRoostCavern]),
    ("Dragon Roost Cavern - Dark Room",                                            JoyPendant,          &[Chest(NewD2,    LX(0x04))],        &[Dungeons, DragonRoostCavern]),
    ("Dragon Roost Cavern - Tingle Chest in Hub Room",                             YellowRupee,         &[Chest(NewD2,    LX(0x0d))],        &[Dungeons, DragonRoostCavern, Tingle]),
    ("Dragon Roost Cavern - Pot on Upper Shelf in Pot Room",                       JoyPendant,          &[Actor(NewD2R6,  LX(0x23))],        &[Dungeons, DragonRoostCavern, Secrets]),
    ("Dragon Roost Cavern - Pot Room Chest",                                       TreasureChart39,     &[Chest(NewD2,    LX(0x03))],        &[Dungeons, DragonRoostCavern]),
    ("Dragon Roost Cavern - Miniboss",                                             GrapplingHook,       &[Chest(Dra09,    LX(0x00))],        &[Dungeons, DragonRoostCavern]),

    ("Dragon Roost Cavern - Under Rope Bridge",                                    JoyPendant,          &[Chest(NewD2,    LX(0x09))],        &[Dungeons, DragonRoostCavern]),
    ("Dragon Roost Cavern - Tingle Statue Chest",                                  DragonTingleStatue,  &[Chest(NewD2,    LX(0x0C))],        &[Dungeons, DragonRoostCavern, Tingle]),

    ("Dragon Roost Cavern - Big Key Chest",                                        BigKey,              &[Chest(NewD2,    LX(0x00))],        &[Dungeons, DragonRoostCavern]),
    ("Dragon Roost Cavern - Boss Stairs Right Chest",                              KnightsCrest,        &[Chest(NewD2,    LX(0x01))],        &[Dungeons, DragonRoostCavern]),
    ("Dragon Roost Cavern - Boss Stairs Left Chest",                               YellowRupee,         &[Chest(NewD2,    LX(0x02))],        &[Dungeons, DragonRoostCavern]),
    ("Dragon Roost Cavern - Boss Stairs Right Pot",                                JoyPendant,          &[Actor(NewD2R10, LX(0x02))],        &[Dungeons, DragonRoostCavern]),
    ("Dragon Roost Cavern - Gohma Heart Container",                                HeartContainer,      &[Rpx  (0x020f5ed6)],                &[Dungeons, DragonRoostCavern]),
  ]);
  static ref UNUSED_LOCATIONS: Vec<Location> = make_locations(vec![
    ("Medli - Grappling Hook",                                                     GrapplingHook,       &[Rpx  (0x0228a85f)],                &[]),
    ("Medli - Empty Bottle",                                                       EmptyBottle,         &[Rpx  (0x0228a89b)],                &[]),
    ("Medli - Father's Letter",                                                    FathersLetter,       &[Rpx  (0x0228a8cf)],                &[]),
    ("Barrel Shooting - Reward 3+",                                                OrangeRupee,         &[Rpx  (0x0226901f)],                &[]),
    ("Chu Jelly Juice Shop - Red Potion Free Sample",                              RedPotion,           &[Rpx  (0x02231377)],                &[]),
    ("Chu Jelly Juice Shop - Green Potion Free Sample",                            GreenPotion,         &[Rpx  (0x022313cb)],                &[]),
    ("Chu Jelly Juice Shop - Blue Potion Free Sample",                             BluePotion,          &[Rpx  (0x022313c3)],                &[]),
    ("Mrs Marie - Give 1 or 2 Joy Pendants",                                       RedRupee,            &[Rpx  (0x0223fe83)],                &[]),
    ("Mrs Marie - Give 3 or 4 Joy Pendants",                                       PurpleRupee,         &[Rpx  (0x0223fea3)],                &[]),
    ("Mrs Marie - Give 5+ Joy Pendants",                                           OrangeRupee,         &[Rpx  (0x0223fe9b)],                &[]),
  ]);
}

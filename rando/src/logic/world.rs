use std::collections::HashMap;

use lazy_static::lazy_static;
use rand_xoshiro::rand_core::RngCore;
use rand_xoshiro::rand_core::SeedableRng;
use rand_xoshiro::Xoroshiro64StarStar;

type Condition = fn(&WorldInfo) -> bool;
type LocationId = &'static str;
type RegionId = &'static str;

fn always(_: &World) -> bool {
  true
}

use crate::logic::Item;
use Item::*;

use super::locations::Location;
use super::locations::LOCATIONS;

pub struct Region {
  name: &'static str,
  locations: Vec<(LocationId, Condition)>,
  exits: Vec<(RegionId, Condition)>,
}

#[rustfmt::skip]
lazy_static! {
  pub static ref REGIONS: Vec<Region> = vec![
    Region {
      name: "ROOT",
      locations: vec![],
      exits: vec![
        ("Sea",                                                                        |_: &_| true)
      ],
    },
    Region {
      name: "Sea",
      locations: vec![
        ("Salvage Corp Gift",                                                          |w: &_| w.can_sail()),
      ],
      exits: vec![
        ("Windfall Island",                                                            |w: &_| w.can_sail()),
        ("Dragon Roost Island",                                                        |w: &_| w.can_sail()),
        ("Forest Haven",                                                               |w: &_| w.can_sail()),
      ],
    },
    Region {
      name: "Windfall Island",
      locations: vec![
        ("Windfall Island - Tott - Teach Rhythm",                                      |w: &_| w.has(WindWaker)),
        ("Windfall Island - Ivan - Catch Bees",                                        |_: &_| true),
        ("Windfall Island - Pompie and Vera - Lenzo and Minenco Photo",                |w: &_| w.has(PictoBox)),
        ("Windfall Island - Kamo - Full Moon Photo",                                   |w: &_| w.has(DeluxePictoBox) && w.has(SongOfPassing)),
        ("Windfall Island - Minenco - Miss Windfall Photo",                            |w: &_| w.has(DeluxePictoBox)),
      ],
      exits: vec![
        ("Windfall Island - Auction House",                                            |_: &_| true),
        ("Windfall Island - Auction House Upstairs",                                   |_: &_| true),
        ("Windfall Island - School",                                                   |_: &_| true),
        ("Windfall Island - Jail",                                                     |_: &_| true),
      ],
    },
    Region {
      name: "Windfall Island - Auction House",
      locations: vec![
      ],
      exits: vec![
        ("Windfall Island",                                                            |_: &_| true),
      ],
    },
    Region {
      name: "Windfall Island - Auction House Upstairs",
      locations: vec![
        ("Windfall Island - Maggie Free Item",                                         |_: &_| true),
        ("Windfall Island - Maggie Delivery Reward",                                   |w: &_| w.has(DeliveryBag) && w.has(MoblinsLetter)),
      ],
      exits: vec![
        ("Windfall Island",                                                            |_: &_| true),
        ("Windfall Island - Auction House",                                            |_: &_| true),
      ],
    },
    Region {
      name: "Windfall Island - Jail",
      locations: vec![
        ("Windfall Island - Jail - Tingle - Gift 1",                                   |_: &_| true),
        ("Windfall Island - Jail - Tingle - Gift 2",                                   |_: &_| true),
        ("Windfall Island - Jail - Tingle - Maze Chest",                               |_: &_| true),
      ],
      exits: vec![
        ("Windfall Island",                                                            |_: &_| true),
      ],
    },
    Region {
      name: "Windfall Island - School",
      locations: vec![
        ("Windfall Island - Mrs. Marie - Catch Killer Bees",                           |w: &_| w.has(SpoilsBag)),
        ("Windfall Island - Mrs. Marie - Give 1 Joy Pendant",                          |w: &_| w.has(SpoilsBag)),
        ("Windfall Island - Mrs. Marie - Give 21 Joy Pendants",                        |w: &_| w.has(GrapplingHook) && w.has(SpoilsBag)),
        ("Windfall Island - Mrs. Marie - Give 40 Joy Pendants",                        |w: &_| w.has(GrapplingHook) && w.has(SpoilsBag)),
      ],
      exits: vec![
        ("Windfall Island",                                                            |_: &_| true),
      ],
    },

    Region {
      name: "Dragon Roost Island",
      locations: vec![
      ],
      exits: vec![
        ("Sea",                                                                        |_: &_| true),
        ("Dragon Roost Cavern",                                                        |_: &_| true),
      ],
    },

    Region {
      name: "Dragon Roost Cavern",
      locations: vec![
        ("Dragon Roost Cavern - First Room",                                           |_: &_| true),
        ("Dragon Roost Cavern - Alcove With Water Jugs",                               |w: &_| w.has_drc_keys(1)),
        ("Dragon Roost Cavern - Water Jug on Upper Shelf",                             |w: &_| w.has_drc_keys(1)),
        ("Dragon Roost Cavern - Boarded Up Chest",                                     |w: &_| w.has_drc_keys(1)),
        ("Dragon Roost Cavern - Chest Across Lava Pit",                                |w: &_| w.has_drc_keys(2) && (w.has(GrapplingHook) || w.can_fly())),
        ("Dragon Roost Cavern - Rat Room",                                             |w: &_| w.has_drc_keys(2)),
        ("Dragon Roost Cavern - Rat Room Boarded Up Chest",                            |w: &_| w.has_drc_keys(2)),
        ("Dragon Roost Cavern - Bird's Nest",                                          |w: &_| w.has_drc_keys(3)),
        ("Dragon Roost Cavern - Dark Room",                                            |w: &_| w.has_drc_keys(4)),
        ("Dragon Roost Cavern - Tingle Chest in Hub Room",                             |w: &_| w.has_drc_keys(4) && w.has_bombs()),
        ("Dragon Roost Cavern - Pot on Upper Shelf in Pot Room",                       |w: &_| w.has_drc_keys(4)),
        ("Dragon Roost Cavern - Pot Room Chest",                                       |w: &_| w.has_drc_keys(4)),
        ("Dragon Roost Cavern - Miniboss",                                             |w: &_| w.has_drc_keys(4) && w.has(GrapplingHook) && w.can_fly_outdoors()),
        ("Dragon Roost Cavern - Under Rope Bridge",                                    |w: &_| w.has_drc_keys(4) && w.has(GrapplingHook) && w.can_fly_outdoors()),
        ("Dragon Roost Cavern - Tingle Statue Chest",                                  |w: &_| w.has_drc_keys(1) && w.has(GrapplingHook) && w.has_bombs()),
        ("Dragon Roost Cavern - Big Key Chest",                                        |w: &_| w.has_drc_keys(1) && w.has(GrapplingHook) && w.can_stun_magtails()),
        ("Dragon Roost Cavern - Boss Stairs Right Chest",                              |w: &_| w.has_drc_keys(4) && (w.has_any(&[GrapplingHook, Hookshot]) || w.has_magic_arrows() || w.can_fly()) ),
        ("Dragon Roost Cavern - Boss Stairs Left Chest",                               |w: &_| w.has_drc_keys(4) && (w.has_any(&[GrapplingHook, Hookshot]) || w.has_magic_arrows() || w.can_fly()) ),
        ("Dragon Roost Cavern - Boss Stairs Right Pot",                                |w: &_| w.has_drc_keys(4) && (w.has_any(&[GrapplingHook, Hookshot]) || w.has_magic_arrows() || w.can_fly()) ),
        ("Dragon Roost Cavern - Gohma Heart Container",                                |w: &_| w.has_drc_keys(4) && w.has(GrapplingHook) && (w.has(Hookshot) || w.has_magic_arrows() || w.can_fly())),
      ],
      exits: vec![
        ("Dragon Roost Island",                                                         |_: &_| true),
      ],
    },
    Region {
      name: "Forest Haven",
      locations: vec![
        ("Forest Haven - On Tree Branch",                                              |w: &_| w.has(GrapplingHook)),
      ],
      exits: vec![
        ("Sea",                                                                        |_: &_| true),
      ],
    }
  ];
}

struct WorldInfo<'a> {
  items: &'a Vec<Item>,
}

impl<'a> WorldInfo<'a> {
  pub fn has(&self, item: Item) -> bool {
    self.items.contains(&item)
  }

  pub fn has_bombs(&self) -> bool {
    false
  }

  pub fn has_any(&self, items: &[Item]) -> bool {
    self.items.iter().any(|i| items.contains(i))
  }

  pub fn has_all(&self, items: &[Item]) -> bool {
    items.iter().all(|i| self.items.contains(i))
  }

  pub fn can_stun_magtails(&self) -> bool {
    false
  }

  pub fn can_sail(&self) -> bool {
    true
  }

  pub fn can_fly(&self) -> bool {
    self.has(DekuLeaf)
  }

  pub fn can_fly_outdoors(&self) -> bool {
    self.has_all(&[DekuLeaf, WindWaker, WindsRequiem])
  }

  pub fn has_magic_arrows(&self) -> bool {
    false
  }

  pub fn has_drc_keys(&self, n: usize) -> bool {
    self.items.iter().filter(|i| **i == DragonRoostCavernSmallKey).count() >= n
  }
}

struct Settings {
  goal: LocationId,
}

struct World {
  placements: HashMap<&'static str, Item>,
  rng: Xoroshiro64StarStar,
}

enum SearchResult {
  Locations(Vec<LocationId>),
  Items(Vec<LocationId>, Vec<Item>),
}

impl World {
  pub fn new() -> Self {
    World {
      placements: HashMap::new(),
      rng: Xoroshiro64StarStar::seed_from_u64(0xf1f),
    }
  }

  fn shuffle_pop<T>(&mut self, v: &mut Vec<T>) -> Option<T> {
    let len = v.len();
    if len > 0 {
      let idx = self.rng.next_u64() as usize % len;
      Some(v.swap_remove(idx))
    } else {
      None
    }
  }

  pub fn reachable(&self, seen_locs: &[LocationId], winfo: &'_ WorldInfo) -> SearchResult {
    let mut to_search: Vec<RegionId> = vec!["ROOT"];
    let mut seen_regions: Vec<RegionId> = Vec::new();
    let mut reachable_locs = Vec::new();
    let mut full_locs = Vec::new();
    let mut items = Vec::new();
    while let Some(next) = to_search.pop() {
      seen_regions.push(next);
      let next = REGIONS.iter().find(|r| r.name == next).expect(next);
      for (loc, condition) in &next.locations {
        if condition(winfo) {
          if let Some(item) = self.placements.get(*loc).cloned() {
            full_locs.push(*loc);
            if self.is_major_item(item) && !seen_locs.contains(loc) {
              items.push(item);
            }
          } else {
            reachable_locs.push(*loc);
          }
        }
      }
      for (reg, condition) in &next.exits {
        if !seen_regions.contains(reg) && condition(winfo) {
          to_search.push(*reg);
        }
      }
    }
    if items.is_empty() {
      SearchResult::Locations(reachable_locs)
    } else {
      SearchResult::Items(full_locs, items)
    }
  }

  pub fn randomize(&mut self) -> HashMap<LocationId, Item> {
    let mut item_pool = vec![HeartContainer, HeartContainer];
    let mut major_items = vec![
      GrapplingHook,
      Item::ProgressiveSword,
      Item::ProgressiveSword,
      DragonRoostCavernSmallKey,
      DragonRoostCavernSmallKey,
      DragonRoostCavernSmallKey,
      DragonRoostCavernSmallKey,
      DragonRoostCavernBigKey,
    ];

    while !major_items.is_empty() {
      let mut usable_items = major_items.clone();
      let mut seen = Vec::new();

      let mut locs = loop {
        match self.reachable(&seen, &WorldInfo { items: &usable_items }) {
          SearchResult::Locations(locs) => break locs,
          SearchResult::Items(locs, items) => {
            seen.extend(locs);
            usable_items.extend(items);
          }
        }
      };

      if locs.is_empty() {
        panic!();
      }

      if let (Some(item), Some(loc)) = (self.shuffle_pop(&mut major_items), self.shuffle_pop(&mut locs)) {
        println!("Placing {:?} at {}", item, loc);
        self.placements.insert(loc, item);
      } else {
        panic!();
      }
    }

    // place rest
    let mut locations: Vec<&Location> = LOCATIONS.iter().filter(|l| !self.placements.contains_key(&l.name)).collect();

    while !item_pool.is_empty() {
      if let (Some(item), Some(loc)) = (self.shuffle_pop(&mut item_pool), self.shuffle_pop(&mut locations)) {
        self.placements.insert(loc.name, item);
      } else {
        panic!();
      }
    }
    self.placements.clone()
  }

  #[rustfmt::skip]
  #[allow(clippy::match_like_matches_macro)]
  pub fn is_major_item(&self, item: Item) -> bool {
    match item {
      Item::DragonRoostCavernSmallKey => true,
      Item::SmallKey                  => true,
      Item::ForbiddenWoodsSmallKey    => true,
      Item::ForbiddenWoodsBigKey      => true,
      Item::ForbiddenWoodsMap         => true,
      Item::JoyPendant                => true,
      Item::WindWaker                 => true,
      Item::PictoBox                  => true,
      Item::SpoilsBag                 => true,
      Item::GrapplingHook             => true,
      Item::DeluxePictoBox            => true,
      Item::HerosBow                  => true,
      Item::PowerBracelets            => true,
      Item::IronBoots                 => true,
      Item::MagicArmor                => true,
      Item::ForbiddenWoodsCompass     => true,
      Item::BaitBag                   => true,
      Item::Boomerang                 => true,
      Item::Hookshot                  => true,
      Item::DeliveryBag               => true,
      Item::Bombs                     => true,
      Item::SkullHammer               => true,
      Item::DekuLeaf                  => true,
      Item::FireAndIceArrows          => true,
      Item::LightArrow                => true,
      Item::HerosSword                => true,
      Item::MasterSword               => true,
      Item::MasterSwordHalfPower      => true,
      Item::HerosShield               => true,
      Item::MirrorShield              => true,
      Item::MasterSwordFullPower      => true,
      Item::TowerOfTheGodsSmallKey    => true,
      Item::TowerOfTheGodsBigKey      => true,
      Item::TowerOfTheGodsMap         => true,
      Item::DungeonMap                => true,
      Item::Compass                   => true,
      Item::BigKey                    => true,
      Item::EmptyBottle               => true,
      Item::DragonRoostCavernCompass  => true,
      Item::DragonRoostCavernBigKey   => true,
      Item::DragonRoostCavernMap      => true,
      Item::WindTempleBigKey          => true,
      Item::WindTempleMap             => true,
      Item::WindTempleCompass         => true,
      Item::TriforceShard1            => true,
      Item::TriforceShard2            => true,
      Item::TriforceShard3            => true,
      Item::TriforceShard4            => true,
      Item::TriforceShard5            => true,
      Item::TriforceShard6            => true,
      Item::TriforceShard7            => true,
      Item::TriforceShard8            => true,
      Item::NayrusPearl               => true,
      Item::DinsPearl                 => true,
      Item::FaroresPearl              => true,
      Item::WindsRequiem              => true,
      Item::BalladOfGales             => true,
      Item::CommandMelody             => true,
      Item::EarthGodsLyric            => true,
      Item::WindGodsAria              => true,
      Item::SongOfPassing             => true,
      Item::TowerOfTheGodsCompass     => true,
      Item::ForsakenFortressMap       => true,
      Item::ForsakenFortressCompass   => true,
      Item::EarthTempleSmallKey       => true,
      Item::SwiftSail                 => true,
      Item::Sail                      => true,
      Item::EarthTempleBigKey         => true,
      Item::EarthTempleMap            => true,
      Item::MaggiesLetter             => true,
      Item::MoblinsLetter             => true,
      Item::CabanaDeed                => true,
      Item::DragonTingleStatue        => true,
      Item::ForbiddenTingleStatue     => true,
      Item::GoddessTingleStatue       => true,
      Item::EarthTingleStatue         => true,
      Item::WindTingleStatue          => true,
      Item::EarthTempleCompass        => true,
      Item::WindTempleSmallKey        => true,
      Item::WalletUpgrade             => true,
      Item::WalletUpgrade2            => true,
      Item::MagicPowerUp              => true,
      Item::MagicMeterUpgrade         => true,
      _                               => false
    }
  }
}

#[cfg(test)]
mod tests {
  use super::World;

  #[test]
  fn test_rando() {
    let mut world = World::new();
    println!("{:#?}", world.randomize());
  }
}

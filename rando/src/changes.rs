use std::io::Read;
use std::io::Write;

use crate::asm::map::Symbols;
use crate::formats::dzx::Scob;
use crate::formats::dzx::Tres;
use crate::formats::*;
use crate::logic::items::ARC_NAME_POINTERS;
use crate::logic::items::TEXTURE_NAME_POINTERS;
use crate::logic::DroppedItem;
use crate::logic::Item;
use crate::rpx::Rpx;

use self::dzx::Actr;
use self::dzx::ChunkLayer;
use self::dzx::DaItem;

pub fn add_chests_underground(manager: &mut FileManager) -> Result<(), FormatError> {
  let dzx = manager.get_dzx("LinkUG_Stage.szs")?;

  let tres = dzx.get_tres_mut(ChunkLayer::All).unwrap();

  tres[0].chest_item = Item::HerosSword.id();

  let new_tres = tres[0].clone();
  tres.push(new_tres);

  let new_tres = tres[0].clone();
  tres.push(new_tres);

  let new_tres = tres[0].clone();
  tres.push(new_tres);

  let base_x = tres[0].x_coord;

  // let mut new_tres = tres[0].clone();
  // new_tres.x_coord = base_x - 100.0;
  // new_tres.chest_item = Item::HerosBow.id();
  // tres.push(new_tres);

  // let mut new_tres = tres[0].clone();
  // new_tres.x_coord = base_x - 100.0;
  // new_tres.chest_item = Item::HerosBow.id();
  // tres.push(new_tres);

  let mut new_tres = tres[0].clone();
  new_tres.x_coord = base_x - 100.0;
  new_tres.chest_item = Item::DragonRoostCavernSmallKey.id();
  tres.push(new_tres);

  // let mut new_tres = tres[0].clone();
  // new_tres.x_coord = base_x - 200.0;
  // new_tres.chest_item = Item::WalletUpgrade.id();
  // tres.push(new_tres);

  let mut new_tres = tres[0].clone();
  new_tres.x_coord = base_x - 200.0;
  new_tres.chest_item = Item::WindTempleBigKey.id();
  tres.push(new_tres);

  let mut new_tres = tres[0].clone();
  new_tres.x_coord = base_x + 100.0;
  new_tres.chest_item = Item::BombBag60.id();
  tres.push(new_tres);

  let mut new_tres = tres[0].clone();
  new_tres.x_coord = base_x + 100.0;
  new_tres.chest_item = Item::BombBag60.id();
  tres.push(new_tres);

  let mut new_tres = tres[0].clone();
  new_tres.x_coord = base_x + 200.0;
  new_tres.chest_item = Item::PictoBox.id();
  tres.push(new_tres);

  let mut new_tres = tres[0].clone();
  new_tres.x_coord = base_x + 200.0;
  new_tres.chest_item = Item::PictoBox.id();
  tres.push(new_tres);

  Ok(())
}

pub fn drc_add_boulder_destroyers(manager: &mut FileManager) -> Result<(), FormatError> {
  let drc_room13 = manager.get_dzx("M_NewD2_Room13.szs")?;

  let mut swc00 = Scob {
    name: *b"SW_C00\0\0",
    params: 0x0000ff00,
    x_coord: 2635.0,
    y_coord: 0.0,
    z_coord: 227.0,
    aux_params_1: 0,
    y_rot: 0xc000,
    z_rot: 0xffff,
    enemy_number: 0xffff,
    x_scale: 32,
    y_scale: 16,
    z_scale: 16,
  };
  swc00.set_switch(5);
  swc00.set_behavior_type(3);

  drc_room13.get_scobs_mut(ChunkLayer::All).unwrap().push(swc00);

  let drc_room14 = manager.get_dzx("M_NewD2_Room14.szs")?;

  let mut swc00 = Scob {
    name: *b"SW_C00\0\0",
    params: 0x0000ff00,
    x_coord: -4002.0,
    y_coord: 1950.0,
    z_coord: -2156.0,
    aux_params_1: 0,
    y_rot: 0xa000,
    z_rot: 0xffff,
    enemy_number: 0xffff,
    x_scale: 32,
    y_scale: 16,
    z_scale: 16,
  };

  swc00.set_switch(6);
  swc00.set_behavior_type(3);

  drc_room14.get_scobs_mut(ChunkLayer::All).unwrap().push(swc00);

  Ok(())
}

pub fn drc_add_magic_jars(manager: &mut FileManager) -> Result<(), FormatError> {
  let drc = manager.get_dzx("M_NewD2_Room2.szs")?;
  for (i, actr) in drc
    .get_actors_mut(ChunkLayer::All)
    .unwrap()
    .iter_mut()
    .filter(|a| a.name == *b"Odokuro\0")
    .enumerate()
  {
    if i == 2 || i == 5 {
      actr.set_pot_item_id(DroppedItem::LargeMagicJar.id());
    }
  }

  let drc = manager.get_dzx("M_NewD2_Room10.szs")?;
  for (i, actr) in drc
    .get_actors_mut(ChunkLayer::All)
    .unwrap()
    .iter_mut()
    .filter(|a| a.name == *b"Odokuro\0")
    .enumerate()
  {
    if i == 0 || i == 9 {
      actr.set_pot_item_id(DroppedItem::LargeMagicJar.id());
    }
  }
  Ok(())
}

pub fn drc_remove_invisible_wall(manager: &mut FileManager) -> Result<(), FormatError> {
  let drc = manager.get_dzx("M_NewD2_Room2.szs")?;

  for scob in drc
    .get_scobs_mut(ChunkLayer::All)
    .unwrap()
    .iter_mut()
    .filter(|s| s.name == *b"Akabe\0\0\0")
  {
    scob.set_switch(0xff);
  }

  Ok(())
}

pub fn drc_add_medli_chest(manager: &mut FileManager) -> Result<(), FormatError> {
  let drc = manager.get_dzx("M_Dra09_Stage.szs")?;

  let mut chest = Tres {
    name: *b"takara2\0",
    params: 0xff000000,
    x_coord: -1620.81,
    y_coord: 13600.0,
    z_coord: 263.034,
    room_num: 9,
    y_rot: 0xcc16,
    chest_item: Item::GrapplingHook.id(),
    flag_id: 0xff,
  };
  chest.set_chest_type(2);
  chest.set_opened_flag(0x11);
  drc.add_new_tres().push(chest);

  Ok(())
}

pub fn misc(manager: &mut FileManager) -> Result<(), FormatError> {
  // manager.print_bfres("sea_Stage.szs")?;
  Ok(())
}

#[rustfmt::skip]
pub fn add_dungeon_items(manager: &mut FileManager, symbols: &Symbols) -> Result<(), FormatError> {
  use Item::*;

  let dungeon_items = [
    ("item_func_drc_small_key",    SmallKey,   DragonRoostCavernSmallKey),
    ("item_func_drc_big_key",      BigKey,     DragonRoostCavernBigKey),
    ("item_func_drc_map",          DungeonMap, DragonRoostCavernMap),
    ("item_func_drc_compass",      Compass,    DragonRoostCavernCompass),
    ("item_func_fw_small_key",     SmallKey,   ForbiddenWoodsSmallKey),
    ("item_func_fw_big_key",       BigKey,     ForbiddenWoodsBigKey),
    ("item_func_fw_map",           DungeonMap, ForbiddenWoodsMap),
    ("item_func_fw_compass",       Compass,    ForbiddenWoodsCompass),
    ("item_func_totg_small_key",   SmallKey,   TowerOfTheGodsSmallKey),
    ("item_func_totg_big_key",     BigKey,     TowerOfTheGodsBigKey),
    ("item_func_totg_map",         DungeonMap, TowerOfTheGodsMap),
    ("item_func_totg_compass",     Compass,    TowerOfTheGodsCompass),
    ("item_func_ff_map",           DungeonMap, ForsakenFortressMap),
    ("item_func_ff_compass",       Compass,    ForsakenFortressCompass),
    ("item_func_et_small_key",     SmallKey,   EarthTempleSmallKey),
    ("item_func_et_big_key",       BigKey,     EarthTempleBigKey),
    ("item_func_et_map",           DungeonMap, EarthTempleMap),
    ("item_func_et_compass",       Compass,    EarthTempleCompass),
    ("item_func_wt_small_key",     SmallKey,   WindTempleSmallKey),
    ("item_func_wt_big_key",       BigKey,     WindTempleBigKey),
    ("item_func_wt_map",           DungeonMap, WindTempleMap),
    ("item_func_wt_compass",       Compass,    WindTempleCompass),
  ];
  let item_resources_offset = symbols.get("item_resource");//0x101e4674;
  let field_item_resources_offset = symbols.get("field_item_res");//0x101e6a74;
  let item_info_start = symbols.get("item_info");//0x101e8674;

  let copy_item_data = |rpx: &mut Rpx, source: Item, dest: Item| {
    let item_resources_source = item_resources_offset + (source.id() as u32 * 0x24);
    let field_resources_source = field_item_resources_offset + (source.id() as u32 * 0x24) as u32;
    let item_resources = item_resources_offset + (dest.id() as u32 * 0x24) as u32;
    let field_resources = field_item_resources_offset + (dest.id() as u32 * 0x1c) as u32;

    let arcname = ARC_NAME_POINTERS[source.id() as usize];
    if arcname == 0 {
      log::warn!("{:?} has no arcname pointer", source)
    }

    let texture = TEXTURE_NAME_POINTERS[source.id() as usize];
    if texture == 0 {
      log::warn!("{:?} has no texture name pointer", source)
    }

    let mut data1 = vec![0u8; 0xd];
    let mut data2 = vec![0u8; 0x4];
    let mut data3 = vec![0u8; 0x7];
    let mut data4 = vec![0u8; 0x4];
    let mut data5 = vec![0u8; 0x3];
    let mut data6 = vec![0u8; 0x4];

    rpx.reader(item_resources_source + 0x08).read_exact(&mut data1).unwrap();
    rpx.reader(item_resources_source + 0x1c).read_exact(&mut data2).unwrap();
    rpx.reader(item_resources_source + 0x15).read_exact(&mut data3).unwrap();
    rpx.reader(item_resources_source + 0x20).read_exact(&mut data4).unwrap();
    rpx.reader(field_resources_source + 0x11).read_exact(&mut data5).unwrap();
    rpx.reader(field_resources_source + 0x18).read_exact(&mut data6).unwrap();

    rpx.writer(field_resources).write_u32_be(arcname).unwrap();
    rpx.writer(item_resources).write_u32_be(arcname).unwrap();
    rpx.writer(item_resources + 4).write_u32_be(texture).unwrap();

    rpx.writer(item_resources + 8).write_all(&data1).unwrap();
    rpx.writer(field_resources + 4).write_all(&data1).unwrap();

    rpx.writer(item_resources + 0x1c).write_all(&data2).unwrap();
    rpx.writer(field_resources + 0x14).write_all(&data2).unwrap();

    rpx.writer(item_resources + 0x15).write_all(&data3).unwrap();
    rpx.writer(item_resources + 0x20).write_all(&data4).unwrap();

    rpx.writer(field_resources + 0x11).write_all(&data5).unwrap();
    rpx.writer(field_resources + 0x18).write_all(&data6).unwrap();

    let item_info = match source {
      Item::SmallKey => 0x14281e05,
      Item::BigKey => 0x00282805,
      _ => 0x00282800,
    };

    let item_info_entry = item_info_start + (4 * dest.id() as u32) as u32;
    rpx.writer(item_info_entry).write_u32_be(item_info).unwrap();
  };

  for (func_name, base_item, item) in dungeon_items {
    copy_item_data(manager.get_rpx_mut(), base_item, item);
    let item_get_func_addr = symbols.get("item_func_ptr") + item.id() as u32 * 0x4;
    manager.get_rpx_mut().patch_reloc(item_get_func_addr, symbols.get(func_name));
  }

  let msg = manager.get_msbt("message_msbt.szs")?;
  for (name, base_item, item) in dungeon_items {
    let src_label = format!("{:05}", 101 + base_item.id() as u32);
    let new_label = format!("{:05}", 101 + item.id() as u32);
    let _ = msg.new_entry_from(&src_label, &new_label, name);
  }

  Ok(())
}

fn update_deku_leaf_model(actor: &mut Actr) {
  assert_eq!(&actor.name[0..7], b"itemDek");
  actor.name.fill(0);
  actor.name[0..4].copy_from_slice(b"item");
  actor.params = 0x01ff0000;
  actor.set_param(DaItem::ITEM_ID, Item::DekuLeaf.id());
  actor.set_param(DaItem::ITEM_PICKUP_FLAG, 0x2);
  actor.set_z_param(DaItem::ENABLE_ACTIVATION_SWITCH, 0xff);
}

pub fn fix_deku_leaf_model(manager: &mut FileManager) -> Result<(), FormatError> {
  let dzx = manager.get_dzx("Omori_Room0.szs")?;
  update_deku_leaf_model(dzx.get_actors_mut(ChunkLayer::Layer0).unwrap().get_mut(0x0).unwrap());
  update_deku_leaf_model(dzx.get_actors_mut(ChunkLayer::Layer1).unwrap().get_mut(0xb).unwrap());
  // manager.print_bfres("sea_Stage.szs")?;
  Ok(())
}

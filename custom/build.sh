#!/bin/bash
cargo build --release
powerpc-eabi-as -mregnames -m750cl custom.asm -o target/custom.o
powerpc-eabi-ld -Ttext 0x28f87f4 -T linker.ld target/custom.o -o target/custom.bin -Map target/map.txt "-(" ./target/powerpc-unknown-eabi/release/libcustom.a --gc-sections --print-gc-sections "-)" --oformat binary

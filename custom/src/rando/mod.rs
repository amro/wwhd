use crate::cking::types::CollectSlot;
use crate::cking::types::StageId;
use crate::cking::{self};

use self::items::give_item;
use self::items::Item;

pub mod items;

#[link_section = "data"]
#[no_mangle]
static mut STARTING_ITEMS: [Item; 64] = [Item::NoEntryFF; 64];

const EVENT_FLAGS: &[u16] = &[
  0x3510, 0x2a80, 0x0280, 0x0520, 0x2e01, 0x0f80, 0x0908, 0x2a08, 0x0902, 0x1f40, 0x0a80, 0x0901, 0x0a20, 0x1801, 0x0a08, 0x0808, 0x1f02,
  0x2f20, 0x3840, 0x2d04, 0x3802, 0x2d01, 0x2d02, 0x3201, 0x3380, 0x1001, 0x2e04, 0x2920, 0x1620, 0x3304, 0x2910, 0x1610, 0x3440, 0x3a20,
  0x2d08, 0x3980, 0x3b02, 0x4002,
];

fn generic_on_dungeon_bit(dungeon: StageId, bit: u32) {
  let mut sg = cking::SaveGlobal::new();
  let stage = sg.save_info().dan.stage_no;
  if stage == dungeon as i8 {
    sg.save_info_mut().memory.on_dungeon_item(bit as i32);
  } else {
    unsafe {
      sg.save_info_mut()
        .save_data
        .memory
        .get_unchecked_mut(dungeon as usize)
        .on_dungeon_item(bit as i32);
    }
  }
}

#[no_mangle]
extern "C" fn custom_func_save() {
  unsafe {
    cking::FUN_02720d34();
  }

  let mut sg = cking::SaveGlobal::new();
  let save_info = sg.save_info_mut();

  if save_info.save_data.player.collect.is_collect(CollectSlot::Charms, 0) {
    return;
  }

  for event in EVENT_FLAGS {
    save_info.save_data.event.on_bit(*event);
  }

  save_info.event.on_bit(0x0310);

  use cking::types::switch::*;

  {
    let misc = &mut save_info.save_data.memory[StageId::Misc as usize];

    misc.on_switch(MISC_SWITCH_OUTSET_WOODS_0);
    misc.on_switch(MISC_SWITCH_OUTSET_WOODS_1);
    misc.on_switch(MISC_SWITCH_OUTSET_WOODS_3);
    misc.on_switch(MISC_SWITCH_OUTSET_WOODS_7);
    misc.on_switch(MISC_SWITCH_DRI_POND);
  }

  {
    let sea = &mut save_info.save_data.memory[StageId::Sea as usize];
    sea.on_switch(SEA_SWITCH_ENDLESS_NIGHT);
    sea.on_switch(SEA_SWITCH_WINDFALL_INTRO);
    sea.on_switch(SEA_SWITCH_FCP_INTRO);
    sea.on_switch(SEA_SWITCH_FF2_INTRO);
  }

  {
    let drc = &mut save_info.save_data.memory[StageId::DragonRoostCavern as usize];
    drc.on_switch(DRC_SWITCH_GOSSIP);
    drc.on_switch(DRC_SWITCH_CAMERA_PAN_ENTRANCE);
    drc.on_switch(DRC_SWITCH_CAMERA_PAN_OUTSIDE);
    drc.on_switch(DRC_SWITCH_CAMERA_PAN_HUB);
  }

  {
    let fw = &mut save_info.save_data.memory[StageId::ForbiddenWoods as usize];
    fw.on_switch(FW_SWITCH_INTRO);
  }

  {
    let totg = &mut save_info.save_data.memory[StageId::TowerOfTheGods as usize];
    totg.on_switch(TOTG_SWITCH_INTRO);
    totg.on_switch(TOTG_SWITCH_OUTSIDE_INTRO);
  }

  {
    let et = &mut save_info.save_data.memory[StageId::EarthTemple as usize];
    et.on_switch(ET_SWITCH_GOSSIP);
  }

  {
    let hyrule = &mut save_info.save_data.memory[StageId::Hyrule as usize];
    hyrule.on_switch(HYRULE_SWITCH_INTRO);
    hyrule.on_switch(HYRULE_SWITCH_BLOCK_PUZZLE);
    // hyrule.set_switch(0, 0x00040040);
  }

  {
    let gt = &mut save_info.save_data.memory[StageId::GanonsTower as usize];
    // gt.set_switch(0, 0xf0042000);
    gt.on_switch(GT_SWITCH_INTRO);
    gt.on_switch(GT_SWITCH_CAMERA_LIGHTS);
    gt.on_switch(GT_SWITCH_CAMERA_SWITCHES);
    gt.on_switch(GT_SWITCH_PUPPET_GANON_INTRO);
    gt.on_switch(GT_SWITCH_PUPPET_GANON_OUTRO);
    gt.on_switch(GT_SWITCH_GANONDORF_INTRO);
  }

  for stage in &[
    StageId::DragonRoostCavern,
    StageId::ForsakenFortress,
    StageId::ForbiddenWoods,
    StageId::TowerOfTheGods,
    StageId::EarthTemple,
    StageId::WindTemple,
  ] {
    generic_on_dungeon_bit(*stage, 5);
  }

  save_info.save_data.player.get_bag_item.beast_flags = 0xff;
  save_info.save_data.player.get_bag_item.bait_flags = 0xff;

  unsafe {
    for item in STARTING_ITEMS.iter() {
      if *item == Item::NoEntryFF {
        break;
      }
      give_item(*item);
    }
  }
}

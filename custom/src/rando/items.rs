use crate::cking::types::StageId;
use crate::cking::PlayGlobal;
use crate::cking::SaveGlobal;
use crate::cking::{self};

use super::generic_on_dungeon_bit;

#[rustfmt::skip]
#[no_mangle]
extern "C" fn convert_progressive_item_id(item: Item) -> Item {
  match item {
    Item::HerosSword           => get_progressive_sword_id(),
    Item::MasterSword          => get_progressive_sword_id(),
    Item::MasterSwordHalfPower => get_progressive_sword_id(),
    Item::MasterSwordFullPower => get_progressive_sword_id(),
    Item::RecoveredHerosSword  => get_progressive_sword_id(),
    Item::HerosShield          => get_progressive_shield_id(),
    Item::MirrorShield         => get_progressive_shield_id(),
    Item::HerosBow             => get_progressive_bow_id(),
    Item::FireAndIceArrows     => get_progressive_bow_id(),
    Item::LightArrow           => get_progressive_bow_id(),
    Item::WalletUpgrade        => get_progressive_wallet_id(),
    Item::WalletUpgrade2       => get_progressive_wallet_id(),
    Item::BombBag60            => get_progressive_bomb_bag_id(),
    Item::BombBag99            => get_progressive_bomb_bag_id(),
    Item::Quiver60             => get_progressive_quiver_id(),
    Item::Quiver99             => get_progressive_quiver_id(),
    Item::PictoBox             => get_progressive_picto_box_id(),
    Item::DeluxePictoBox       => get_progressive_picto_box_id(),
    Item::MagicPowerUp         => get_progressive_magic_meter_id(),
    Item::MagicMeterUpgrade    => get_progressive_magic_meter_id(),
    _ => item,
  }
}

pub fn give_item(item: Item) {
  unsafe {
    cking::execItemGet(item as u8);
  }
}

fn get_progressive_sword_id() -> Item {
  let sg = SaveGlobal::new();
  match sg.save_info().save_data.player.collect.collect[0] {
    0 => Item::HerosSword,
    1 => Item::MasterSword,
    3 => Item::MasterSwordHalfPower,
    7 => Item::MasterSwordFullPower,
    _ => Item::GreenRupee,
  }
}

fn get_progressive_shield_id() -> Item {
  let sg = SaveGlobal::new();
  match sg.save_info().save_data.player.collect.collect[1] {
    0 => Item::HerosShield,
    1 => Item::MirrorShield,
    _ => Item::GreenRupee,
  }
}

fn get_progressive_bow_id() -> Item {
  let sg = SaveGlobal::new();

  match sg.save_info().save_data.player.item.items[0xc] {
    0 => Item::HerosBow,
    1 => Item::FireAndIceArrows,
    3 => Item::LightArrow,
    _ => Item::GreenRupee,
  }
}

fn get_progressive_wallet_id() -> Item {
  let sg = SaveGlobal::new();
  let current_wallet = sg.save_info().save_data.player.status_a.wallet_size;
  match current_wallet {
    0 => Item::WalletUpgrade,
    1 => Item::WalletUpgrade2,
    _ => Item::GreenRupee,
  }
}

fn get_progressive_bomb_bag_id() -> Item {
  let sg = SaveGlobal::new();
  let current_max = sg.save_info().save_data.player.item_max.bomb_num;
  match current_max {
    30 => Item::BombBag60,
    60 => Item::BombBag99,
    _ => Item::GreenRupee,
  }
}

fn get_progressive_quiver_id() -> Item {
  let sg = SaveGlobal::new();
  let current_max = sg.save_info().save_data.player.item_max.arrow_num;
  match current_max {
    30 => Item::Quiver60,
    60 => Item::Quiver99,
    _ => Item::GreenRupee,
  }
}

fn get_progressive_picto_box_id() -> Item {
  let sg = SaveGlobal::new();

  match sg.save_info().save_data.player.item.items[0x8] {
    0 => Item::PictoBox,
    1 => Item::DeluxePictoBox,
    _ => Item::GreenRupee,
  }
}

fn get_progressive_magic_meter_id() -> Item {
  let sg = SaveGlobal::new();

  let current_max = sg.save_info().save_data.player.status_a.max_magic;
  match current_max {
    0 => Item::MagicPowerUp,
    1 => Item::MagicMeterUpgrade,
    _ => Item::GreenRupee,
  }
}

#[no_mangle]
extern "C" fn item_func_progressive_sword() {
  let sg = SaveGlobal::new();
  unsafe {
    match sg.save_info().save_data.player.collect.collect[0] {
      0 => cking::item_func_sword(),
      1 => cking::item_func_master_sword(),
      3 => cking::item_func_lv3_sword(),
      7 => cking::item_func_master_sword_ex(),
      _ => {}
    }
  }
}

#[no_mangle]
extern "C" fn item_func_progressive_shield() {
  let sg = SaveGlobal::new();
  unsafe {
    match sg.save_info().save_data.player.collect.collect[1] {
      0 => cking::item_func_shield(),
      1 => cking::item_func_mirror_shield(),
      _ => {}
    }
  }
}

#[no_mangle]
extern "C" fn item_func_progressive_bow() {
  let sg = SaveGlobal::new();
  unsafe {
    match sg.save_info().save_data.player.item.items[0xc] {
      0 => cking::item_func_bow(),
      1 => cking::item_func_magic_arrow(),
      3 => cking::item_func_light_arrow(),
      _ => {}
    }
  }
}

#[no_mangle]
extern "C" fn item_func_progressive_wallet() {
  let mut sg = SaveGlobal::new();
  let current_wallet = sg.save_info().save_data.player.status_a.wallet_size;
  if current_wallet >= 2 {
    return;
  }
  sg.save_info_mut().save_data.player.status_a.wallet_size += 1;
}

#[no_mangle]
extern "C" fn item_func_progressive_bomb_bag() {
  let mut sg = SaveGlobal::new();
  let current_max = sg.save_info().save_data.player.item_max.bomb_num;
  if current_max == 30 {
    sg.save_info_mut().save_data.player.item_max.bomb_num = 60;
  } else {
    sg.save_info_mut().save_data.player.item_max.bomb_num = 99;
  }
}

#[no_mangle]
extern "C" fn item_func_progressive_quiver() {
  let mut sg = SaveGlobal::new();
  let current_max = sg.save_info().save_data.player.item_max.arrow_num;
  if current_max == 30 {
    sg.save_info_mut().save_data.player.item_max.arrow_num = 60;
  } else {
    sg.save_info_mut().save_data.player.item_max.arrow_num = 99;
  }
}

#[no_mangle]
extern "C" fn item_func_progressive_picto_box() {
  let sg = SaveGlobal::new();
  unsafe {
    match sg.save_info().save_data.player.item.items[0x8] {
      0 => cking::item_func_camera(),
      1 => cking::item_func_camera2(),
      _ => {}
    }
  }
}

#[no_mangle]
extern "C" fn item_func_progressive_magic_meter() {
  let sg = SaveGlobal::new();
  let current_max = sg.save_info().save_data.player.status_a.max_magic;
  if current_max == 0 {
    item_func_normal_magic_meter();
  } else if current_max == 16 {
    unsafe {
      cking::item_func_max_mp_up1();
    }
  }
}

#[no_mangle]
extern "C" fn item_func_normal_magic_meter() {
  let mut pg = PlayGlobal::new();
  pg.play_mut().item_magic_count = 16;
  pg.play_mut().item_max_magic_count = 16;
}

#[no_mangle]
extern "C" fn item_func_hurricane_spin() {
  let mut sg = SaveGlobal::new();
  sg.save_info_mut().save_data.event.on_bit(0x6901);
}

fn item_func_generic_small_key(dungeon: StageId) {
  let mut sg = cking::SaveGlobal::new();
  let mut pg = cking::PlayGlobal::new();
  let stage = sg.save_info().dan.stage_no;
  if stage == dungeon as i8 {
    let flag = pg.play_mut().get_stage_info().stage_id_flags & 1;
    // let flag = 0;
    if flag > 0 {
      unsafe {
        cking::item_func_small_key();
      }
    } else {
      sg.save_info_mut().memory.key_num += 1;
    }
  } else {
    sg.save_info_mut().save_data.memory[dungeon as usize].key_num += 1;
  }
}

#[no_mangle]
extern "C" fn item_func_drc_small_key() {
  item_func_generic_small_key(StageId::DragonRoostCavern);
}

#[no_mangle]
extern "C" fn item_func_drc_map() {
  generic_on_dungeon_bit(StageId::DragonRoostCavern, 0);
}

#[no_mangle]
extern "C" fn item_func_drc_compass() {
  generic_on_dungeon_bit(StageId::DragonRoostCavern, 1);
}

#[no_mangle]
extern "C" fn item_func_drc_big_key() {
  generic_on_dungeon_bit(StageId::DragonRoostCavern, 2);
}

#[no_mangle]
extern "C" fn item_func_fw_small_key() {
  item_func_generic_small_key(StageId::ForbiddenWoods);
}

#[no_mangle]
extern "C" fn item_func_fw_map() {
  generic_on_dungeon_bit(StageId::ForbiddenWoods, 0);
}

#[no_mangle]
extern "C" fn item_func_fw_compass() {
  generic_on_dungeon_bit(StageId::ForbiddenWoods, 1);
}

#[no_mangle]
extern "C" fn item_func_fw_big_key() {
  generic_on_dungeon_bit(StageId::ForbiddenWoods, 2);
}

#[no_mangle]
extern "C" fn item_func_totg_small_key() {
  item_func_generic_small_key(StageId::TowerOfTheGods);
}

#[no_mangle]
extern "C" fn item_func_totg_map() {
  generic_on_dungeon_bit(StageId::TowerOfTheGods, 0);
}

#[no_mangle]
extern "C" fn item_func_totg_compass() {
  generic_on_dungeon_bit(StageId::TowerOfTheGods, 1);
}

#[no_mangle]
extern "C" fn item_func_totg_big_key() {
  generic_on_dungeon_bit(StageId::TowerOfTheGods, 2);
}

#[no_mangle]
extern "C" fn item_func_et_small_key() {
  item_func_generic_small_key(StageId::EarthTemple);
}

#[no_mangle]
extern "C" fn item_func_et_map() {
  generic_on_dungeon_bit(StageId::EarthTemple, 0);
}

#[no_mangle]
extern "C" fn item_func_et_compass() {
  generic_on_dungeon_bit(StageId::EarthTemple, 1);
}

#[no_mangle]
extern "C" fn item_func_et_big_key() {
  generic_on_dungeon_bit(StageId::EarthTemple, 2);
}

#[no_mangle]
extern "C" fn item_func_wt_small_key() {
  item_func_generic_small_key(StageId::WindTemple);
}

#[no_mangle]
extern "C" fn item_func_wt_map() {
  generic_on_dungeon_bit(StageId::WindTemple, 0);
}

#[no_mangle]
extern "C" fn item_func_wt_compass() {
  generic_on_dungeon_bit(StageId::WindTemple, 1);
}

#[no_mangle]
extern "C" fn item_func_wt_big_key() {
  generic_on_dungeon_bit(StageId::WindTemple, 2);
}

#[no_mangle]
extern "C" fn item_func_ff_map() {
  generic_on_dungeon_bit(StageId::ForsakenFortress, 0);
}

#[no_mangle]
extern "C" fn item_func_ff_compass() {
  generic_on_dungeon_bit(StageId::ForsakenFortress, 1);
}

#[repr(u8)]
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[allow(dead_code)]
#[rustfmt::skip]
pub enum Item {
  Heart                     = 0x00,
  GreenRupee                = 0x01,
  BlueRupee                 = 0x02,
  YellowRupee               = 0x03,
  RedRupee                  = 0x04,
  PurpleRupee               = 0x05,
  OrangeRupee               = 0x06,
  PieceOfHeart              = 0x07,
  HeartContainer            = 0x08,
  SmallMagicJar             = 0x09,
  LargeMagicJar             = 0x0a,
  Bombs5                    = 0x0b,
  Bombs10                   = 0x0c,
  Bombs20                   = 0x0d,
  Bombs30                   = 0x0e,
  SilverRupee               = 0x0f,
  Arrows10                  = 0x10,
  Arrows20                  = 0x11,
  Arrows30                  = 0x12,
  DragonRoostCavernSmallKey = 0x13,
  Unknown14                 = 0x14,
  SmallKey                  = 0x15,
  Fairy                     = 0x16,
  PieceOfHeart2             = 0x17,
  PieceOfHeart3             = 0x18,
  PieceOfHeart4             = 0x19,
  YellowRupeeJoke           = 0x1a,
  ForbiddenWoodsSmallKey    = 0x1b,
  ForbiddenWoodsBigKey      = 0x1c,
  ForbiddenWoodsMap         = 0x1d,
  TripleHeart               = 0x1e,
  JoyPendant                = 0x1f,
  Telescope                 = 0x20,
  TingleTuner               = 0x21,
  WindWaker                 = 0x22,
  PictoBox                  = 0x23,
  SpoilsBag                 = 0x24,
  GrapplingHook             = 0x25,
  DeluxePictoBox            = 0x26,
  HerosBow                  = 0x27,
  PowerBracelets            = 0x28,
  IronBoots                 = 0x29,
  MagicArmor                = 0x2a,
  ForbiddenWoodsCompass     = 0x2b,
  BaitBag                   = 0x2c,
  Boomerang                 = 0x2d,
  Barehand                  = 0x2e,
  Hookshot                  = 0x2f,
  DeliveryBag               = 0x30,
  Bombs                     = 0x31,
  HerosClothes              = 0x32,
  SkullHammer               = 0x33,
  DekuLeaf                  = 0x34,
  FireAndIceArrows          = 0x35,
  LightArrow                = 0x36,
  HerosNewClothes           = 0x37,
  HerosSword                = 0x38,
  MasterSword               = 0x39,
  MasterSwordHalfPower      = 0x3a,
  HerosShield               = 0x3b,
  MirrorShield              = 0x3c,
  RecoveredHerosSword       = 0x3d,
  MasterSwordFullPower      = 0x3e,
  PieceOfHeartAlternate     = 0x3f,
  TowerOfTheGodsSmallKey    = 0x40,
  TowerOfTheGodsBigKey      = 0x41,
  PiratesCharm              = 0x42,
  HerosCharm                = 0x43,
  TowerOfTheGodsMap         = 0x44,
  SkullNecklace             = 0x45,
  BokoBabaSeed              = 0x46,
  GoldenFeather             = 0x47,
  KnightsCrest              = 0x48,
  RedChuJelly               = 0x49,
  GreenChuJelly             = 0x4a,
  BlueChuJelly              = 0x4b,
  DungeonMap                = 0x4c,
  Compass                   = 0x4d,
  BigKey                    = 0x4e,
  EmptyElixirBottle         = 0x4f,
  EmptyBottle               = 0x50,
  RedPotion                 = 0x51,
  GreenPotion               = 0x52,
  BluePotion                = 0x53,
  ElixirSoupHalf            = 0x54,
  ElixirSoup                = 0x55,
  BottledWater              = 0x56,
  FairyInBottle             = 0x57,
  ForestFirefly             = 0x58,
  ForestWater               = 0x59,
  Bottle1                   = 0x5a,
  DragonRoostCavernCompass  = 0x5b,
  DragonRoostCavernBigKey   = 0x5c,
  DragonRoostCavernMap      = 0x5d,
  WindTempleBigKey          = 0x5e,
  WindTempleMap             = 0x5f,
  WindTempleCompass         = 0x60,
  TriforceShard1            = 0x61,
  TriforceShard2            = 0x62,
  TriforceShard3            = 0x63,
  TriforceShard4            = 0x64,
  TriforceShard5            = 0x65,
  TriforceShard6            = 0x66,
  TriforceShard7            = 0x67,
  TriforceShard8            = 0x68,
  NayrusPearl               = 0x69,
  DinsPearl                 = 0x6a,
  FaroresPearl              = 0x6b,
  Knowledge                 = 0x6c,
  WindsRequiem              = 0x6d,
  BalladOfGales             = 0x6e,
  CommandMelody             = 0x6f,
  EarthGodsLyric            = 0x70,
  WindGodsAria              = 0x71,
  SongOfPassing             = 0x72,
  TowerOfTheGodsCompass     = 0x73,
  ForsakenFortressMap       = 0x74,
  ForsakenFortressCompass   = 0x75,
  EarthTempleSmallKey       = 0x76,
  SwiftSail                 = 0x77,
  Sail                      = 0x78,
  TriforceChart1Deciphered  = 0x79,
  TriforceChart2Deciphered  = 0x7a,
  TriforceChart3Deciphered  = 0x7b,
  TriforceChart4Deciphered  = 0x7c,
  TriforceChart5Deciphered  = 0x7d,
  TriforceChart6Deciphered  = 0x7e,
  TriforceChart7Deciphered  = 0x7f,
  TriforceChart8Deciphered  = 0x80,
  EarthTempleBigKey         = 0x81,
  AllPurposeBait            = 0x82,
  HyoiPear                  = 0x83,
  Esa1                      = 0x84,
  Esa2                      = 0x85,
  Esa3                      = 0x86,
  Esa4                      = 0x87,
  Esa5                      = 0x88,
  MagicBean                 = 0x89,
  BirdEsa10                 = 0x8a,
  EarthTempleMap            = 0x8b,
  TownFlower                = 0x8c,
  SeaFlower                 = 0x8d,
  ExoticFlower              = 0x8e,
  HerosFlag                 = 0x8f,
  BigCatchFlag              = 0x90,
  BigSaleFlag               = 0x91,
  Pinwheel                  = 0x92,
  SickleMoonFlag            = 0x93,
  SkullTowerIdol            = 0x94,
  FountainIdol              = 0x95,
  PostmanStatue             = 0x96,
  ShopGuruStatue            = 0x97,
  FathersLetter             = 0x98,
  NoteToMom                 = 0x99,
  MaggiesLetter             = 0x9a,
  MoblinsLetter             = 0x9b,
  CabanaDeed                = 0x9c,
  ComplimentaryID           = 0x9d,
  FillUpCoupon              = 0x9e,
  SalvageItem1              = 0x9f,
  SalvageItem2              = 0xa0,
  SalvageItem3              = 0xa1,
  UnknownA2                 = 0xa2,
  DragonTingleStatue        = 0xa3,
  ForbiddenTingleStatue     = 0xa4,
  GoddessTingleStatue       = 0xa5,
  EarthTingleStatue         = 0xa6,
  WindTingleStatue          = 0xa7,
  EarthTempleCompass        = 0xa8,
  WindTempleSmallKey        = 0xa9,
  HurricaneSpin             = 0xaa,
  WalletUpgrade             = 0xab,
  WalletUpgrade2            = 0xac,
  BombBag60                 = 0xad,
  BombBag99                 = 0xae,
  Quiver60                  = 0xaf,
  Quiver99                  = 0xb0,
  MagicPowerUp              = 0xb1,
  MagicMeterUpgrade         = 0xb2,
  TingleRupee1              = 0xb3,
  TingleRupee2              = 0xb4,
  TingleRupee3              = 0xb5,
  TingleRupee4              = 0xb6,
  TingleRupee5              = 0xb7,
  TingleRupee6              = 0xb8,
  Lithograph1               = 0xb9,
  Lithograph2               = 0xba,
  Lithograph3               = 0xbb,
  Lithograph4               = 0xbc,
  Lithograph5               = 0xbd,
  Lithograph6               = 0xbe,
  UnknownBF                 = 0xbf,
  UnknownC0                 = 0xc0,
  UnknownC1                 = 0xc1,
  SubmarineChart            = 0xc2,
  BeedlesChart              = 0xc3,
  PlatformChart             = 0xc4,
  LightRingChart            = 0xc5,
  SecretCaveChart           = 0xc6,
  SeaHeartsChart            = 0xc7,
  IslandHeartsChart         = 0xc8,
  GreatFairyChart           = 0xc9,
  OctoChart                 = 0xca,
  IncredibleChart           = 0xcb,
  TreasureChart7            = 0xcc,
  TreasureChart27           = 0xcd,
  TreasureChart21           = 0xce,
  TreasureChart13           = 0xcf,
  TreasureChart32           = 0xd0,
  TreasureChart19           = 0xd1,
  TreasureChart41           = 0xd2,
  TreasureChart26           = 0xd3,
  TreasureChart8            = 0xd4,
  TreasureChart37           = 0xd5,
  TreasureChart25           = 0xd6,
  TreasureChart17           = 0xd7,
  TreasureChart36           = 0xd8,
  TreasureChart22           = 0xd9,
  TreasureChart9            = 0xda,
  GhostShipChart            = 0xdb,
  TinglesChart              = 0xdc,
  TreasureChart14           = 0xdd,
  TreasureChart10           = 0xde,
  TreasureChart40           = 0xdf,
  TreasureChart3            = 0xe0,
  TreasureChart4            = 0xe1,
  TreasureChart28           = 0xe2,
  TreasureChart16           = 0xe3,
  TreasureChart18           = 0xe4,
  TreasureChart34           = 0xe5,
  TreasureChart29           = 0xe6,
  TreasureChart1            = 0xe7,
  TreasureChart35           = 0xe8,
  TreasureChart12           = 0xe9,
  TreasureChart6            = 0xea,
  TreasureChart24           = 0xeb,
  TreasureChart39           = 0xec,
  TreasureChart38           = 0xed,
  TreasureChart2            = 0xee,
  TreasureChart33           = 0xef,
  TreasureChart31           = 0xf0,
  TreasureChart23           = 0xf1,
  TreasureChart5            = 0xf2,
  TreasureChart20           = 0xf3,
  TreasureChart30           = 0xf4,
  TreasureChart15           = 0xf5,
  TreasureChart11           = 0xf6,
  TriforceChart8            = 0xf7,
  TreasureChart45           = 0xf8,
  TriforceChart6            = 0xf9,
  TriforceChart5            = 0xfa,
  TriforceChart4            = 0xfb,
  TriforceChart3            = 0xfc,
  TriforceChart2            = 0xfd,
  TriforceChart1            = 0xfe,
  NoEntryFF                 = 0xff,
}

pub mod types;

use self::types::GameInfo;
use self::types::PGameInfo;
use self::types::Play;
use self::types::SaveEvent;
use self::types::SaveInfo;

extern "C" {
  pub static GAME_INFO: *mut PGameInfo;
  pub fn GetInf() -> *mut GameInfo;
  pub fn onEventBit(mem: *mut SaveEvent, bit: u16);
  pub fn item_func_sword();
  pub fn item_func_master_sword();
  pub fn item_func_lv3_sword();
  pub fn item_func_master_sword_ex();

  pub fn item_func_small_key();

  pub fn item_func_bow();
  pub fn item_func_magic_arrow();
  pub fn item_func_light_arrow();

  pub fn item_func_shield();
  pub fn item_func_mirror_shield();
  pub fn item_func_camera();
  pub fn item_func_camera2();
  pub fn item_func_max_mp_up1();

  pub fn execItemGet(i: u8);

  pub fn FUN_02720d34();
}

pub struct SaveGlobal {
  pointer: *mut PGameInfo,
}

impl SaveGlobal {
  pub fn new() -> SaveGlobal {
    unsafe { SaveGlobal { pointer: GAME_INFO } }
  }

  pub fn save_info(&self) -> &SaveInfo {
    unsafe { &(*self.pointer).info.save }
  }

  pub fn save_info_mut(&mut self) -> &mut SaveInfo {
    unsafe { &mut (*self.pointer).info.save }
  }
}

pub struct PlayGlobal {
  pointer: *mut GameInfo,
}

impl PlayGlobal {
  pub fn new() -> PlayGlobal {
    let pointer = unsafe { GetInf() };
    PlayGlobal { pointer }
  }
  pub fn play_mut(&mut self) -> &mut Play {
    unsafe { &mut (*self.pointer).play }
  }
}

use core::ffi::c_void;

macro_rules! const_assert_eq_usize {
    ($x:expr, $($y:expr),+ $(,)?) => {
        $(const _: [(); $x] = [(); $y];)+
    };
}

#[repr(C)]
pub struct Membit {
  pub tbox: u32,
  pub switch: [u32; 4],
  pub item: [u32; 1],
  pub visited_room: [u32; 2],
  pub key_num: u8,
  pub dungeon_item: u8,
}

const_assert_eq_usize!(0x00, core::mem::offset_of!(Membit, tbox));
const_assert_eq_usize!(0x04, core::mem::offset_of!(Membit, switch));
const_assert_eq_usize!(0x14, core::mem::offset_of!(Membit, item));
const_assert_eq_usize!(0x18, core::mem::offset_of!(Membit, visited_room));
const_assert_eq_usize!(0x20, core::mem::offset_of!(Membit, key_num));
const_assert_eq_usize!(0x21, core::mem::offset_of!(Membit, dungeon_item));

#[rustfmt::skip]
pub mod switch {
  pub const MISC_SWITCH_OUTSET_WOODS_0:      i32 = 0x00;
  pub const MISC_SWITCH_OUTSET_WOODS_1:      i32 = 0x01;
  pub const MISC_SWITCH_OUTSET_WOODS_3:      i32 = 0x03;
  pub const MISC_SWITCH_OUTSET_WOODS_7:      i32 = 0x07;
  pub const MISC_SWITCH_DRI_POND:            i32 = 0x08;
  pub const MISC_SWITCH_RITO_AERIE_BAG:      i32 = 0x09;
  pub const MISC_SWITCH_FOREST_HAVEN_INTRO:  i32 = 0x1e;
  pub const MISC_SWITCH_WINDFALL_JAIL_INTRO: i32 = 0x13;
  pub const MISC_SWITCH_SONG_TABLET_ET:      i32 = 0x3e;
  pub const MISC_SWITCH_SONG_TABLET_WT:      i32 = 0x3f;

  pub const SEA_SWITCH_ENDLESS_NIGHT:        i32 = 0x19;
  pub const SEA_SWITCH_WINDFALL_INTRO:       i32 = 0x3f;
  pub const SEA_SWITCH_FF2_INTRO:            i32 = 0x58;
  pub const SEA_SWITCH_FCP_INTRO:            i32 = 0x50;

  pub const DRC_SWITCH_KEY_ENTRANCE:         i32 = 0x3c;
  pub const DRC_SWITCH_GOSSIP:               i32 = 0x21;
  pub const DRC_SWITCH_BARRICADE_HUB:        i32 = 0x07;
  pub const DRC_SWITCH_CAMERA_PAN_HUB:       i32 = 0x33;
  pub const DRC_SWITCH_CAMERA_PAN_ENTRANCE:  i32 = 0x46;
  pub const DRC_SWITCH_CAMERA_PAN_OUTSIDE:   i32 = 0x09;

  pub const FW_SWITCH_INTRO:                 i32 = 0x36;

  pub const TOTG_SWITCH_INTRO:               i32 = 0x2d;
  pub const TOTG_SWITCH_OUTSIDE_INTRO:       i32 = 0x63;

  pub const ET_SWITCH_GOSSIP:                i32 = 0x2a;

  pub const HYRULE_SWITCH_INTRO:             i32 = 0x12;
  pub const HYRULE_SWITCH_BLOCK_PUZZLE:      i32 = 0x06;

  pub const GT_SWITCH_INTRO:                 i32 = 0x0d;
  pub const GT_SWITCH_CAMERA_LIGHTS:         i32 = 0x1c;
  pub const GT_SWITCH_CAMERA_SWITCHES:       i32 = 0x1d;
  pub const GT_SWITCH_PUPPET_GANON_INTRO:    i32 = 0x1e;
  pub const GT_SWITCH_PUPPET_GANON_OUTRO:    i32 = 0x12;
  pub const GT_SWITCH_GANONDORF_INTRO:       i32 = 0x1f;
}

impl Membit {
  pub fn on_switch(&mut self, switch: i32) {
    self.switch[(switch >> 5) as usize] |= 1 << (switch & 0x1f);
  }

  pub fn set_switch(&mut self, idx: u32, switch: u32) {
    self.switch[idx as usize] = switch;
  }

  pub fn on_dungeon_item(&mut self, item: i32) {
    self.dungeon_item |= (1 << item) as u8;
  }
}

#[repr(C)]
pub struct PGameInfo {
  unknown: [u8; 0x20],
  pub info: GameInfo,
}

const_assert_eq_usize!(0x20, core::mem::offset_of!(PGameInfo, info));

#[repr(C)]
pub struct Play {
  bgs: [u8; 0x1404],
  ccs: [u8; 0x3df8 - 0x1404],
  adm: [u8; 0x3e94 - 0x3df8],
  cur_stage: [u8; 0x3ea0 - 0x3e94],
  next_stage: [u8; 0x3eae - 0x3ea0],
  field_0x3eae: [u8; 0x3eb0 - 0x3eae],
  stage_data: *const c_void,

  unknown: [u8; 0xa08],
  pub item_key_num_count: i16,
  pub item_max_life: i16,
  pub item_magic_count: i16,
  field: i16,
  pub item_max_magic_count: i16,
  unknown2: [u8; 0x1b6],
}

const_assert_eq_usize!(0x1404, core::mem::offset_of!(Play, ccs));
const_assert_eq_usize!(0x3df8, core::mem::offset_of!(Play, adm));
const_assert_eq_usize!(0x3e94, core::mem::offset_of!(Play, cur_stage));
const_assert_eq_usize!(0x3ea0, core::mem::offset_of!(Play, next_stage));
const_assert_eq_usize!(0x3eb0, core::mem::offset_of!(Play, stage_data));
const_assert_eq_usize!(0x48bc, core::mem::offset_of!(Play, item_key_num_count));
const_assert_eq_usize!(0x48c4, core::mem::offset_of!(Play, item_max_magic_count));
const_assert_eq_usize!(0x4a7c, core::mem::size_of::<Play>());

pub struct StagInfo {
  field_0x0: f32,
  field_0x4: f32,
  pub camera_type: u8,
  pub stage_id_flags: u8,
  field_0x0a: u16,
  field_0x0c: u32,
  field_0x10: u32,
  field_0x14: u32,
  field_0x18: u32,
  field_0x1c: u32,
}

type GetStagInfo = extern "C" fn(*const c_void) -> *mut StagInfo;

impl Play {
  pub fn get_stage_info(&self) -> &StagInfo {
    unsafe {
      let f: GetStagInfo = core::mem::transmute(self.stage_data.add(0x15c));
      &*(f(self.stage_data))
    }
  }
}

#[repr(C)]
pub struct DrawList {
  unknown: [u8; 0x16234],
}

#[repr(C)]
pub struct ResControl {
  unknown: [u8; 0x1200],
}

#[repr(C)]
pub struct GameInfo {
  pub save: SaveInfo,
  pub play: Play,
  pub drawlist: DrawList,
  pub unknown: [u8; 0x1bfc0 - 0x1bf50],
  pub res_control: ResControl,
  pub field_0x1d1c0: u8,
  pub brightness: u8,
}

#[repr(C)]
pub struct DanBit {
  pub stage_no: i8,
  pub gba_rupee_count: u8,
  pub switch: [u32; 2],
}

#[repr(C)]
pub struct ZoneBit {
  switch: [u16; 3],
  item: u16,
}

#[repr(C)]
pub struct ZoneActor {
  actor_flags: [u32; 16],
}

#[repr(C)]
pub struct Zone {
  room: i8,
  zone: ZoneBit,
  actor: ZoneActor,
}

#[repr(C)]
pub struct Restart {
  restart_room: i8,
  option: u8,
  option_room_no: i8,
  option_point: i16,
  option_room_angle_y: i16,
  option_room_pos: Xyz,
  start_code: i16,
  restart_angle: i16,
  restart_pos: Xyz,
  restart_param: u32,
  last_speed_f: f32,
  last_mode: u32,
}

#[repr(C)]
pub struct TurnRestart {
  position: Xyz,
  param: u32,
  angle_y: i16,
  room_no: i8,
  field_0x13: u8,
  field_0x14: [u8; 0x24 - 0x14],
  ship_pos: Xyz,
  ship_angle_y: i16,
  field_0x34: i32,
}

const MEMORY_SWITCH: u8 = 0x80;
const DAN_SWITCH: u8 = 0x40;
const ZONE_SWITCH: u8 = 0x30;
const MEMORY_ITEM: u8 = 0x40;
const ZONE_ITEM: u8 = 0x10;
const ZONE_MAX: u8 = 0x20;

#[repr(C)]
pub struct SaveInfo {
  pub save_data: Save,
  pub memory: Membit,
  pub dan: DanBit,
  pub zone: [Zone; ZONE_MAX as usize],
  pub restart: Restart,
  pub event: SaveEvent,
  pub turn_restart: TurnRestart,
  pub field_0x1290: u8,
  pub field_0x1291: u8,
  pub field_0x1292: u8,
  pub field_0x1298: i64,
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum StageId {
  Sea = 0x00,
  Sea2 = 0x01,
  ForsakenFortress = 0x02,
  DragonRoostCavern = 0x03,
  ForbiddenWoods = 0x04,
  TowerOfTheGods = 0x05,
  EarthTemple = 0x06,
  WindTemple = 0x07,
  GanonsTower = 0x08,
  Hyrule = 0x09,
  Ship = 0x0a,
  Misc = 0x0b,
  Subdungeon = 0x0c,
  SubdungeonNew = 0x0d,
  BlueChuJelly = 0x0e,
  Test = 0x0f,
  Max = 0x10,
}

#[repr(C)]
pub struct Save {
  pub player: SavePlayer,
  pub memory: [Membit; StageId::Max as usize],
  pub ocean: SaveOcean,
  pub event: SaveEvent,
  pub reserve: SaveReserve,
}

const_assert_eq_usize!(0x380, core::mem::offset_of!(Save, memory));
const_assert_eq_usize!(0x5c0, core::mem::offset_of!(Save, ocean));
const_assert_eq_usize!(0x778, core::mem::size_of::<Save>());

#[repr(C)]
pub struct StatusA {
  pub max_life: u16,
  pub life: u16,
  pub rupee: u16,
  pub field_0x6: u16,
  pub field_0x8: u8,
  pub select_item: [u8; 5],
  pub select_equip: [u8; 4],
  pub wallet_size: u8,
  pub max_magic: u8,
  pub magic: u8,
  pub field_0x15: u8,
  pub field_0x16: u8,
}

#[repr(C)]
pub struct StatusB {
  date_ipl: u64,
  field_0x8: f32,
  time: f32,
  date: u16,
  tact_wind_angle_x: i16,
  tact_wind_angle_y: i16,
}

#[repr(C)]
pub struct ReturnPlace {
  name: [u8; 8],
  room_no: i8,
  point: u8,
  unk_0xa: u8,
  unk_0xb: u8,
}

#[repr(C)]
pub struct PlayerItem {
  pub items: [u8; 21],
}

#[repr(C)]
pub struct PlayerGetItem {
  pub item_flags: [u8; 21],
}

#[repr(C)]
pub struct PlayerItemRecord {
  pub timer: u16,
  pub picture_num: u8,
  pub arrow_num: u8,
  pub bomb_num: u8,
  pub bottle_num: [u8; 3],
}

#[repr(C)]
pub struct PlayerItemMax {
  pub unknown_num: u8,
  pub arrow_num: u8,
  pub bomb_num: u8,
  pub unknown_nums: [u8; 5],
}

#[repr(C)]
pub struct PlayerBagItem {
  beast: [u8; 8],
  bait: [u8; 8],
  reserve: [u8; 8],
}

#[repr(C)]
pub struct PlayerGetBagItem {
  reserve_flags: u32,
  pub beast_flags: u8,
  pub bait_flags: u8,
  unknown: [u8; 6],
}

#[repr(C)]
pub struct PlayerBagItemRecord {
  beast_num: [u8; 8],
  bait_num: [u8; 8],
  reserve_num: [u8; 8],
}

#[repr(C)]
pub struct PlayerCollect {
  pub collect: [u8; 8],
  pub field_0x8: u8,
  pub tact: u8,
  pub triforce: u8,
  pub symbol: u8,
  pub field_0xc: u8,
}

#[repr(u8)]
pub enum CollectSlot {
  Swords = 0x0,
  Charms = 0x3,
}

#[repr(u8)]
pub enum CollectSword {
  HerosSword = 0x0,
  MasterSword = 0x1,
  Level3Sword = 0x2,
  MasterSwordEx = 0x3,
}

#[repr(u8)]
pub enum CollectTact {
  WindsRequiem = 0x0,
  BalladOfGales = 0x1,
  CommandMelody = 0x2,
  EarthGodsLyric = 0x3,
  WindGodsAria = 0x4,
  SongOfPassing = 0x5,
}

impl PlayerCollect {
  pub fn on_collect(&mut self, slot: CollectSlot, item: u8) {
    self.collect[slot as usize] |= (1 << item) as u8;
  }

  pub fn off_collect(&mut self, slot: CollectSlot, item: u8) {
    self.collect[slot as usize] &= !(1 << item) as u8;
  }

  pub fn is_collect(&self, slot: CollectSlot, item: u8) -> bool {
    self.collect[slot as usize] & (1 << item) as u8 > 0
  }

  pub fn on_tact(&mut self, tact: CollectTact) {
    self.tact |= (1 << tact as u32) as u8;
  }

  pub fn off_tact(&mut self, tact: CollectTact) {
    self.tact &= !(1 << tact as u32) as u8;
  }

  pub fn is_tact(&self, tact: CollectTact) -> bool {
    self.tact & (1 << tact as u32) as u8 > 0
  }
}

#[repr(C)]
pub struct Xyz {
  x: f32,
  y: f32,
  z: f32,
}

#[repr(C)]
pub struct PlayerMap {
  field_0x0: [[u32; 4]; 4],
  field_0x40: [u8; 49],
  field_0x71: [u8; 16],
  field_0x81: u8,
  field_0x82: [u8; 0x84 - 0x82],
}

#[repr(C)]
pub struct PlayerInfo {
  field_0x0: [u8; 0x10],
  field_0x10: u16,
  death_count: u16,
  player_name: [u8; 17],
  field_0x25: [u8; 17],
  field_0x36: [u8; 17],
  field_0x47: [u8; 17],
  clear_count: u8,
  random_salvage_point: u8,
  field_0x5a: [u8; 0x5c - 0x5a],
}

#[repr(C)]
pub struct PlayerConfig {
  ruby: u8,
  sound_mode: u8,
  attention_type: u8,
  vibration: u8,
  field_0x4: u8,
}

#[repr(C)]
pub struct PlayerPriest {
  pos: Xyz,
  rotate: i16,
  room: i8,
  flags: u8,
}

#[repr(C)]
pub struct PlayerStatusC {
  noise: [u8; 0x70],
}

#[repr(C)]
pub struct SavePlayer {
  pub status_a: StatusA,
  pub status_b: StatusB,
  pub return_place: ReturnPlace,
  pub item: PlayerItem,
  pub get_item: PlayerGetItem,
  pub item_record: PlayerItemRecord,
  pub item_max: PlayerItemMax,
  pub bag_item: PlayerBagItem,
  pub get_bag_item: PlayerGetBagItem,
  pub bag_item_record: PlayerBagItemRecord,
  pub collect: PlayerCollect,
  pub map: PlayerMap,
  pub info: PlayerInfo,
  pub config: PlayerConfig,
  pub priest: PlayerPriest,
  pub status_c: [PlayerStatusC; 4],
}

const_assert_eq_usize!(0x380, core::mem::size_of::<SavePlayer>());

#[repr(C)]
pub struct SaveEvent {
  flags: [u8; 0x100],
}

impl SaveEvent {
  pub fn on_bit(&mut self, e: u16) {
    self.flags[(e >> 8) as usize] |= (e & 0xff) as u8;
  }

  pub fn off_bit(&mut self, e: u16) {
    self.flags[(e >> 8) as usize] &= !(e & 0xff) as u8;
  }

  pub fn is_bit(&self, e: u16) -> bool {
    self.flags[(e >> 8) as usize] & (e & 0xff) as u8 > 0
  }
}

#[repr(C)]
pub struct SaveOcean {
  salvage: [u16; 50],
}

const_assert_eq_usize!(0x64, core::mem::size_of::<SaveOcean>());

#[repr(C)]
pub struct SaveReserve {
  reserve: [u8; 0x50],
}

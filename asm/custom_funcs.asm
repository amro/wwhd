convert_progressive_item_id_for_createDemoItem:
stwu sp, -0x10 (sp)
mflr r0
stw r0, 0x14 (sp)

mr r3, r4 ; createDemoItem keeps the item ID in r4
bl convert_progressive_item_id ; Get the correct item ID
mr r31, r3 ; Put it back where createDemoItem expects it, in r31
; createDemoItem also expects to be in r4, but it was clobbered by convert_progressive_item_id
mr r4, r31

mr r29, r9

lwz r0, 0x14 (sp)
mtlr r0
addi sp, sp, 0x10
blr

convert_progressive_item_id_for_daItem_create:
stwu sp, -0x10 (sp)
mflr r0
stw r0, 0x14 (sp)

lbz r3, 0xB3 (r31) ; Read this field item's item ID from its params (params are at 0xB0, the item ID is has the mask 0x000000FF)
bl convert_progressive_item_id ; Get the correct item ID
stb r3, 0xB3 (r31) ; Store the corrected item ID back into the field item's params

; Then we return the item ID in r0 so that the next few lines in daItem_create can use it.
mr r0, r3

lwz r3, 0x14 (sp)
mtlr r3
addi sp, sp, 0x10
blr

convert_progressive_item_id_for_dProcGetItem_init_1:
stwu sp, -0x10 (sp)
mflr r0
stw r0, 0x14 (sp)

lwz r3, 0x30C (r28) ; Read the item ID property for this event action
bl convert_progressive_item_id ; Get the correct item ID

; Then we return the item ID in r0 so that the next few lines in dProcGetItem_init can use it.
mr r0, r3

lwz r3, 0x14 (sp)
mtlr r3
addi sp, sp, 0x10
blr

convert_progressive_item_id_for_dProcGetItem_init_2:
stwu sp, -0x10 (sp)
mflr r0
stw r0, 0x14 (sp)

lbz r3, 0x52AC (r3) ; Read the item ID from 803C9EB4
bl convert_progressive_item_id ; Get the correct item ID

; Then we return the item ID in r27 so that the next few lines in dProcGetItem_init can use it.
mr r27, r3

lwz r0, 0x14 (sp)
mtlr r0
addi sp, sp, 0x10
blr


convert_progressive_item_id_for_shop_item:
stwu sp, -0x10 (sp)
mflr r0
stw r0, 0x14 (sp)

; Replace the call to savegpr we overwrote to call this custom function
;bl _savegpr_28
mr r30, r3 ; Preserve the shop item entity pointer

lbz r3, 0xB3 (r30)
bl convert_progressive_item_id ; Get the correct item ID
stb r3, 0x63A (r30) ; Store the item ID to shop item entity+0x63A

mr r3, r30 ; Put the shop item entity pointer back into r3, because that's where the function that called this one expects it to be

lwz r0, 0x14 (sp)
mtlr r0
addi sp, sp, 0x10
blr

; Acts as a replacement to getSelectItemNo, but should only be called when the shopkeeper is checking if the item get animation should play or not, in order to have that properly show for progressive items past the first tier.
; If this was used all the time as a replacement for getSelectItemNo it would cause the shop to be buggy since it uses the item ID to know what slot it's on.
custom_getSelectItemNo_progressive:
stwu sp, -0x10 (sp)
mflr r0
stw r0, 0x14 (sp)

;bl getSelectItemNo__11ShopItems_cFv
bl convert_progressive_item_id

lwz r0, 0x14 (sp)
mtlr r0
addi sp, sp, 0x10
blr

create_pot_item:
; Most registers are filled with the function call arguments at this point, so we can only use r0.
lhz r0, 0x798 (r27) ; Load the pot's backup of its original Z rotation parameters bitfield
rlwinm r0, r0, 24, 24, 31 ; Extract byte 0xFF00 from the Z rotation parameters (unused in vanilla)

; If the custom item ID is invalid we use the vanilla parameter (dropped_item_id).
; The item IDs we consider invalid are 0xFF (invalid item) and 0x00 (heart pickup).
; The reason we consider 0x00/heart to be invalid here is simply that this unused byte was always
; set to 0x00 in vanilla, and it's better to avoid modifying every existing pot in the game.
; Also, if we want a pot to drop a heart, we can use its existing dropped_item_id param instead.
cmpwi r0, 0xff
beq create_pot_item_use_dropped_item_id
cmpwi r0, 0x00
beq create_pot_item_use_dropped_item_id

create_pot_item_use_custom_item_id:
mr r4, r0 ; Modify the item ID parameter to use our custom item ID before passing it
li r7, 3 ; Don't fade out
li r9, 5 ; Item action, how it behaves. 5 causes it to make a ding sound so that it's more obvious.
bl fopAcM_createItem__FP4cXyziiiiP5csXyziP4cXyz
b create_pot_item_return_point ; Return to original code

create_pot_item_use_dropped_item_id:
; Don't modify any parameters, use the ones already set by the vanilla code.
; And call the same function as the line we overwrote to jump here.
bl fopAcM_createItemFromTable__FP4cXyziiiiP5csXyziP4cXyz
b create_pot_item_return_point ; Return to original code
